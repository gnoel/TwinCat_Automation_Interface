﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;


namespace TwinCat_Automation_Interface2.Objs
{
  public  class DriveType : INotifyPropertyChanged
    {

        #region INotifyPropertyChanged Members

        /// <summary>
        /// Need to implement this interface in order to get data binding
        /// to work properly.
        /// </summary>
        /// <param name="propertyName"></param>
        private void NotifyPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion



       private string _rfc_A = "Unidrive M700 RFC_A";
       public string RFC_A
        {
            get { return _rfc_A; }
            set
            {

                if (_rfc_A == value) return;

                _rfc_A = value;


                NotifyPropertyChanged("RFC_A");


            }
        }



       private string _rfc_S = "Unidrive M700 RFC_S";
       public string RFC_S
        {
            get { return _rfc_S; }
            set
            {

                if (_rfc_S == value) return;

                _rfc_S = value;


                NotifyPropertyChanged("RFC_S");


            }
        }



        private string _beckhoff = "BeckhoffTerm Drive";
        public string Beckhoff_Drive
        {
            get { return _beckhoff; }
            set
            {

                if (_beckhoff == value) return;

                _beckhoff = value;


                NotifyPropertyChanged("Beckhoff_Drive");

            }
        }


        private string _virtualDrive = "Virtual Drive";
        public string Virtual_Drive
        {
            get { return _virtualDrive; }
            set
            {

                if (_virtualDrive == value) return;

                _virtualDrive = value;


                NotifyPropertyChanged("Virtual_Drive");


            }
        }


        private List<string> _typeList = new List<string>();

        public List<string> TypeList
        {
            get { return _typeList; }
            set
            {

                if (_typeList == value) return;

                _typeList = value;


                NotifyPropertyChanged("TypeList");


            }
        }
    }




}


   


