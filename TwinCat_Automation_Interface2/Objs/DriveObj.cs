﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwinCat_Automation_Interface2.Objs
{
      

    public class DriveAxisObj : INotifyPropertyChanged, IDataErrorInfo
    {

    

        #region INotifyPropertyChanged Members

        /// <summary>
        /// Need to implement this interface in order to get data binding
        /// to work properly.
        /// </summary>
        /// <param name="propertyName"></param>
        private void NotifyPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        
        private bool _axisInArray = false;
        public bool AxisInArray
        {
            get { return _axisInArray; }
            set
            {

                if (_axisInArray == value) return;

                _axisInArray = value;

                NotifyPropertyChanged("AxisInArray");



            }
        }

        
        private bool _driveInArray = false;
        public bool DriveInArray
        {
            get { return _driveInArray; }
            set
            {

                if (_driveInArray == value) return;

                _driveInArray = value;

                NotifyPropertyChanged("DriveInArray");

                
            }
        }

        

        private int _deisredAxisIndex = 0;
        public int DesiredAxisIndex { get { return _deisredAxisIndex; } set {

                if (_deisredAxisIndex == value) return;

                _deisredAxisIndex = value;

                NotifyPropertyChanged("DesiredAxisIndex");

                
            }
        }


        private int _deisredDriveIndex = 0;
        public int DesiredDriveIndex
        {
            get { return _deisredDriveIndex; }
            set
            {

                if (_deisredDriveIndex == value) return;

                _deisredDriveIndex = value;

                NotifyPropertyChanged("DesiredDriveIndex");


            }
        }

        private string _driveName = string.Empty;
        public string DriveName { get { return  _driveName; }


            set {
                if (_driveName == value) return;

                _driveName = value;


                NotifyPropertyChanged("DriveName");

            }
        }


        private string _axisName = string.Empty;
        public string AxisName
        {
            get { return _axisName; }


            set
            {
                if (_axisName == value) return;

                _axisName = value;


                NotifyPropertyChanged("AxisName");

            }
        }




        private string _selectedDriveType = string.Empty;

        public string SelectedDriveType { get { return _selectedDriveType; }
            set {

                if (_selectedDriveType == value) return;

                _selectedDriveType = value;


                NotifyPropertyChanged("SelectedDriveType");

            }
        }

        

        public string Error
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public string this[string columnName]
        {
            get
            {
                if (columnName == "DesiredIndex") {


                }
                return null;

            }

        }
    }
}
