﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TCatSysManagerLib;
using TwinCat_Automation_Interface2.Objs;
using TwinCat_Automation_Interface2.ViewModels;

namespace TwinCat_Automation_Interface2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            
            var dC = new ViewModels.MainWindowViewModel();
            DataContext = dC;

            DriveType types = new DriveType();
            types.TypeList.Add(types.RFC_A);
            types.TypeList.Add(types.RFC_S);
            types.TypeList.Add(types.Beckhoff_Drive);
            types.TypeList.Add(types.Virtual_Drive);
            Column7.ItemsSource = types.TypeList;
        }

      
    }
}
