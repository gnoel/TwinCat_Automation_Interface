﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using TCatSysManagerLib;
using TwinCat_Automation_Interface2.Helpers;
using TwinCat_Automation_Interface2.Objs;
using TwinCat_Automation_Interface2.Views;



namespace TwinCat_Automation_Interface2.ViewModels
{
    public class PLCSelectionViewModel : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged Members

        /// <summary>
        /// Need to implement this interface in order to get data binding
        /// to work properly.
        /// </summary>
        /// <param name="propertyName"></param>
        private void NotifyPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        public RelayCommand SelectPLC { get; set; }


        #region Contructor
        public PLCSelectionViewModel(string SelectedPLC, ITcSysManager sysManager) {
            SelectPLC = new RelayCommand(ExitApplication);


            _sysManager = sysManager;

            _selectedPLCName = SelectedPLC;

            PopulateComboList();

        }
        #endregion


        
        private ITcSysManager _sysManager;
        public ITcSysManager SysManager
        {

            get { return _sysManager; }

            set
            {
                if (_sysManager == value) return;

                _sysManager = value;


                NotifyPropertyChanged("SysManager");
            }

        }




        #region SelectedPLCName
        private string _selectedPLCName;

        public String SelectedPLCName
        {
            get { return _selectedPLCName; }

            set
            {
                if (_selectedPLCName == value) return;

                _selectedPLCName = value;

                NotifyPropertyChanged("SelectedPLCName");
            }

        }

        #endregion


        #region AvailablePLCs
        private List<string> _availablePLCs = new List<string>();

        public List<string> AvailablePLCs
        {
            get { return _availablePLCs; }

            set
            {
                if (_availablePLCs == value) return;

                _availablePLCs = value;

                NotifyPropertyChanged("AvailablePLCs");
            }

        }

        #endregion

        


        public void PopulateComboList() {

            ITcSmTreeItem plcPousItem = SysManager.LookupTreeItem("TIPC");

            for (int i = 1; i <= plcPousItem.ChildCount; i++ ) {
                             

              AvailablePLCs.Add(plcPousItem.Child[i].Name.ToString());

            }
            
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        void ExitApplication(object parameter)
        {
            if (SelectedPLCName != string.Empty) {


                foreach (Window Win in System.Windows.Application.Current.Windows)
                {

                    if (Win.DataContext.ToString() == "TwinCat_Automation_Interface2.ViewModels.PLCSelectionViewModel")
                    {

                        Win.Close();


                    }


                }

            }
           

        }


    }
}
