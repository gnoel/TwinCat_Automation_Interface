﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using TwinCat_Automation_Interface2.ViewModels;
using TwinCat_Automation_Interface2.Helpers;
using TwinCat_Automation_Interface2.Objs;
using TwinCat_Automation_Interface2.Views;
using System.IO;
using TCatSysManagerLib;
using System.Windows;
using EnvDTE;
using TwinCat_Automation_Interface2.Helpers;
using System.Threading;
using System.Xml;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Collections;
using System.Runtime.InteropServices.ComTypes;
using System.Windows.Media.Animation;
using System.Xml.Serialization;
using System.Xml.Linq;

namespace TwinCat_Automation_Interface2.ViewModels
{
    public class MainWindowViewModel : INotifyPropertyChanged 
    {

        #region Construction
        public  MainWindowViewModel()
        {

            MessageFilter.Register(); 

            CreateNewProjectCommand = new RelayCommand(CreateNewProject);
            HelpFileCommand = new RelayCommand(OpenHelpFile);
            ExitCommand = new RelayCommand(ExitApplication);
            CreateDriveListCommand = new RelayCommand(CreateDriveList);
            CreateDrivesCommand = new RelayCommand(CreateDrives);

            OpenSolutionCommand = new RelayCommand(OpenRunningSolution);

            ExportDriveListCommand = new RelayCommand(ExportDriveList);

            ImportDriveListCommand = new RelayCommand(ImportDriveList);


            //
            VSVersions = new List<string>();
            VSVersions.Add("Visual Studio 2013");
            VSVersions.Add("Visual Studio 2015");


        }
        #endregion	 

        #region Properties
        public RelayCommand CreateNewProjectCommand { get; set; }
        public RelayCommand HelpFileCommand { get; set; }


        public RelayCommand ExitCommand { get; set; }

        public RelayCommand CreateDrivesCommand { get; set; }


        public RelayCommand OpenSolutionCommand { get; set; }


        public RelayCommand CreateDriveListCommand { get; set; }


        public RelayCommand ImportDriveListCommand { get; set; }

        public RelayCommand ExportDriveListCommand { get; set; }





        #region SelectedPLCName
        private string _selectedPLCName = string.Empty;

        public String SelectedPLCName {
            get { return _selectedPLCName; }

            set {
                if (_selectedPLCName == value) return;

                _selectedPLCName = value;

                NotifyPropertyChanged("SelectedPLCName");
            }

        }

        #endregion

        #region SelectedVSVersion
        private string _selectedVSVersion = "None Selected";

        public String SelectedVSVersion
        {
            get { return _selectedVSVersion; }

            set
            {
                if (_selectedVSVersion == value) return;

                _selectedVSVersion = value;


                if (_selectedVSVersion == "Visual Studio 2013") {

                    VSVersionNum = "12.0";

                } else if (_selectedVSVersion == "Visual Studio 2015") {

                    VSVersionNum = "14.0";

                }



                if (!(_selectedVSVersion == "None Selected")){
                    VSSelectionMade = true;
                }



                NotifyPropertyChanged("SelectedVSVersion");
            }

        }

        #endregion


        #region VSVersionNum
        private string _vSVersionNum = string.Empty;

        public String VSVersionNum
        {
            get { return _vSVersionNum; }

            set
            {
                if (_vSVersionNum == value) return;

                _vSVersionNum = value;
                        



                NotifyPropertyChanged("VSVersionNum");
            }

        }

        #endregion

        #region VSSelectionMade
        private Boolean _vSSelectionMade = false;
        public Boolean VSSelectionMade
        {
            get { return _vSSelectionMade; }

            set
            {
                if (_vSSelectionMade == value) return;

                _vSSelectionMade = value;

                NotifyPropertyChanged("VSSelectionMade");
            }

        }
        #endregion

        
        #region DriveList
        private ObservableCollection <DriveAxisObj>  _drivelist = new ObservableCollection<DriveAxisObj>();
        public ObservableCollection <DriveAxisObj> DriveList
        {
            get { return _drivelist; }

            set
            {
                if (_drivelist == value) return;

                _drivelist = value;


                NotifyPropertyChanged("DriveList");
            }

        }

        #endregion

        #region DriveBeckoffList
        private ObservableCollection<DriveAxisObj> _driveBeckofflist = new ObservableCollection<DriveAxisObj>();
        public ObservableCollection<DriveAxisObj> DriveBeckoffList
        {
            get { return _driveBeckofflist; }

            set
            {
                if (_driveBeckofflist == value) return;

                _driveBeckofflist = value;


                NotifyPropertyChanged("DriveBeckoffList");
            }

        }

        #endregion

        #region HasDrives
        private Boolean _hasdrive;
        public Boolean HasDrives
        {
            get { return _hasdrive; }

            set
            {
                if (_hasdrive == value) return;

                _hasdrive = value;

                NotifyPropertyChanged("HasDrives");
            }

        }
        #endregion

        #region NumberOfDrive
        private int _numberOfDrives =1;
        public int NumberOfDrive
        {
            get { return _numberOfDrives; }

            set
            {
                if (_numberOfDrives == value) return;

                _numberOfDrives = value;

                NotifyPropertyChanged("NumberOfDrive");
            }

        }
        #endregion

        #region HasValidEtherCat
        private Boolean _hasValidEtherCat;
        public Boolean HasValidEtherCat
        {
            get { return _hasValidEtherCat; }

            set
            {
                if (_hasValidEtherCat == value) return;

                _hasValidEtherCat = value;
       


                NotifyPropertyChanged("HasValidEtherCat");
            }

        }
        #endregion		

       
        
        #region FileIsLoaded
        private Boolean _fileIsLoaded;
        public Boolean FileIsLoaded
        {
            get { return _fileIsLoaded; }

            set
            {
                if (_fileIsLoaded == value) return;

                _fileIsLoaded = value;

                ProjectIsLoaded = !value;

                NotifyPropertyChanged("FileIsLoaded");
            }

        }
        #endregion

        #region ProjectIsLoaded
        private Boolean _projectIsLoaded = true;
        public Boolean ProjectIsLoaded
        {
            get { return _projectIsLoaded; }

            set
            {
                if (_projectIsLoaded == value) return;

                _projectIsLoaded = value;


                NotifyPropertyChanged("ProjectIsLoaded");
            }

        }
        #endregion

        #region FileStatus
        private string _fileStatus = string.Empty;
        public string FileStatus
        {
            get { return _fileStatus; }

            set
            {
                if (_fileStatus == value) return;

                _fileStatus = value;



                NotifyPropertyChanged("FileStatus");
            }

        }
        #endregion


        #region SCIMotionVersion
        private string _sCIMotionVersion = string.Empty;
        public string SCIMotionVersion
        {
            get { return _sCIMotionVersion; }

            set
            {
                if (_sCIMotionVersion == value) return;

                _sCIMotionVersion = value;



                NotifyPropertyChanged("SCIMotionVersion");
            }

        }
        #endregion


        #region FileName
        private string _fileName = string.Empty;
        public string FileName
        {
            get { return _fileName; }

            set
            {
                if (_fileName == value) return;

                _fileName = value;



                NotifyPropertyChanged("FileName");
            }

        }
        #endregion

        #region SolutionDirectory
        private string _solutionDirectoy = string.Empty;
        public string SolutionDirectory
        {
            get { return _solutionDirectoy; }

            set
            {
                if (_solutionDirectoy == value) return;

                _solutionDirectoy = value;



                NotifyPropertyChanged("SolutionDirectory");
            }

        }
        #endregion

        #region SolutionName
        private string _solutionName = string.Empty;
        public string SolutionName
        {
            get { return _solutionName; }

            set
            {
                if (_solutionName == value) return;

                _solutionName = value;



                NotifyPropertyChanged("SolutionName");
            }

        }
        #endregion


        #region EtherCatMasName
        private string _etherCatMasName = string.Empty;
        public string EtherCatMasName
        {
            get { return _etherCatMasName; }

            set
            {
                if (_etherCatMasName == value) return;

                _etherCatMasName = value;



                NotifyPropertyChanged("EtherCatMasName");
            }

        }
        #endregion

        #region NCTaskName
        private string _nCTaskName = string.Empty;
        public string NCTaskName
        {
            get { return _nCTaskName; }

            set
            {
                if (_nCTaskName == value) return;

                _nCTaskName = value;



                NotifyPropertyChanged("NCTaskName");
            }

        }
        #endregion
        
        #region Project
        private dynamic _project ;
        public dynamic Project {
            get { return _project; }

            set
            {
                if (_project == value) return;

                _project = value;


                NotifyPropertyChanged("Project");
            }




        }
        #endregion

        #region DTE
        private EnvDTE.DTE _dte;
        public EnvDTE.DTE DTE {
            get { return _dte; }

            set
            {
                if (_dte == value) return;

                _dte = value;


                NotifyPropertyChanged("DTE");
            }

            
        }
        #endregion

        #region VSVersions
        private List<string> _vSVersions;
        public List<string> VSVersions
        {
            get { return _vSVersions; }

            set
            {
                if (_vSVersions == value) return;

                _vSVersions = value;


                NotifyPropertyChanged("VSVersions");
            }


        }
        #endregion

        #region SysManager
        private ITcSysManager _sysManager;
        public ITcSysManager SysManager {

            get { return _sysManager; }

            set
            {
                if (_sysManager == value) return;

                _sysManager = value;


                NotifyPropertyChanged("SysManager");
            }
            
        }
        #endregion

        #region SysLibrary
        private ITcPlcLibraryManager _sysLibrary;
        public ITcPlcLibraryManager SysLibrary {
            get { return _sysLibrary; }

            set
            {
                if (_sysLibrary == value) return;

                _sysLibrary = value;


                NotifyPropertyChanged("SysLibrary");
            }

        }
        #endregion

       

      

        #region HasValidReference
        private Boolean _hasValidReference = false;
        public Boolean HasValidReference
        {
            get { return _hasValidReference; }

            set
            {
                if (_hasValidReference == value) return;

                _hasValidReference = value;

                NotifyPropertyChanged("HasValidReference");
            }

        }
        #endregion

        #region HasValidTerminal
        private Boolean _hasValidTerminal = false;
        public Boolean HasValidTerminal
        {
            get { return _hasValidTerminal; }

            set
            {
                if (_hasValidTerminal == value) return;

                _hasValidTerminal = value;

                NotifyPropertyChanged("HasValidTerminal");
            }

        }
        #endregion

        #region HasValidEndTerminal
        private Boolean _hasValidEndTerminal = false;
        public Boolean HasValidEndTerminal
        {
            get { return _hasValidEndTerminal; }

            set
            {
                if (_hasValidEndTerminal == value) return;

                _hasValidEndTerminal = value;

                NotifyPropertyChanged("HasValidEndTerminal");
            }

        }
        #endregion

        #region TerminalName
        private string _terminalName = string.Empty;
        public string TerminalName
        {
            get { return _terminalName; }

            set
            {
                if (_terminalName == value) return;

                _terminalName = value;



                NotifyPropertyChanged("TerminalName");
            }

        }
        #endregion
        

        #region HasValidTMC
        private Boolean _hasValidTMC = false;
        public Boolean HasValidTMC
        {
            get { return _hasValidTMC; }

            set
            {
                if (_hasValidTMC == value) return;

                _hasValidTMC = value;

                NotifyPropertyChanged("HasValidTMC");
            }

        }
        #endregion

       

        #region DrivesConfigured
        private Boolean _driveConfigured = false;
        public Boolean DrivesConfigured
        {
            get { return _driveConfigured; }

            set
            {
                if (_driveConfigured == value) return;

                _driveConfigured = value;

                NotifyPropertyChanged("DrivesConfigured");
            }

        }
        #endregion

        #region DrivesCreated
        private Boolean _driveCreated = false;
        public Boolean DrivesCreated
        {
            get { return _driveCreated; }

            set
            {
                if (_driveCreated == value) return;

                _driveCreated = value;

                NotifyPropertyChanged("DrivesCreated");
            }

        }
        #endregion

        #region HasValidGVL
        private Boolean _hasValidGVL = false;
        public Boolean HasValidGVL
        {
            get { return _hasValidGVL; }

            set
            {
                if (_hasValidGVL == value) return;

                _hasValidGVL = value;

                NotifyPropertyChanged("HasValidGVL");
            }

        }
        #endregion


        #region HasValidPLC
        private Boolean _hasValidPLC = false;
        public Boolean HasValidPLC
        {
            get { return _hasValidPLC; }

            set
            {
                if (_hasValidPLC == value) return;

                _hasValidPLC = value;

                NotifyPropertyChanged("HasValidPLC");
            }

        }
        #endregion

        #region MinIndexAxisValue
        private int _minIndexAxisValue = 0;
        public int MinIndexAxisValue
        {
            get { return _minIndexAxisValue; }

            set
            {
                if (_minIndexAxisValue == value) return;

                _minIndexAxisValue = value;

                NotifyPropertyChanged("MinIndexAxisValue");
            }

        }
        #endregion

        #region MaxIndexAxisValue
        private int _maxIndexAxisValue = 0;
        public int MaxIndexAxisValue
        {
            get { return _maxIndexAxisValue; }

            set
            {
                if (_maxIndexAxisValue == value) return;

                _maxIndexAxisValue = value;

                NotifyPropertyChanged("MaxIndexAxisValue");
            }

        }
        #endregion

        #region MinIndexVirtuDriveValue
        private int _minIndexVirtuDriveValue = 0;
        public int MinIndexVirtuDriveValue
        {
            get { return _minIndexVirtuDriveValue; }

            set
            {
                if (_minIndexVirtuDriveValue == value) return;

                _minIndexVirtuDriveValue = value;

                NotifyPropertyChanged("MinIndexVirtuDriveValue");
            }

        }
        #endregion

        #region MaxIndexVirtuDriveValue
        private int _maxIndexVirtuDriveValue = 0;
        public int MaxIndexVirtuDriveValue
        {
            get { return _maxIndexVirtuDriveValue; }

            set
            {
                if (_maxIndexVirtuDriveValue == value) return;

                _maxIndexVirtuDriveValue = value;

                NotifyPropertyChanged("MaxIndexVirtuDriveValue");
            }

        }
        #endregion

        #region MinIndexBeckDriveValue
        private int _minIndexBeckDriveValue = 0;
        public int MinIndexBeckDriveValue
        {
            get { return _minIndexBeckDriveValue; }

            set
            {
                if (_minIndexBeckDriveValue == value) return;

                _minIndexBeckDriveValue = value;

                NotifyPropertyChanged("MinIndexBeckDriveValue");
            }

        }
        #endregion

        #region MaxIndexBeckDriveValue
        private int _maxIndexBeckDriveValue = 0;
        public int MaxIndexBeckDriveValue
        {
            get { return _maxIndexBeckDriveValue; }

            set
            {
                if (_maxIndexBeckDriveValue == value) return;

                _maxIndexBeckDriveValue = value;

                NotifyPropertyChanged("MaxIndexBeckDriveValue");
            }

        }
        #endregion

        #region MinIndexUniDriveValue
        private int _minIndexUniDriveValue = 0;
        public int MinIndexUniDriveValue
        {
            get { return _minIndexUniDriveValue; }

            set
            {
                if (_minIndexUniDriveValue == value) return;

                _minIndexUniDriveValue = value;

                NotifyPropertyChanged("MinIndexUniDriveValue");
            }

        }
        #endregion

        #region MaxIndexUniDriveValue
        private int _maxIndexUniDriveValue = 0;
        public int MaxIndexUniDriveValue
        {
            get { return _maxIndexUniDriveValue; }

            set
            {
                if (_maxIndexUniDriveValue == value) return;

                _maxIndexUniDriveValue = value;

                NotifyPropertyChanged("MaxIndexUniDriveValue");
            }

        }
        #endregion


        #region MinIndexAllDriveValue
        private int _minIndexAllDriveValue = 0;
        public int MinIndexAllDriveValue
        {
            get { return _minIndexAllDriveValue; }

            set
            {
                if (_minIndexAllDriveValue == value) return;

                _minIndexAllDriveValue = value;

                NotifyPropertyChanged("MinIndexAllDriveValue");
            }

        }
        #endregion

        #region MaxIndexAllDriveValue
        private int _maxIndexAllDriveValue = 0;
        public int MaxIndexAllDriveValue
        {
            get { return _maxIndexAllDriveValue; }

            set
            {
                if (_maxIndexAllDriveValue == value) return;

                _maxIndexAllDriveValue = value;

                NotifyPropertyChanged("MaxIndexAllDriveValue");
            }

        }
        #endregion


        #region HasValidMotion
        private Boolean _hasValidMotion = false;
        public Boolean HasValidMotion
        {
            get { return _hasValidMotion; }

            set
            {
                if (_hasValidMotion == value) return;

                _hasValidMotion = value;

                NotifyPropertyChanged("HasValidMotion");
            }

        }
        #endregion

        #region CurrentITCMAIN

        private ITcSmTreeItem _currentITcMain;
        public ITcSmTreeItem CurrentITCMAIN
        {
            get { return _currentITcMain; }

            set
            {
                if (_currentITcMain == value) return;

                _currentITcMain = value;

                NotifyPropertyChanged("CurrentITCMAIN");
            }

        }

        #endregion


        #region CurrentITCGVL

        private ITcSmTreeItem _currentITcGVL;
        public ITcSmTreeItem CurrentITCGVL
        {
            get { return _currentITcGVL; }

            set
            {
                if (_currentITcGVL == value) return;

                _currentITcGVL = value;

                NotifyPropertyChanged("CurrentITCGVL");
            }

        }

        #endregion
        #endregion


        #region Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        public void ImportDriveList(object parameter)
        {
            


                OpenFileDialog openfileDiaglog = new OpenFileDialog();
                //openfileDiaglog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);


                openfileDiaglog.Title = "Import Drive List";
                openfileDiaglog.DefaultExt = "xml";
                openfileDiaglog.Filter = "XML Document (*.xml)|*.xml";

                if (openfileDiaglog.ShowDialog() == DialogResult.OK)
                {


                    XmlDocument doc = new XmlDocument();

                    doc.Load(Path.GetFullPath(openfileDiaglog.FileName));

                    XmlElement root = doc.DocumentElement;

                    XmlNodeList nodes = root.SelectNodes("Drive");

                    ObservableCollection<DriveAxisObj> _dummyCollection1 = new ObservableCollection<DriveAxisObj>();

                    foreach (XmlNode node in nodes)
                    {


                        DriveAxisObj temp = new DriveAxisObj();

                        temp.DesiredAxisIndex = Int32.Parse(node["DesiredAxisIndex"].InnerText);
                        temp.DesiredDriveIndex = Int32.Parse(node["DesiredDriveIndex"].InnerText);
                        temp.AxisInArray = convertStringToBool(node["AxisInArray"].InnerText);
                        temp.AxisName = node["AxisName"].InnerText;
                        temp.SelectedDriveType = node["SelectedDriveType"].InnerText;
                        temp.DriveName = node["DriveName"].InnerText;
                        temp.DriveInArray = convertStringToBool(node["DriveInArray"].InnerText);



                        _dummyCollection1.Add(temp);
                    }
                    DriveList = _dummyCollection1;

                    NumberOfDrive = DriveList.Count;

                }



            

            
            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        private bool convertStringToBool(string info) {

            if (info =="true") {

                return true;
            } else {
                return false;

            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        public void ExportDriveList(object parameter)
        {
            if (DriveList.Count> 0) {
                SaveFileDialog SaveFileDialog1 = new SaveFileDialog();


                SaveFileDialog1.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                

                SaveFileDialog1.Title = "Export Drive List";
                SaveFileDialog1.DefaultExt = "xml";
                SaveFileDialog1.Filter = "XML Document (*.xml)|*.xml";




                if (SaveFileDialog1.ShowDialog() == DialogResult.OK) {

                  



                    XElement root = new XElement("DriveList");
                    foreach (var item in DriveList)
                    {
                        var xml = new XElement("Drive",
                                   new XElement("AxisInArray", item.AxisInArray),
                                   new XElement("DesiredAxisIndex", item.DesiredAxisIndex),
                                   new XElement("AxisName", item.AxisName),
                                   new XElement("DriveInArray", item.DriveInArray),
                                   new XElement("DriveName", item.DriveName),

                                   new XElement("DesiredDriveIndex", item.DesiredDriveIndex),
                                   new XElement("SelectedDriveType", item.SelectedDriveType)
                                  );
                        root.Add(xml);
                    }
                    root.Save(Path.GetFullPath(SaveFileDialog1.FileName));








                }


                


                FileStatus = "Drive List Exported";










            }

        }






        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        public void CreateDrives(object parameter)
        {


            //Verify if Drive list is correctly created.
            foreach (DriveAxisObj element in DriveList)
            {
                if ((element.SelectedDriveType == "") || (element.DriveName == "") || (element.AxisName == ""))
                {
                    DrivesConfigured = false;
                }
                else
                {
                    DrivesConfigured = true;
                }
            }


            if (DrivesConfigured == false)
            {
                //Warn user
                var result = System.Windows.Forms.MessageBox.Show("The given drives are not completly configured. Please complete you drives configuration table.",
                    "Drive Configuration Table",
                     MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                return;
            }
            else {
                //Warning user that you are about to create the drive based on the drive list
                CreateMainDriveAxisFile();


            }
            
            

            ITcSmTreeItem plcPousDevice = SysManager.LookupTreeItem("TIID");


            if (plcPousDevice.ChildCount > 0)
            {

                for (int w = 1; w <= plcPousDevice.ChildCount; w++)
                {

                    if (plcPousDevice.Child[w].ItemSubType == 111)
                    {



                        EtherCatMasName = plcPousDevice.Child[w].Name;
                    }


                }


            }


            ITcSmTreeItem plcPousNCTask = SysManager.LookupTreeItem("TINC");

            for (int i = 0; i <= plcPousNCTask.ChildCount; i++)
            {
                try
                {

                    if (plcPousNCTask.Child[i].ItemType == 1)
                    {

                        NCTaskName = plcPousNCTask.Child[i].Name;

                    }


                }
                catch (System.ArgumentException)
                {

                }

            }


            string templateDir = Directory.GetCurrentDirectory();

            //Go thoru the drive list to create the drives


            foreach (DriveAxisObj obj in DriveList)
            {

         ////////////////////////////////////////////////////////////////////////////////////////////////////


                //NC- Task Axises  Creation
                ITcSmTreeItem plcPousTask = SysManager.LookupTreeItem("TINC^" + NCTaskName + "^Axes");




                ITcSmTreeItem newAxis = plcPousTask.ImportChild(templateDir + "\\Axis_Template.xti", "", false, obj.AxisName);


       ////////////////////////////////////////////////////////////////////////////////////////////////////

                //Drive creation
                ITcSmTreeItem plcPousMasterDevice = SysManager.LookupTreeItem("TIID^" + EtherCatMasName);


                if (obj.SelectedDriveType == "Unidrive M700 RFC_A")
                {


                    ITcSmTreeItem newIo = plcPousMasterDevice.ImportChild(templateDir + "\\Unidrive M700 RFC-A.xti", "", false, obj.DriveName + "_RFC-A");



                }
                else if (obj.SelectedDriveType == "Unidrive M700 RFC_S")
                {



                    ITcSmTreeItem newIo = plcPousMasterDevice.ImportChild(templateDir + "\\Unidrive M700 RFC-S.xti", "", false, obj.DriveName + "_RFC-S");


                    



                }

                else if (obj.SelectedDriveType == "BeckhoffTerm Drive")
                {

                  
                    DriveBeckoffList.Add(obj);


                }
                else if (obj.SelectedDriveType == "Virtual Drive")
                {

                    //TODO CHANGE THE IMPORT OR CREATE FOR THE APPROPRIATE VIRTUAL DRIVE.

                    // ITcSmTreeItem newIo = plcPousMasterDevice.ImportChild(templateDir + "\\Unidrive M700 RFC-S.xti", "", false, obj.DriveName + "_RFC-S");

                   // ITcSmTreeItem virtualAxis = plcPousTask.ImportChild(templateDir + "\\Axis_Template.xti", "", false, obj.DriveName + "_Virt");






                }

            }



            CreateBeckhoffDrives();


            CreateVariableLinks();


            // Producing the Mapping info 
            ITcSysManager3 systemManager3 = (ITcSysManager3)SysManager;

            string mappingInfo = systemManager3.ProduceMappingInfo();

            systemManager3.ConsumeMappingInfo(mappingInfo);



            DrivesCreated = true;

        }


        /// <summary>
        /// 
        /// </summary>
        public void CreateVariableLinks() {

            //Build Solution:

            /* ------------------------------------------------------
             * Compile PLC project to generate module data
             * ------------------------------------------------------ */
            DTE.Solution.SolutionBuild.Build(true);

            int test = DTE.Solution.SolutionBuild.LastBuildInfo;

            //Verify if build is done successfully. 
            if (DTE.Solution.SolutionBuild.LastBuildInfo == 0) { 

            foreach (DriveAxisObj OBJ in DriveList) {

                #region locator helper
                ///////
                string ioMasterDevice = string.Empty;
                string ioNCMotion = string.Empty;

                //Master Ethercat locator

                ITcSmTreeItem ProjectEtherCat = SysManager.LookupTreeItem("TIID");

                if (ProjectEtherCat.ChildCount >= 0)
                {


                    for (int i = 1; i <= ProjectEtherCat.ChildCount; i++)
                    {


                        if (ProjectEtherCat.Child[i].ItemSubType == 111)
                        {

                            ioMasterDevice = ProjectEtherCat.Child[i].Name;
                        }




                    }

                }

                //NC Motion locator
                ITcSmTreeItem ncConfig = SysManager.LookupTreeItem("TINC");
                if (ncConfig.ChildCount > 0)
                {


                    for (int i = 0; i <= ncConfig.ChildCount; i++)
                    {
                        try
                        {

                            if (ncConfig.Child[i].ItemType == 1)
                            {

                                ioNCMotion = ncConfig.Child[i].Name;

                            }


                        }
                        catch (System.ArgumentException)
                        {

                        }


                    }

                }
                ///////////
                #endregion







                if (OBJ.SelectedDriveType == "Unidrive M700 RFC_A") {

                    string AxisPath = string.Empty;
                    string DrivePath = string.Empty;


                    string plcInputsPath = "TIPC^" + SelectedPLCName + "^" + SelectedPLCName + " Instance^PlcTask Inputs";
                    string plcOutputsPath = "TIPC^" + SelectedPLCName + "^" + SelectedPLCName + " Instance^PlcTask Outputs";


                    string ioInputsPath = "TIID^" + ioMasterDevice + "^" + OBJ.DriveName + "_RFC-A";
                    string ioOutputsPath = "TIID^" + ioMasterDevice + "^" + OBJ.DriveName + "_RFC-A";

                    if (OBJ.DriveInArray)
                    {

                        AxisPath = "MAIN.arrAxis";
                        DrivePath = "MAIN.arrUni_Drives";



                        //Linking Transmition PDO 3 variables
                        //Status word
                        SysManager.LinkVariables(ioInputsPath + "^" + "Transmit PDO Mapping 3^Status word",
                            plcInputsPath + "^" + DrivePath + "[" + OBJ.DesiredDriveIndex + "].unInStatusWord");



                        //Position Actual value
                        SysManager.LinkVariables(ioInputsPath + "^" + "Transmit PDO Mapping 3^Position actual value",
                            "TINC^" + ioNCMotion + "^Axes^" + OBJ.AxisName + "^Enc^Inputs^In^nDataIn1");



                        SysManager.LinkVariables(ioInputsPath + "^" + "Transmit PDO Mapping 3^Position actual value",
                       "TINC^" + ioNCMotion + "^Axes^" + OBJ.AxisName + "^Enc^Inputs^In^nDataIn2");




                        //Current Magnitude
                        SysManager.LinkVariables(ioInputsPath + "^" + "Transmit PDO Mapping 3^Current Magnitude",
                            plcInputsPath + "^" + DrivePath + "[" + OBJ.DesiredDriveIndex + "].dInActualCurrent");


                        //DC Bus Voltage
                        SysManager.LinkVariables(ioInputsPath + "^" + "Transmit PDO Mapping 3^DC Bus Voltage",
                            plcInputsPath + "^" + DrivePath + "[" + OBJ.DesiredDriveIndex + "].nInDcBusVoltage");





                        //Break Release
                        SysManager.LinkVariables(ioInputsPath + "^" + "Transmit PDO Mapping 3^Brake Release",
                            plcInputsPath + "^" + DrivePath + "[" + OBJ.DesiredDriveIndex + "].snInBrakeRelease");


                        //Input Register
                        SysManager.LinkVariables(ioInputsPath + "^" + "Transmit PDO Mapping 3^Input Register",
                             plcInputsPath + "^" + DrivePath + "[" + OBJ.DesiredDriveIndex + "].unInStatusWord");


                        //Torque Producing Current
                        SysManager.LinkVariables(ioInputsPath + "^" + "Transmit PDO Mapping 3^Torque Producing Current",
                             plcInputsPath + "^" + DrivePath + "[" + OBJ.DesiredDriveIndex + "].dInTorqueProducingCurrent");





                        //Magnetising Current
                        SysManager.LinkVariables(ioInputsPath + "^" + "Transmit PDO Mapping 3^Magnetising Current",
                             plcInputsPath + "^" + DrivePath + "[" + OBJ.DesiredDriveIndex + "].dInMagnetisingCurrent");


                        //Linking Receiving PDO 3 variables
                        //Control word
                        SysManager.LinkVariables(ioInputsPath + "^" + "Receive PDO Mapping 6^Control word",
                             plcOutputsPath + "^" + DrivePath + "[" + OBJ.DesiredDriveIndex + "].unOutControlWord");








                    }
                    else
                    {

                        AxisPath = "MAIN.";
                        DrivePath = "MAIN";



                        //Linking Transmition PDO 3 variables
                        //Status word
                        SysManager.LinkVariables(ioInputsPath + "^" + "Transmit PDO Mapping 3^Status word",
                            plcInputsPath + "^" + DrivePath + "." + OBJ.DriveName + ".unInStatusWord");


                        //Position Actual value
                        SysManager.LinkVariables(ioInputsPath + "^" + "Transmit PDO Mapping 3^Position actual value",
                            "TINC^" + ioNCMotion + "^Axes^" + OBJ.AxisName + "^Enc^Inputs^In^nDataIn1");



                        SysManager.LinkVariables(ioInputsPath + "^" + "Transmit PDO Mapping 3^Position actual value",
                       "TINC^" + ioNCMotion + "^Axes^" + OBJ.AxisName + "^Enc^Inputs^In^nDataIn2");


                        //Current Magnitude
                        SysManager.LinkVariables(ioInputsPath + "^" + "Transmit PDO Mapping 3^Current Magnitude",
                         plcInputsPath + "^" + DrivePath + "." + OBJ.DriveName + ".dInActualCurrent");


                        //DC Bus Voltage
                        SysManager.LinkVariables(ioInputsPath + "^" + "Transmit PDO Mapping 3^DC Bus Voltage",
                            plcInputsPath + "^" + DrivePath + "." + OBJ.DriveName + ".nInDcBusVoltage");



                        //Break Release
                        SysManager.LinkVariables(ioInputsPath + "^" + "Transmit PDO Mapping 3^Brake Release",
                            plcInputsPath + "^" + DrivePath + "." + OBJ.DriveName + ".snInBrakeRelease");


                        //Input Register
                        SysManager.LinkVariables(ioInputsPath + "^" + "Transmit PDO Mapping 3^Input Register",
                             plcInputsPath + "^" + DrivePath + "." + OBJ.DriveName + ".unInStatusWord");



                        //Torque Producing Current
                        SysManager.LinkVariables(ioInputsPath + "^" + "Transmit PDO Mapping 3^Torque Producing Current",
                             plcInputsPath + "^" + DrivePath + "." + OBJ.DriveName + ".dInTorqueProducingCurrent");


                        //Magnetising Current
                        SysManager.LinkVariables(ioInputsPath + "^" + "Transmit PDO Mapping 3^Magnetising Current",
                            plcInputsPath + "^" + DrivePath + "." + OBJ.DriveName + ".dInMagnetisingCurrent");


                        //Linking Receiving PDO 3 variables
                        //Control word
                        SysManager.LinkVariables(ioInputsPath + "^" + "Receive PDO Mapping 6^Control word",
                             plcOutputsPath + "^" + DrivePath + "." + OBJ.DriveName + ".unOutControlWord");











                    }
















                } else if (OBJ.SelectedDriveType == "Unidrive M700 RFC_S") {

                    string AxisPath = string.Empty;
                    string DrivePath = string.Empty;


                    string plcInputsPath = "TIPC^" + SelectedPLCName + "^" + SelectedPLCName + " Instance^PlcTask Inputs";
                    string plcOutputsPath = "TIPC^" + SelectedPLCName + "^" + SelectedPLCName + " Instance^PlcTask Outputs";


                    string ioInputsPath = "TIID^" + ioMasterDevice + "^" + OBJ.DriveName + "_RFC-S";
                    string ioOutputsPath = "TIID^" + ioMasterDevice + "^" + OBJ.DriveName + "_RFC-S";

                    if (OBJ.DriveInArray)
                    {

                        AxisPath = "MAIN.arrAxis";
                        DrivePath = "MAIN.arrUni_Drives";



                        //Linking Transmition PDO 3 variables
                        //Status word
                        SysManager.LinkVariables(ioInputsPath + "^" + "Transmit PDO Mapping 3^Status Word",
                            plcInputsPath + "^" + DrivePath + "[" + OBJ.DesiredDriveIndex + "].unInStatusWord");



                        //Position Actual value
                        SysManager.LinkVariables(ioInputsPath + "^" + "Transmit PDO Mapping 3^Position actual value",
                            "TINC^" + ioNCMotion + "^Axes^" + OBJ.AxisName + "^Enc^Inputs^In^nDataIn1");



                        SysManager.LinkVariables(ioInputsPath + "^" + "Transmit PDO Mapping 3^Position actual value",
                       "TINC^" + ioNCMotion + "^Axes^" + OBJ.AxisName + "^Enc^Inputs^In^nDataIn2");




                        //Current Magnitude
                        SysManager.LinkVariables(ioInputsPath + "^" + "Transmit PDO Mapping 3^Current actual value",
                            plcInputsPath + "^" + DrivePath + "[" + OBJ.DesiredDriveIndex + "].dInActualCurrent");


                        //DC Bus Voltage
                        SysManager.LinkVariables(ioInputsPath + "^" + "Transmit PDO Mapping 3^DC Bus Voltage",
                            plcInputsPath + "^" + DrivePath + "[" + OBJ.DesiredDriveIndex + "].nInDcBusVoltage");







                        //Break Release
                        SysManager.LinkVariables(ioInputsPath + "^" + "Transmit PDO Mapping 3^Brake Release",
                            plcInputsPath + "^" + DrivePath + "[" + OBJ.DesiredDriveIndex + "].snInBrakeRelease");


                        //Input Register
                        SysManager.LinkVariables(ioInputsPath + "^" + "Transmit PDO Mapping 3^Input Register",
                             plcInputsPath + "^" + DrivePath + "[" + OBJ.DesiredDriveIndex + "].unInStatusWord");




                        //Linking Receiving PDO 3 variables
                        //Control word
                        SysManager.LinkVariables(ioInputsPath + "^" + "Receive PDO Mapping 6^Control word",
                             plcOutputsPath + "^" + DrivePath + "[" + OBJ.DesiredDriveIndex + "].unOutControlWord");




                    }
                    else
                    {

                        AxisPath = "MAIN.";
                        DrivePath = "MAIN";



                        //Linking Transmition PDO 3 variables
                        //Status word
                        SysManager.LinkVariables(ioInputsPath + "^" + "Transmit PDO Mapping 3^Status Word",
                            plcInputsPath + "^" + DrivePath + "." + OBJ.DriveName + ".unInStatusWord");


                        //Position Actual value
                        SysManager.LinkVariables(ioInputsPath + "^" + "Transmit PDO Mapping 3^Position actual value",
                            "TINC^" + ioNCMotion + "^Axes^" + OBJ.AxisName + "^Enc^Inputs^In^nDataIn1");



                        SysManager.LinkVariables(ioInputsPath + "^" + "Transmit PDO Mapping 3^Position actual value",
                       "TINC^" + ioNCMotion + "^Axes^" + OBJ.AxisName + "^Enc^Inputs^In^nDataIn2");


                        //Current Magnitude
                        SysManager.LinkVariables(ioInputsPath + "^" + "Transmit PDO Mapping 3^Current actual value",
                         plcInputsPath + "^" + DrivePath + "." + OBJ.DriveName + ".dInActualCurrent");


                        //DC Bus Voltage
                        SysManager.LinkVariables(ioInputsPath + "^" + "Transmit PDO Mapping 3^DC Bus Voltage",
                            plcInputsPath + "^" + DrivePath + "." + OBJ.DriveName + ".nInDcBusVoltage");



                        //Break Release
                        SysManager.LinkVariables(ioInputsPath + "^" + "Transmit PDO Mapping 3^Brake Release",
                            plcInputsPath + "^" + DrivePath + "." + OBJ.DriveName + ".snInBrakeRelease");


                        //Input Register
                        SysManager.LinkVariables(ioInputsPath + "^" + "Transmit PDO Mapping 3^Input Register",
                             plcInputsPath + "^" + DrivePath + "." + OBJ.DriveName + ".unInStatusWord");






                        //Safe Speed
                        SysManager.LinkVariables(ioInputsPath + "^" + "Transmit PDO Mapping 3^Safe Speed",
                            plcInputsPath + "^" + DrivePath + "." + OBJ.DriveName + ".snInSafeSpeed");


                        //Linking Receiving PDO 3 variables
                        //Control word
                        SysManager.LinkVariables(ioInputsPath + "^" + "Receive PDO Mapping 6^Control word",
                             plcOutputsPath + "^" + DrivePath + "." + OBJ.DriveName + ".unOutControlWord");


                            



                    }


                    



                }
                else if (OBJ.SelectedDriveType == "BeckhoffTerm Drive") {

                    string AxisPath = string.Empty;
                    string DrivePath = string.Empty;


                    string plcInputsPath = "TIPC^" + SelectedPLCName + "^" + SelectedPLCName + " Instance^PlcTask Inputs";
                    string plcOutputsPath = "TIPC^" + SelectedPLCName + "^" + SelectedPLCName + " Instance^PlcTask Outputs";


                    string ioInputsPath = "TIID^" + ioMasterDevice + "^Term 3 (EK1200)^" + OBJ.DriveName + "_BeckH";
                    string ioOutputsPath = "TIID^" + ioMasterDevice + "^" + OBJ.DriveName + "_BeckH";


                    if (OBJ.DriveInArray)
                    {

                        AxisPath = "MAIN.arrAxis";
                        DrivePath = "MAIN.arrBeckhoff_Drives";


                        //Position
                        SysManager.LinkVariables(ioInputsPath + "^" + "FB Position^Position",
                            plcInputsPath + "^" + DrivePath + "[" + OBJ.DesiredDriveIndex + "].dInPositionFeedback");


                        //Statusword
                        SysManager.LinkVariables(ioInputsPath + "^" + "DRV Statusword^Statusword",
                            plcInputsPath + "^" + DrivePath + "[" + OBJ.DesiredDriveIndex + "].unInStatusWord");

                        //ActualCurrent
                        SysManager.LinkVariables(ioInputsPath + "^" + "DRV Info data 1^ActualCurrent",
                            plcInputsPath + "^" + DrivePath + "[" + OBJ.DesiredDriveIndex + "].unInActualCurrent");

                        //DcBusVoltage
                        SysManager.LinkVariables(ioInputsPath + "^" + "DRV Info data 2^DcBusVoltage",
                            plcInputsPath + "^" + DrivePath + "[" + OBJ.DesiredDriveIndex + "].unInDcBusVoltage");



                        //TP1 Input
                        SysManager.LinkVariables(ioInputsPath + "^" + "FB Touch probe status^TP1 Input",
                            plcInputsPath + "^" + DrivePath + "[" + OBJ.DesiredDriveIndex + "].bInDigitalInput1");


                        //TP2 Input
                        SysManager.LinkVariables(ioInputsPath + "^" + "FB Touch probe status^TP2 Input",
                            plcInputsPath + "^" + DrivePath + "[" + OBJ.DesiredDriveIndex + "].bInDigitalInput2");


                        //Controlword
                        SysManager.LinkVariables(ioInputsPath + "^" + "DRV Controlword^Controlword",
                            plcOutputsPath + "^" + DrivePath + "[" + OBJ.DesiredDriveIndex + "].unOutControlWord");



                        //Target Velocity
                        SysManager.LinkVariables(ioInputsPath + "^" + "DRV Target velocity^Target velocity",
                            plcOutputsPath + "^" + DrivePath + "[" + OBJ.DesiredDriveIndex + "].unOutControlWord");


                    }
                    else
                    {

                        AxisPath = "MAIN";
                        DrivePath = "MAIN";





                        //Position
                        SysManager.LinkVariables(ioInputsPath + "^" + "FB Position^Position",
                            plcInputsPath + "^" + DrivePath + "." + OBJ.DriveName + ".dInPositionFeedback");


                        //Statusword
                        SysManager.LinkVariables(ioInputsPath + "^" + "DRV Statusword^Statusword",
                              plcInputsPath + "^" + DrivePath + "." + OBJ.DriveName + ".unInStatusWord");

                        //ActualCurrent
                        SysManager.LinkVariables(ioInputsPath + "^" + "DRV Info data 1^ActualCurrent",
                             plcInputsPath + "^" + DrivePath + "." + OBJ.DriveName + ".unInActualCurrent");

                        //DcBusVoltage
                        SysManager.LinkVariables(ioInputsPath + "^" + "DRV Info data 2^DcBusVoltage",
                              plcInputsPath + "^" + DrivePath + "." + OBJ.DriveName + ".unInDcBusVoltage");



                        //TP1 Input
                        SysManager.LinkVariables(ioInputsPath + "^" + "FB Touch probe status^TP1 Input",
                              plcInputsPath + "^" + DrivePath + "." + OBJ.DriveName + ".bInDigitalInput1");


                        //TP2 Input
                        SysManager.LinkVariables(ioInputsPath + "^" + "FB Touch probe status^TP2 Input",
                              plcInputsPath + "^" + DrivePath + "." + OBJ.DriveName + ".bInDigitalInput2");


                        //Controlword
                        SysManager.LinkVariables(ioInputsPath + "^" + "DRV Controlword^Controlword",
                              plcOutputsPath + "^" + DrivePath + "." + OBJ.DriveName + ".unOutControlWord");



                        //Target Velocity
                        SysManager.LinkVariables(ioInputsPath + "^" + "DRV Target velocity^Target velocity",
                            plcOutputsPath + "^" + DrivePath + "." + OBJ.DriveName + ".unOutControlWord");



                            

                    }




                }
                else if (OBJ.SelectedDriveType == "Virtual Drive") {
                   
                }
                                
                }


        }else
            {

                System.Windows.MessageBox.Show("The Project has not successfully build.\nPlease remove all Axis and Drives created and restart the application!");


                FileStatus = "Only Drives and Axises Created! No variable Links created.";
            }
            FileStatus = "Drives and Axises Created and Variable Linked.";



        }



        /// <summary>
        /// 
        /// </summary>
        public void CreateBeckhoffDrives() {


            if (DriveBeckoffList.Count > 0)
            {
                string templateDir = Directory.GetCurrentDirectory();


            ITcSmTreeItem plcPousMasterDevice = SysManager.LookupTreeItem("TIID^" + EtherCatMasName);

            //Verify if the ethercat master has a Term3 item
            for (int i = 1; i <= plcPousMasterDevice.ChildCount; i++)
            {

                if (plcPousMasterDevice.Child[i].ItemSubType == 9099 && plcPousMasterDevice.Child[i].Name == "Term 3 (EK1200)")
                {

                    HasValidTerminal = true;
                    TerminalName = plcPousMasterDevice.Child[i].Name.ToString();
                    break;

                }


            }



           

                if (!HasValidTerminal)
                {
                    //Create the missing terminal
                    ITcSmTreeItem tempTerm = plcPousMasterDevice.CreateChild("Term 3 (EK1200)", 9099, "", "EK1200-5000-0000");
                    TerminalName = tempTerm.Name.ToString();
                    HasValidTerminal = true;

                }

                foreach (DriveAxisObj obj in DriveBeckoffList)
                {
                    ITcSmTreeItem tempTermin = SysManager.LookupTreeItem("TIID^" + EtherCatMasName + "^" + TerminalName + "^");

                  //  ITcSmTreeItem tempBeckhoff = tempTermin.CreateChild(obj.DriveName + "_BeckH", 403, "", "EL7211-9014-0026");


                    ITcSmTreeItem tempBeckhend = tempTermin.ImportChild(templateDir + "\\Beckhoff_EL7211.xti", "", false, obj.DriveName + "_BeckH");

                    


                }


                if (!HasValidEndTerminal)
                {
                    ITcSmTreeItem tempTermin = SysManager.LookupTreeItem("TIID^" + EtherCatMasName + "^" + TerminalName + "^");


                    ITcSmTreeItem tempBeckhend = tempTermin.ImportChild(templateDir + "\\Term 3 (EK1110).xti", "", false, "Term3 (EK1110)");

                    HasValidEndTerminal = true;

                }
            }


        }




        /// <summary>
        /// 
        /// </summary>
        public void CreateMainDriveAxisFile()
        {

            
           

            ////Main Drive Axis CREATIION/////////////////////
            //Create funtion block with array index. see example

            // Retreive that Min and Max desired axis value
            List<int> AxisIndexValueList = new List<int>();

            // Retreive that Min and Max desired drive value

            List<int> VirDriveIndexValueList = new List<int>();


            List<int> BeckDriveIndexValueList = new List<int>();

            List<int> UniDriveIndexValueList = new List<int>();

            List<int> AllDriveIndexValueList = new List<int>();





            foreach (DriveAxisObj obj in DriveList)
            {

                if (obj.AxisInArray) {

                    AxisIndexValueList.Add(obj.DesiredAxisIndex);


                }

                if (obj.DriveInArray) {
                    if (obj.SelectedDriveType == "BeckhoffTerm Drive") {

                        BeckDriveIndexValueList.Add(obj.DesiredDriveIndex);

                    }

                    if (obj.SelectedDriveType == "Unidrive M700 RFC_A" )
                    {

                        UniDriveIndexValueList.Add(obj.DesiredDriveIndex);

                    }

                    
                    if (obj.SelectedDriveType == "Unidrive M700 RFC_S")
                    {

                        UniDriveIndexValueList.Add(obj.DesiredDriveIndex);

                    }

                    if (obj.SelectedDriveType == "Virtual Drive")
                    {

                        VirDriveIndexValueList.Add(obj.DesiredDriveIndex);

                    }

                    AllDriveIndexValueList.Add(obj.DesiredDriveIndex);


                }


            }



            int[] indexAxisValuesArray;
            int[] indexAllDriveValuesArray;

            int[] indexVirDriveValuesArray;
            int[] indexBeckDriveValuesArray;
            int[] indexUniDriveValuesArray;


            if (AxisIndexValueList.Count > 0)
            {

                indexAxisValuesArray = AxisIndexValueList.ToArray();

            }
            else {

                indexAxisValuesArray = null;


            }




            if (VirDriveIndexValueList.Count > 0) {

                 indexVirDriveValuesArray = VirDriveIndexValueList.ToArray();

            }else {

                indexVirDriveValuesArray = null;

            }





            if (BeckDriveIndexValueList.Count > 0)
            {

                indexBeckDriveValuesArray = BeckDriveIndexValueList.ToArray();

            } else {
                indexBeckDriveValuesArray = null;

            }


            if (UniDriveIndexValueList.Count > 0)
            {

                indexUniDriveValuesArray = UniDriveIndexValueList.ToArray();

            }
            else {

                indexUniDriveValuesArray = null;


            }


            if (AllDriveIndexValueList.Count > 0)
            {

                indexAllDriveValuesArray = AllDriveIndexValueList.ToArray();

            }
            else
            {

                indexAllDriveValuesArray = null;


            }





            if (indexAxisValuesArray == null)
            {
            }
            else {

                MinIndexAxisValue = indexAxisValuesArray.Min();
                MaxIndexAxisValue = indexAxisValuesArray.Max();


            }




            if (indexVirDriveValuesArray == null)
            {
            }
            else
            {



                MinIndexVirtuDriveValue = indexVirDriveValuesArray.Min();
                MaxIndexVirtuDriveValue = indexVirDriveValuesArray.Max();

            }



            if (indexBeckDriveValuesArray == null)
            {
            }
            else
            {

                MinIndexBeckDriveValue = indexBeckDriveValuesArray.Min();
                MaxIndexBeckDriveValue = indexBeckDriveValuesArray.Max();
            }




            if (indexUniDriveValuesArray == null)
            {
            }
            else
            {
                MinIndexUniDriveValue = indexUniDriveValuesArray.Min();
                MaxIndexUniDriveValue = indexUniDriveValuesArray.Max();

            }


            if (indexAllDriveValuesArray == null)
            {
            }
            else
            {
                MinIndexAllDriveValue = indexAllDriveValuesArray.Min();
                MaxIndexAllDriveValue = indexAllDriveValuesArray.Max();

            }





            //This evalutate if the user only chose one drive 

            if (MaxIndexAxisValue == 1 && MinIndexAxisValue == 1)
            {

                MinIndexAxisValue = 0;
                MaxIndexAxisValue = 1;

            }


            if (MinIndexVirtuDriveValue == 1 && MaxIndexVirtuDriveValue == 1)
            {

                MinIndexVirtuDriveValue = 0;
                MaxIndexVirtuDriveValue = 1;

            }


            if (MinIndexBeckDriveValue == 1 && MaxIndexBeckDriveValue == 1)
            {

                MinIndexBeckDriveValue = 0;
                MaxIndexBeckDriveValue = 1;

            }


            if (MinIndexUniDriveValue == 1 && MaxIndexUniDriveValue == 1)
            {

                MinIndexUniDriveValue = 0;
                MaxIndexUniDriveValue = 1;

            }


            if (MinIndexAllDriveValue == 1 && MaxIndexAllDriveValue == 1)
            {

                MinIndexAllDriveValue = 0;
                MaxIndexAllDriveValue = 1;

            }



            //Create the Axis arra Main file .
            ITcSmTreeItem plcPousGVLItem = SysManager.LookupTreeItem("TIPC^" + SelectedPLCName + "^" + SelectedPLCName + " Project^POUs");

            string XMLfGVLAxis = "\n VAR \n //Axis Array\n arr_Axis : ARRAY[";

            string XMLfGVLAxis1 = "..";

            string XMLfGVLAxis2 = "] OF SCI_Motion.fbAxis;\n ";


            string TotalAXisString = XMLfGVLAxis + MinIndexAxisValue + XMLfGVLAxis1 + MaxIndexAxisValue + XMLfGVLAxis2;



            // Create the  bInhibit  GVL    
            string XMLfGVLbInhibit = "VAR_GLOBAL\n //Axis Array\n arr_bInhibit : ARRAY[";

            string XMLfGVLbInhibit1 = "..";

            string XMLfGVLbInhibit2 = "] OF BOOL;\n ";


            string TotalbInhibitString = XMLfGVLbInhibit + MinIndexAllDriveValue + XMLfGVLbInhibit1 + MaxIndexAllDriveValue + XMLfGVLbInhibit2;





            //Create individual axis
            foreach (DriveAxisObj obj in DriveList)
            {
                if (!obj.AxisInArray) {

                    string tempAxis = obj.AxisName + ": SCI_Motion.fbAxis;//Individual Axis \n ";


                    TotalAXisString = TotalAXisString + tempAxis;



                }

                if (!obj.DriveInArray) {

                    string tempInHibit = obj.DriveName + "_inhibit" + " AT %I*	: BOOL; \n  ";
                    TotalbInhibitString = TotalbInhibitString + tempInHibit;



                }

            }



            //Closing The GVL statement
            TotalbInhibitString = TotalbInhibitString + "END_VAR";

            ITcSmTreeItem selec = SysManager.LookupTreeItem("TIPC^" + SelectedPLCName + "^" + SelectedPLCName + " Project");

            for (int y = 1; y <= selec.ChildCount; y++)
                if ("GVLs" == selec.Child[y].Name.ToString())
                {

                    for (int q = 1; q <= selec.Child[y].ChildCount; q++)
                    {

                        if ("GVL" == selec.Child[y].Child[q].Name.ToString())
                        {

                            ITcSmTreeItem SelectedGVLFile = selec.Child[y].Child[q];

                            ITcPlcDeclaration fbpou3 = (ITcPlcDeclaration)SelectedGVLFile;

                            fbpou3.DeclarationText = TotalbInhibitString;

                        }


                    }



                }








            //create Array for Beckhoff drives
            string beckDriveArr_dec1 = "arrBeckhoff_Drives : ARRAY[";

            string beckDriveArr_dec2 = "] OF SCI_Motion.fbDriveBeckhoffTerm;\n ";

            string TotalBeckOffDrive = beckDriveArr_dec1 + MinIndexBeckDriveValue + XMLfGVLAxis1 + MaxIndexBeckDriveValue + beckDriveArr_dec2;



            ///////////////////////////////////////////////////////
            //create individual beckoff drive in Main.pou

            string BeckOffAssign = string.Empty;

            foreach (DriveAxisObj obj in DriveList)
            {
                if (!obj.DriveInArray && obj.SelectedDriveType == "BeckhoffTerm Drive" ) {

                    string tempBeckDrive = obj.DriveName + ": SCI_Motion.fbDriveBeckhoffTerm; //Individual Beckhoff Drive \n";

                    TotalBeckOffDrive = TotalBeckOffDrive + tempBeckDrive;                   

                    
                }



                if ( obj.SelectedDriveType == "BeckhoffTerm Drive")
                {

                

                    if (obj.AxisInArray)
                    {


                        if (obj.DriveInArray)
                        {


                            //BeckOffAssign = BeckOffAssign + "MAIN.arr_Axis[" + obj.DesiredAxisIndex + "].sAxisName :=" + "'" + obj.DriveName + "'" + ";\n" +
                            //    "MAIN.arr_Axis[" + obj.DesiredAxisIndex + "].oDrive :=" + " MAIN.arrBeckhoff_Drives[" + obj.DesiredDriveIndex + "];\n" +
                            //   "MAIN.arr_Axis[" + obj.DesiredAxisIndex + "].bInhibit :=" + " GVL.arr_bInhibit[" + obj.DesiredAxisIndex + "];\n";


                            BeckOffAssign = BeckOffAssign + "MAIN.arr_Axis[" + obj.DesiredAxisIndex + "].sAxisName :=" + "'" + obj.DriveName + "'" + ";\n" +
                             "MAIN.arr_Axis[" + obj.DesiredAxisIndex + "].oDrive :=" + " MAIN.arrBeckhoff_Drives[" + obj.DesiredDriveIndex + "];\n" +
                            "MAIN.arr_Axis[" + obj.DesiredAxisIndex + "].bInhibit :=" + " GVL.arr_bInhibit[" + obj.DesiredAxisIndex + "];\n";


                        }
                        else
                        {


                            BeckOffAssign = BeckOffAssign + "MAIN.arr_Axis[" + obj.DesiredAxisIndex + "].sAxisName :=" + "'" + obj.DriveName + "'" + ";\n" +
                                "arr_Axis[" + obj.DesiredAxisIndex + "].oDrive :=" + " MAIN." + obj.DriveName + ";\n" +
                                "MAIN.arr_Axis[" + obj.DesiredAxisIndex + "].bInhibit :=" + " GVL." + obj.DriveName + "_inhibit;\n";


                        }





                    }
                    else
                    {



                        if (obj.DriveInArray)
                        {


                            BeckOffAssign = BeckOffAssign + "MAIN." + obj.AxisName + ".sAxisName :=" + "'" + obj.DriveName + "'" + ";\n" +
                                "MAIN." + obj.AxisName + ".oDrive :=" + " MAIN.arrBeckhoff_Drives[" + obj.DesiredDriveIndex + "];\n" +
                                 "MAIN." + obj.AxisName + ".bInhibit :=" + " GVL.arr_bInhibit[" + obj.DesiredAxisIndex + "];\n";




                        }
                        else
                        {


                            BeckOffAssign = BeckOffAssign + "MAIN." + obj.AxisName + ".sAxisName :=" + "'" + obj.DriveName + "'" + ";\n" +
                                "MAIN." + obj.AxisName + ".oDrive :=" + obj.DriveName + ";\n" +
                                 "MAIN." + obj.AxisName + ".bInhibit :=" + " GVL." + obj.DriveName + "_inhibit;\n";


                        }




                    }
              

                }
            }
            /////////////////////////////////////////////////////////



            //create Array for Virtual Drive 
            string VirkDriveArr_dec1 = "arrVirtual_Drives : ARRAY[";

            string VirDriveArr_dec2 = "] OF SCI_Motion.fbDriveVirtual;\n ";

            string TotalVirtualDrive = VirkDriveArr_dec1 + MinIndexVirtuDriveValue + XMLfGVLAxis1 + MaxIndexVirtuDriveValue + VirDriveArr_dec2;


            //create individual Virtual Drive in Main.pou

            string VirtualDriveAssign = string.Empty;


            foreach (DriveAxisObj obj in DriveList)
            {
                if (!obj.DriveInArray && obj.SelectedDriveType == "Virtual Drive")
                {

                    string tempVirtualDrive = obj.DriveName + ": SCI_Motion.fbDriveVirtual; //Individual Virtual Drive \n";

                    TotalVirtualDrive = TotalVirtualDrive + tempVirtualDrive;

                    



                }


                if (obj.SelectedDriveType == "Virtual Drive")
                {

                    if (obj.AxisInArray)
                    {



                        if (obj.DriveInArray)
                        {


                            VirtualDriveAssign = VirtualDriveAssign + "MAIN.arr_Axis[" + obj.DesiredAxisIndex + "].sAxisName :=" + "'" + obj.DriveName + "'" + ";\n" +
                                "MAIN.arr_Axis[" + obj.DesiredAxisIndex + "].oDrive :=" + " MAIN.arrVirtual_Drives[" + obj.DesiredDriveIndex + "];\n" +
                               "MAIN.arr_Axis[" + obj.DesiredAxisIndex + "].bInhibit :=" + " GVL.arr_bInhibit[" + obj.DesiredAxisIndex + "];\n";




                        }
                        else
                        {


                            VirtualDriveAssign = VirtualDriveAssign + "MAIN.arr_Axis[" + obj.DesiredAxisIndex + "].sAxisName :=" + "'" + obj.DriveName + "'" + ";\n" +
                                "arr_Axis[" + obj.DesiredAxisIndex + "].oDrive :=" + " MAIN." + obj.DriveName + ";\n" +
                                "MAIN.arr_Axis[" + obj.DesiredAxisIndex + "].bInhibit :=" + " GVL." + obj.DriveName + "_inhibit;\n";


                        }





                    }
                    else
                    {



                        if (obj.DriveInArray)
                        {


                            VirtualDriveAssign = VirtualDriveAssign + "MAIN." + obj.AxisName + ".sAxisName :=" + "'" + obj.DriveName + "'" + ";\n" +
                                "MAIN." + obj.AxisName + ".oDrive :=" + " MAIN.arrVirtual_Drives[" + obj.DesiredDriveIndex + "];\n" +
                                 "MAIN." + obj.AxisName + ".bInhibit :=" + " GVL.arr_bInhibit[" + obj.DesiredAxisIndex + "];\n";




                        }
                        else
                        {


                            VirtualDriveAssign = VirtualDriveAssign + "MAIN." + obj.AxisName + ".sAxisName :=" + "'" + obj.DriveName + "'" + ";\n" +
                                "MAIN." + obj.AxisName + ".oDrive :=" + obj.DriveName + ";\n" +
                                 "MAIN." + obj.AxisName + ".bInhibit :=" + " GVL." + obj.DriveName + "_inhibit;\n";


                        }




                    }


                }




            }




            //create Array for Unidrive Drive 
            string UniDriveArr_dec1 = "arrUni_Drives : ARRAY[";

            string UniDriveArr_dec2 = "] OF SCI_Motion.fbDriveUnidriveM700;\n ";

            string TotalUniDrive = UniDriveArr_dec1 + MinIndexUniDriveValue + XMLfGVLAxis1 + MaxIndexUniDriveValue + UniDriveArr_dec2;


            //create individual Virtual Drive in Main.pou

            string UniDriveAssign = string.Empty;


            foreach (DriveAxisObj obj in DriveList)
            {
                if (!obj.DriveInArray )
                {

                    if (obj.SelectedDriveType == "Unidrive M700 RFC_S" || obj.SelectedDriveType == "Unidrive M700 RFC_A") {

                        string tempUniDrive = obj.DriveName + ": SCI_Motion.fbDriveUnidriveM700; //Individual UniDrive Drive \n";

                        TotalUniDrive = TotalUniDrive + tempUniDrive;

                                            


                    }


                    

                }



                if (obj.SelectedDriveType == "Unidrive M700 RFC_S" || obj.SelectedDriveType == "Unidrive M700 RFC_A")
                {



                    if (obj.AxisInArray)
                    {


                        if (obj.DriveInArray)
                        {


                            UniDriveAssign = UniDriveAssign + "MAIN.arr_Axis[" + obj.DesiredAxisIndex + "].sAxisName :=" + "'" + obj.DriveName + "'" + ";\n" +
                                "MAIN.arr_Axis[" + obj.DesiredAxisIndex + "].oDrive :=" + " MAIN.arrUni_Drives[" + obj.DesiredDriveIndex + "];\n" +
                               "MAIN.arr_Axis[" + obj.DesiredAxisIndex + "].bInhibit :=" + " GVL.arr_bInhibit[" + obj.DesiredAxisIndex + "];\n";




                        }
                        else
                        {


                            UniDriveAssign = UniDriveAssign + "MAIN.arr_Axis[" + obj.DesiredAxisIndex + "].sAxisName :=" + "'" + obj.DriveName + "'" + ";\n" +
                                "arr_Axis[" + obj.DesiredAxisIndex + "].oDrive :=" + " MAIN." + obj.DriveName + ";\n" +
                                "MAIN.arr_Axis[" + obj.DesiredAxisIndex + "].bInhibit :=" + " GVL." + obj.DriveName + "_inhibit;\n";


                        }





                    }
                    else
                    {



                        if (obj.DriveInArray)
                        {


                            UniDriveAssign = UniDriveAssign + "MAIN." + obj.AxisName + ".sAxisName :=" + "'" + obj.DriveName + "'" + ";\n" +
                                "MAIN." + obj.AxisName + ".oDrive :=" + " MAIN.arrUni_Drives[" + obj.DesiredDriveIndex + "];\n" +
                                 "MAIN." + obj.AxisName + ".bInhibit :=" + " GVL.arr_bInhibit[" + obj.DesiredAxisIndex + "];\n";




                        }
                        else
                        {


                            UniDriveAssign = UniDriveAssign + "MAIN." + obj.AxisName + ".sAxisName :=" + "'" + obj.DriveName + "'" + ";\n" +
                                "MAIN." + obj.AxisName + ".oDrive :=" + obj.DriveName + ";\n" +
                                 "MAIN." + obj.AxisName + ".bInhibit :=" + " GVL." + obj.DriveName + "_inhibit;\n";


                        }




                    }

                }





            }



            string TotalAssign = BeckOffAssign + VirtualDriveAssign + UniDriveAssign;


            


            ITcPlcDeclaration fbpou2 = (ITcPlcDeclaration)CurrentITCMAIN;

            fbpou2.DeclarationText = fbpou2.DeclarationText + TotalAXisString + TotalBeckOffDrive + TotalUniDrive + TotalVirtualDrive + "END_VAR";

            ITcPlcImplementation Selec = (ITcPlcImplementation)CurrentITCMAIN;

            Selec.ImplementationText = Selec.ImplementationText + TotalAssign;



        }


        public void OpenRunningSolution(object parameter) {

           // Hashtable result = GetRunningObjectTable();

            OpenFileDialog openFile = new OpenFileDialog();          


            openFile.Title = "Open TwinCat Solution";
            openFile.Filter = "TwinCat Solution (*.sln)|*.sln|All files (*.*)|*.*";

            openFile.ShowDialog();

            if (openFile.FileName == string.Empty) { }
            else
            {


                DTE = attachToExistingDte(openFile.FileName, "VisualStudio.DTE." + VSVersionNum);

               

                if (DTE == null)
                {

                    System.Windows.MessageBox.Show("Their is no Visual Studio is running with " + openFile.SafeFileName +
                        ". Or Verify that you have chosen the correct Visual Studio version Please re-do your action.", 
                        "Important Message",MessageBoxButton.OK , MessageBoxImage.Warning);

                    OpenRunningSolution(null);


                }
                else {

                    DTE.SuppressUI = false;
                    // DTE.UserControl = false;
                    DTE.MainWindow.Visible = true;


                    EnvDTE.Solution solution;


                    solution = DTE.Solution;

                    

                    Project = solution.Projects.Item(1);



                    SysManager = Project.Object;

                
                    SolutionDirectory = Path.GetDirectoryName(Path.GetDirectoryName(Project.filename));
                    SolutionName = Path.GetFileNameWithoutExtension(solution.FileName);



                    FileIsLoaded = true;
                    FileName = Project.Name;

                    //Verify the structurc of the connected file
                    VerifyProjectStruct();

                   CurrentITCMAIN = GetCurrentMainFile(SysManager, SelectedPLCName, "MAIN");

                }

              
               
            }



        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sysman"></param>
        /// <param name="selectPLCName"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static ITcSmTreeItem GetCurrentMainFile(ITcSysManager sysman, string selectPLCName, string name) {


            ITcSmTreeItem plcProjectRoot = sysman.LookupTreeItem("TIPC^" + selectPLCName + "^" + selectPLCName + " Project");


            TreeviewItemFinder.Helpers tv = new TreeviewItemFinder.Helpers(plcProjectRoot);

            return tv.GetItemByTypeName(TREEITEMTYPES.TREEITEMTYPE_PLCPOUPROG, name);

        }





        #region Attached to DTE Helper
        /// <summary>
        /// 
        /// </summary>
        /// <param name="solutionPath"></param>
        /// <param name="progId"></param>
        /// <returns></returns>
        public static EnvDTE.DTE attachToExistingDte(string solutionPath, string progId)
        {
            EnvDTE.DTE dte = null;
            try
            {
               
                Hashtable dteInstances = Helper.GetIDEInstances(false, progId);
                IDictionaryEnumerator hashtableEnumerator = dteInstances.GetEnumerator();

                while (hashtableEnumerator.MoveNext())
                {
                    EnvDTE.DTE dteTemp = (EnvDTE.DTE)hashtableEnumerator.Value;
                    if (dteTemp.Solution.FullName.ToUpper() == solutionPath.ToUpper())
                    {
                        Console.WriteLine("Found solution in list of all open DTE objects. " + dteTemp.Name);
                        dte = dteTemp;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            return dte;


        }


        #endregion
        
        


        /// <summary>
        /// 
        /// </summary>
        public void VerifyProjectStruct() {

            FileStatus = "Verifying selected file";

            //PLC Verification
            ITcSmTreeItem plcPousItem = SysManager.LookupTreeItem("TIPC");

            if (plcPousItem.ChildCount >= 0)
            {
                
                if (plcPousItem.ChildCount > 1) {
                    //Select wich PLC to verifiy 

                    System.Windows.Window PLCSelectiontWindow = new PLCSelectionWindow();

                     var dataC = new PLCSelectionViewModel(SelectedPLCName, SysManager);

                    PLCSelectiontWindow.DataContext = dataC;

                    PLCSelectiontWindow.ShowDialog();

                    SelectedPLCName = dataC.SelectedPLCName;
                    

                    //Check References
                    ITcSmTreeItem references = SysManager.LookupTreeItem("TIPC^" + SelectedPLCName +
                        "^" + SelectedPLCName + " Project^References");

                    var list4 = new List<string>();
                    for (int t = 1; t <= references.ChildCount; t++)
                    {

                        list4.Add(references.Child[t].Name.ToString());

                    }

                    if (list4.Contains("Tc2_Standard") &&
                        list4.Contains("Tc2_System") &&
                        list4.Contains("Motion") &&
                        list4.Contains("Tc3_Module") &&
                        list4.Contains("Tc2_MC2") &&
                        list4.Contains("MC") &&
                        list4.Contains("Tc2_EtherCAT") &&
                        list4.Contains("Tc2_Utilities"))
                    {

                        HasValidReference = true;

                    }


                    //looking for gvl anywhere
                    ITcSmTreeItem selec = SysManager.LookupTreeItem("TIPC^" + SelectedPLCName + "^" + SelectedPLCName + " Project");

                   

                    for (int y = 1; y <= selec.ChildCount; y++)
                        if ("GVLs" == selec.Child[y].Name.ToString())
                        {
                            var list5 = new List<string>();

                            for (int q = 1; q <= selec.Child[y].ChildCount; q++)
                            {

                                list5.Add(selec.Child[y].Name.ToString());

                            }

                            if (list5.Contains("GVL")) {

                                HasValidGVL = true;
                            }

                        }




                    //Evalute EtherCat
                    ITcSmTreeItem ProjectEtherCat4 = SysManager.LookupTreeItem("TIID");

                    if (ProjectEtherCat4.ChildCount >= 0)
                    {


                        for (int i = 1; i <= ProjectEtherCat4.ChildCount; i++)
                        {


                            if (ProjectEtherCat4.Child[i].ItemSubType == 111)
                            {

                                HasValidEtherCat = true;
                                break;
                            }
                            else
                            {

                                HasValidEtherCat = false;

                            }



                        }


                    }

                    

                    if (HasValidReference && HasValidGVL && HasValidEtherCat)
                    {

                        FileStatus = "The selected file has the correct PLC file structure";
                        HasValidPLC = true;

                    }
                    else
                    {

                        FileStatus = "The selected file does not have the correct PLC file structure";



                    }

                }
                else if (plcPousItem.ChildCount == 1 ) {

                    SelectedPLCName = plcPousItem.Child[1].Name;
                    
                                        

                  

                    //Check References
                    ITcSmTreeItem references = SysManager.LookupTreeItem("TIPC^" + SelectedPLCName +
                        "^" + SelectedPLCName + " Project^References");

                    var list4 = new List<string>();
                    for (int t = 1; t <= references.ChildCount; t++)
                    {

                        list4.Add(references.Child[t].Name.ToString());
                    }

                    if (list4.Contains("Tc2_Standard") &&
                        list4.Contains("Motion") &&
                        list4.Contains("Tc2_System") &&
                        list4.Contains("Tc3_Module") &&
                        list4.Contains("Tc2_MC2") &&
                        list4.Contains("MC") &&
                        list4.Contains("Tc2_EtherCAT") &&
                        list4.Contains("Tc2_Utilities"))
                    {

                        HasValidReference = true;

                    }


                    ITcSmTreeItem selec = SysManager.LookupTreeItem("TIPC^" + SelectedPLCName + "^" + SelectedPLCName + " Project");

                    //ITcSmTreeItem GVLItem = TreviewItemFinder.TreviewItemFinder.GetItemByTypeName(selec, TREEITEMTYPES.TREEITEMTYPE_PLCGVL, "GVL");


                    for (int y = 1; y <= selec.ChildCount; y++)
                        if ("GVLs" == selec.Child[y].Name.ToString())
                        {
                            var list5 = new List<string>();

                            for (int q = 1; q <= selec.Child[y].ChildCount; q++)
                            {

                                list5.Add(selec.Child[y].Child[q].Name.ToString());

                            }

                            if (list5.Contains("GVL"))
                            {

                                HasValidGVL = true;
                            }

                        }




                    if (HasValidReference && HasValidGVL)
                    {

                        FileStatus = "The selected file has the correct PLC file structure";
                        HasValidPLC = true;

                    }
                    else
                    {

                        FileStatus = "The selected file does not have the correct PLC file structure";

                    }

                }
               
            }
            else {

                FileStatus = "Their is no PLC attached to this Project!";


            }


            //Evaluate if their is a NCTask
            ITcSmTreeItem ncConfig = SysManager.LookupTreeItem("TINC");
            if (ncConfig.ChildCount > 0)
            {



                for (int i = 0; i <= ncConfig.ChildCount; i++) {
                    try
                    {

                        if (ncConfig.Child[i].ItemType == 1)
                        {

                            HasValidMotion = true;
                            
                        }
                        else
                        {


                            HasValidMotion = false;

                        }


                    }
                    catch (System.ArgumentException)
                    {
                                            
                    }
                                                   

                 }
                
            }
            else {

                FileStatus = "Their is no Motion attached to this Project!";

                HasValidMotion = false;

            }


            //Evalute EtherCat
            ITcSmTreeItem ProjectEtherCat = SysManager.LookupTreeItem("TIID");

            if (ProjectEtherCat.ChildCount >= 0) {


                for (int i = 1; i <= ProjectEtherCat.ChildCount; i++)
                {


                    if (ProjectEtherCat.Child[i].ItemSubType == 111)
                    {

                        HasValidEtherCat = true;
                        break;
                    }
                    else
                    {

                        HasValidEtherCat = false;

                    }



                }


            }

            //Verifucation done send info to user
            if (HasValidPLC && HasValidMotion && HasValidEtherCat)
            {

                FileStatus = "The selected file has the correct  project structure";

            }
            else
            {

                FileStatus = "The selected file does not meet Show Canada's project structure";

                //ask user if want to fix issues.

                var result = System.Windows.Forms.MessageBox.Show("The selected file does not meet Show Canada's standard. Do you want to fix the issues?",
                    "TwinCat Project Verification",
                     MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk);



                if (result.ToString() =="Yes")
                {

                   
                    FixingCurrentProjet();


                    FileStatus = "The selected file has the correct  project structure";

                }
                else
                {



                }
            }

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        void OpenHelpFile(object parameter) {
            string templateDir = Directory.GetCurrentDirectory();
            string completePath = templateDir + "\\Help.pdf";
            
            System.Diagnostics.Process.Start(@completePath);


        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        void CreateNewProject(object parameter) {

            

            FolderBrowserDialog FolderDialog = new FolderBrowserDialog();

            FolderDialog.Description = "Please select directory where your new solution would be.";

            FolderDialog.ShowDialog();
            if (FolderDialog.SelectedPath == "")
            {






            }
            else {


                 Type t = System.Type.GetTypeFromProgID("VisualStudio.DTE." + VSVersionNum);
                 DTE = (EnvDTE.DTE)Activator.CreateInstance(t);


                 DTE.SuppressUI = false;
                 DTE.UserControl = true;
                 //DTE.MainWindow.Visible = true;

                 dynamic solution = DTE.Solution;
                 SolutionDirectory = FolderDialog.SelectedPath;



                 string pathStrng = FolderDialog.SelectedPath + @"\MySolution1.sln";
                 string pathStrng1 = FolderDialog.SelectedPath + @"\MySolution1";

                 solution.Create(@FolderDialog.SelectedPath, "MySolution1");
                 solution.SaveAs(@pathStrng);

                 string template = @"C:\TwinCAT\3.1\Components\Base\PrjTemplate\TwinCAT Project.tsproj"; //path to project template

                 Project = solution.AddFromTemplate(template, @pathStrng1, "MyProject");

                
                 SolutionName = Path.GetFileNameWithoutExtension(solution.FileName);


                 SysManager = Project.Object;

                 CreatePLC_obj(SysManager);

                 CreateGVL(SysManager);

                 AddReference(SysManager);

                 CreateMotion_NCTask(SysManager);


                 CreateEtheCatDevice(SysManager);


                 FileName = Project.Name;

                 FileStatus = "The selected file has the correct  project structure";

                 FileIsLoaded = true;

                 Project.Save();
                 solution.SaveAs(@pathStrng);
               


            }


           
            

        }

        /// <summary>
        /// 
        /// </summary>
        void FixingCurrentProjet() {


            //PLC Verification
            ITcSmTreeItem plcPousItem = SysManager.LookupTreeItem("TIPC");
            if (plcPousItem.ChildCount == 0)
            {
                CreatePLC_obj(SysManager);

            }

            //PLC Fixing
            if (!HasValidPLC)
            {

                if (!HasValidGVL) {
                    CreateGVL(SysManager);

                }

                if (!HasValidReference)
                {

                    AddReference(SysManager);

                }


            }
            

            if (!HasValidMotion) {


                CreateMotion_NCTask(SysManager);


            }



            if (!HasValidEtherCat) {

                CreateEtheCatDevice(SysManager);


            }

        }

        private void CreateGVL(ITcSysManager sysManager)
        {
            try
            {
                ITcSmTreeItem selec = SysManager.LookupTreeItem("TIPC^" + SelectedPLCName + "^" + SelectedPLCName + " Project");

                for (int y = 1; y <= selec.ChildCount; y++)
                    if ("GVLs" == selec.Child[y].Name.ToString())
                    {

                        ITcSmTreeItem gvl = selec.Child[y].CreateChild("GVL", 615);
                    }
            }
            catch (System.Runtime.InteropServices.COMException)
            {


                CreateGVL(SysManager);

            }
        }




        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        void ExitApplication(object parameter) {



            System.Windows.Application.Current.Shutdown();
            
        }



     
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sysManager"></param>
        public void CreatePLC_obj(ITcSysManager sysManager) {

            //Create PLC from scratch
            ITcSmTreeItem plc = sysManager.LookupTreeItem("TIPC");

            ITcSmTreeItem newProject = plc.CreateChild("Plc1", 0, "", "");

            SelectedPLCName = newProject.Name;
        }
        
   

       
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sysManager"></param>
        public void AddReference(ITcSysManager sysManager) {
            try
            {
                ITcSmTreeItem references = sysManager.LookupTreeItem("TIPC^" + SelectedPLCName + "^" + SelectedPLCName + " Project^References");
                ITcPlcLibraryManager libManager = (ITcPlcLibraryManager)references;


                List<string> listRef = new List<string>();
                List<string> listLibrary = new List<string>();
                List<string> listRepo = new List<string>();



                for (int z = 1; z <= references.ChildCount; z++)
                {

                    listRef.Add(references.Child[z].Name.ToString());


                }


                //verify if reference exist already

                if (!listRef.Contains("Tc2_MC2")) {
                    libManager.AddLibrary("Tc2_MC2", "*", "Beckhoff Automation GmbH");

                }


                if (!listRef.Contains("Tc2_Utilities"))
                {
                    libManager.AddLibrary("Tc2_Utilities", "*", "Beckhoff Automation GmbH");


                }

                if (!listRef.Contains("Tc2_EtherCAT"))
                {
                    libManager.AddLibrary("Tc2_EtherCAT", "*", "Beckhoff Automation GmbH");


                }





                string templateDir = Directory.GetCurrentDirectory();

               


                if (!listRef.Contains("MC"))
                {


                    if (libManager.Repositories.Count > 0) {

                        for (int y = 0; y <= libManager.Repositories.Count -1 ; y++) {

                            listRepo.Add(libManager.Repositories[y].Name.ToString());


                        }

                    }

                    if (!listRepo.Contains("TNM"))
                    {

                        string newRepoPath1 = @"C:\TwinCat\TestRepository";
                        Directory.CreateDirectory(newRepoPath1);
                        libManager.InsertRepository("TNM", newRepoPath1, 0);

                      

                    }

                
                    libManager.InstallLibrary("TNM", templateDir + "\\Objs\\TNM-MC.library", true);
                    libManager.InstallLibrary("TNM", templateDir + "\\Objs\\TNM-MC.library", true);

                    libManager.AddLibrary("TNM-MC", "*", "STE Trekwerk B.V.");






                    listRepo.Clear();


                }





                if (!listRef.Contains("Motion"))
                {


                    if (libManager.Repositories.Count > 0)
                    {

                        for (int y = 0; y <= libManager.Repositories.Count - 1; y++)
                        {

                            listRepo.Add(libManager.Repositories[y].Name.ToString());


                        }

                    }


                    if (!listRepo.Contains("SCI Motion Library"))
                    {

                        string newRepoPath1 = @"C:\TwinCat\TestRepository";
                        Directory.CreateDirectory(newRepoPath1);
                        libManager.InsertRepository("SCI Motion Library", newRepoPath1, 0);

                        libManager.InstallLibrary("SCI Motion ", templateDir + "\\Objs\\SCI_Motion.library", true);
                        libManager.InstallLibrary("SCI Motion ", templateDir + "\\Objs\\SCI_Motion.library", true);

                    }

                    

                   



                    libManager.AddLibrary("SCI.Motion", "*", "Show Canada");

                    
                }


                // Change properties to Qualififed access only!!


            }
            catch (System.Runtime.InteropServices.COMException) {


                AddReference(SysManager);

            }
        }






        /// <summary>
        /// 
        /// </summary>
        /// <param name="sysManager"></param>
        public void CreateEtheCatDevice(ITcSysManager sysManager) {
            //EtherCat Device Code
            try { 
                

                ITcSmTreeItem ProjectEtherCat = SysManager.LookupTreeItem("TIID");



                if (ProjectEtherCat.ChildCount > 0)
                {


                    for (int i = 1; i <= ProjectEtherCat.ChildCount; i++)
                    {


                        if (ProjectEtherCat.Child[i].ItemSubType == 111)
                        {

                            HasValidEtherCat = true;
                            break;
                        }
                        else
                        {


                            ITcSmTreeItem NewDevice = ProjectEtherCat.CreateChild("EtherCAT Master", 111, null, null);


                        }



                    }


                }
                else {



                    ITcSmTreeItem NewDevice = ProjectEtherCat.CreateChild("EtherCAT Master", 111, null, null);


                }



            }
            catch (System.Runtime.InteropServices.COMException) {


                CreateEtheCatDevice(SysManager);





            }


        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sysManager"></param>
        public void CreateMotion_NCTask(ITcSysManager sysManager) {
            try { 

                ITcSmTreeItem ncConfig = sysManager.LookupTreeItem("TINC");

                //Verify if 

                if (ncConfig.ChildCount > 0)
                {



                    for (int i = 0; i <= ncConfig.ChildCount; i++)
                    {
                        try
                        {

                            if (ncConfig.Child[i].ItemType == 1)
                            {



                            }
                            else
                            {


                                ncConfig.CreateChild("NC-Task 1 SAF", 1);


                            }


                        }
                        catch (System.ArgumentException)
                        {

                        }


                    }

                }
                else {



                    ncConfig.CreateChild("NC-Task 1 SAF", 1);



                }



            }
            catch (System.Runtime.InteropServices.COMException) {
                CreateMotion_NCTask(SysManager);

            }
        }




        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        public void CreateDriveList(object parameter)
        {


            if (DriveList.Count == NumberOfDrive)
            {
                //clear table 
            }
            else
            {
                CreateTable();

            }

            DrivesConfigured = false;

        }

        /// <summary>
        /// 
        /// </summary>
        public void CreateTable()
        {
            if (DriveList.Count == NumberOfDrive)
            {


            }
            else
            {
                if (DriveList.Count > 0)
                {

                    DriveList.Clear();


                }
                

                for (int i = 1; i <= NumberOfDrive; i++)
                {
                    var temp = new DriveAxisObj();

                    temp.AxisInArray = true;

                    temp.DriveInArray = true;

                    temp.DesiredAxisIndex = i;

                    temp.DriveName = "Drive" + i;

                    temp.AxisName = "Axis" + temp.DesiredAxisIndex;

                    temp.DesiredDriveIndex = i;


                    DriveList.Add(temp);

                }

            }

        }

        #endregion


        #region INotifyPropertyChanged Members

        /// <summary>
        /// Need to implement this interface in order to get data binding
        /// to work properly.
        /// </summary>
        /// <param name="propertyName"></param>
        private void NotifyPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public bool PreFilterMessage(ref Message m)
        {
            throw new NotImplementedException();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion
    }
}
