﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;
using TwinCat_Automation_Interface2.Objs;

namespace TwinCat_Automation_Interface2.Validation
{
   public class IndexNumberValidation : ValidationRule
    {



        public CollectionViewSource CurrentCollection { get; set; }

    

        /// <summary>
        /// Validates the proposed value.
        /// </summary>
        /// <param name="value">The proposed value.</param>
        /// <param name="cultureInfo">A CultureInfo.</param>
        /// <returns>The result of the validation.</returns>
        public override ValidationResult Validate(object value, System.Globalization.CultureInfo cultureInfo)
        {
            if (value != null)
            {

                ObservableCollection<DriveAxisObj> currentList = (ObservableCollection<DriveAxisObj>) CurrentCollection.Source;
                List<int> ListIndex = new List<int>();


                //Populate List
                foreach (var DriveObj in currentList)
                {

                    ListIndex.Add(DriveObj.DesiredAxisIndex);

                }




                if (ListIndex.Count != ListIndex.Distinct().Count()) {



                    return new ValidationResult(false, "The index number must be unique.");

                }
                    







            }

            // Everything OK.
            return new ValidationResult(true, null);
        }
    }



}

