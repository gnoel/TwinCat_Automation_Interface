﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TCatSysManagerLib;

namespace TwinCat_Automation_Interface2.Helpers
{
    class TreeViewFinder
    {
        public static ITcSmTreeItem GetItemByTypeName(ITcSmTreeItem root, TREEITEMTYPES type, string Name)
        {
            return GetTreeItemsbyType(root, type).Find(x => x.Name == Name);
        }

        public static List<ITcSmTreeItem> GetTreeItemsbyType(ITcSmTreeItem root, TREEITEMTYPES type)
        {
            List<ITcSmTreeItem> temp = new List<ITcSmTreeItem>();
            if (root.Name.Contains("POU"))
            {
                ;
            }
            if ((TREEITEMTYPES)root.ItemType == type)
            {
                temp.Add(root);
            }
            if (root.ChildCount > 0)
            {

                for (int i = 1; i <= root.ChildCount; i++)
                {

                    List<ITcSmTreeItem> tempList = new List<ITcSmTreeItem>();

                    tempList = GetTreeItemsbyType(root.Child[i], type);

                    temp.AddRange(tempList);
                }
            }

            return temp;
        }
    }
}
