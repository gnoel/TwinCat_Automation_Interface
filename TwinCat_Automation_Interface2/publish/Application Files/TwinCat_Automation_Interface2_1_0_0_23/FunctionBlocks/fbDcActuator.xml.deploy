﻿<?xml version="1.0" encoding="utf-8"?><TcPlcObject Version="1.0.0.0"><POU Name="fbDcActuator" Id="{66FA01C1-AD5E-4BE5-9ACF-A856B2301EF9}"><Declaration>
	<![CDATA[FUNCTION_BLOCK fbDcActuator

VAR_INPUT
	bInhibit	: BOOL;		//Need to be ON to operate the actuator
	bExtend		: BOOL;		//Extend actuator until limit. ON for only one scan
	bRetract	: BOOL;		//Retract actuator until limit. ON for only one scan	
	bJogFwd		: BOOL;		//Jog forward
	bJogRev		: BOOL;		//Jog Reverse
	tTravelTimeOutDelay	: TIME; //Maximum time to reach full stroke	
	bBypassLimits	: BOOL; //Bypass limit switches
	bHmiCommOK		: BOOL;
	{attribute 'OPC.UA.DA':='1'}
	HmiControl		: stAxisHmiControl;
END_VAR

VAR_OUTPUT
	bFault: BOOL;	//Actuator Faulted
	bInExtendLimit	AT %I* : BOOL; //Extend limit switch
	bInRetractLimit AT %I* : BOOL; //Retract limit switch
	{attribute 'OPC.UA.DA':='1'}
	HmiStatus		: stAxisHmiStatus;
END_VAR

VAR
	bOutExtend	AT %Q*	: BOOL; //Extend output signal
	bOutRetract	AT %Q*	: BOOL; //Retract output signal
	tonExtendTimeOut 	: TON; //Protection timer to reach full stroke 	
	tonRetractTimeOut 	: TON; //Protection timer to reach full stroke 	
	tonBypassTimeOut	: TON; //Bypass limit protection 
	bExtendLatch		: BOOL;
	bRetractLatch		: BOOL;
	bJogFwdCmd			: BOOL;
	bJogRevCmd			: BOOL;
	bBypassLmitsCmd		: BOOL;
END_VAR

VAR CONSTANT
	tBYPASS_TIME_OUT	: TIME := T#60S; //Maximum bypass time allowed 
END_VAR


]]></Declaration><Implementation><ST>
	<![CDATA[
mHmiControl();

tonExtendTimeOut(IN := bExtendLatch, PT := tTravelTimeOutDelay);
tonRetractTimeOut(IN := bRetractLatch, PT := tTravelTimeOutDelay);
tonBypassTimeOut(IN := bBypassLimits, PT := tBYPASS_TIME_OUT);

//Time out fault detection
IF tonExtendTimeOut.Q OR tonRetractTimeOut.Q THEN
	bFault := TRUE;
END_IF

//Bypass limits signal need to be reset after delay
IF NOT tonBypassTimeOut.Q THEN
	bBypassLmitsCmd := bBypassLimits OR HmiControl.bHardLimitBypass;
ELSE
	bBypassLmitsCmd := FALSE;
END_IF

//Extend Latch and reset fault
IF bExtend THEN
	bExtendLatch := TRUE;
	bExtend := FALSE;
	bFault := FALSE;
//Retract Latch and reset fault
ELSIF bRetract THEN
	bRetractLatch := TRUE;
	bRetract := FALSE;
	bFault := FALSE;
END_IF

//Extend Unlatch
IF (NOT bInExtendLimit AND NOT bBypassLmitsCmd) OR bRetractLatch OR bFault OR NOT bInhibit OR HmiControl.bStop THEN 
	bExtendLatch := FALSE;
END_IF

//Retract Unlatch
IF (NOT bInRetractLimit AND NOT bBypassLmitsCmd) OR bExtendLatch OR bFault OR NOT bInhibit OR HmiControl.bStop THEN
	bRetractLatch := FALSE;
END_IF

bJogFwdCmd := (bJogFwd OR HmiControl.bJogFwd) AND (bInExtendLimit OR bBypassLmitsCmd); //Jog fwd 
bJogRevCmd := (bJogRev OR HmiControl.bJogRev) AND (bInRetractLimit OR bBypassLmitsCmd); //Jog rev

bOutExtend := bInhibit AND (bExtendLatch OR bJogFwdCmd); //Extend output
bOutRetract := bInhibit AND (bRetractLatch OR bJogRevCmd); //Retract output

mHmiStatus();

]]></ST></Implementation>
	<Method Name="mHmiControl" Id="{3841dea5-f796-49b6-bf0e-ff21098f1d42}">
		<Declaration>
			<![CDATA[METHOD PRIVATE mHmiControl : BOOL

VAR_INPUT
END_VAR

VAR
END_VAR
			]]>
		</Declaration>
		<Implementation>
			<ST>
				<![CDATA[ //Reset Control variables if communication HMI is down
IF NOT bHmiCommOk THEN
	MEMSET(ADR(HmiControl), 0, SIZEOF(HmiControl)); //Set all HMI control to 0
END_IF

//Check if control word is used
IF HmiControl.bUseCtrlWord THEN
	//Hmi Control word mapping
	HmiControl.bJogFwd := HmiControl.dwControlWord.0;	
	HmiControl.bJogRev := HmiControl.dwControlWord.1;
	HmiControl.bStop 	:= HmiControl.dwControlWord.3;
	HmiControl.bHardLimitBypass:= HmiControl.dwControlWord.8;
	HmiControl.bMoveFwd	:= HmiControl.dwControlWord.16;
	HmiControl.bMoveRev	:= HmiControl.dwControlWord.17;
END_IF



]]>
			</ST>
		</Implementation>
	</Method>
  <Method Name="mHmiStatus" Id="{3841dea5-f706-49b6-bf0e-af21298f1d42}">
    <Declaration>
      <![CDATA[METHOD PRIVATE mHmiStatus : BOOL

VAR_INPUT
END_VAR

			]]>
    </Declaration>
    <Implementation>
      <ST>
        <![CDATA[HmiStatus.bAxisMoving			:= bRetractLatch OR bExtendLatch;
HmiStatus.bHardLimitHiReached	:= NOT bInExtendLimit;
HmiStatus.bHardLimitLoReached	:= NOT bInRetractLimit; 
HmiStatus.bAxisAlarmStatus		:= bFault; 
HmiStatus.bJoggingFwd			:= bJogFwdCmd; 
HmiStatus.bJoggingRev			:= bJogRevCmd;
HmiStatus.bHardBypassStatus	:= bBypassLmitsCmd;

//Hmi Status Word Mapping
HmiStatus.dwStatusWord := 0;
HmiStatus.dwStatusWord.0 := HmiStatus.bJoggingFwd;
HmiStatus.dwStatusWord.1 := HmiStatus.bJoggingRev;
HmiStatus.dwStatusWord.2 := HmiStatus.bAxisMoving;	
HmiStatus.dwStatusWord.8 := HmiStatus.bHardBypassStatus;
HmiStatus.dwStatusWord.9 := HmiStatus.bAxisAlarmStatus;
HmiStatus.dwStatusWord.18 := HmiStatus.bHardLimitHiReached;
HmiStatus.dwStatusWord.19 := HmiStatus.bHardLimitLoReached;


]]>
      </ST>
    </Implementation>
  </Method><ObjectProperties /></POU></TcPlcObject>