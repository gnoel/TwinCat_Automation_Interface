﻿<?xml version="1.0" encoding="utf-8"?><TcPlcObject Version="1.0.0.0"><POU Name="fbMotionUnidrive" Id="{027546b0-fa3e-4f39-8192-e1002dc357c8}"><Declaration>
	<![CDATA[FUNCTION_BLOCK fbMotionUnidrive
VAR_INPUT
	AxisRef		: Axis_Ref;
	bInhibit	: BOOL;
	bEnable		: BOOL;
	bJogFwd		: BOOL;
	bJogRev		: BOOL;	
	rJogSpeed	: REAL;
	bTestMode	: BOOL;	
	rTestSpeed	: REAL;
	bBypassHardLimits	: BOOL;
	bBypassSoftLimits	: BOOL;
	bHmiCommOk	: BOOL;
	unAxisId	: UINT;
	sAxisName	: STRING;
	bWriteParamToCsv	: BOOL;
	bReadParamFromCsv	: BOOL;	
	lrOverride		: LREAL := 100;
	bHardLimitHi 	AT %I*: BOOL;
	bHardLimitLo 	AT %I*: BOOL;
	Parameter		: stAxisParam;
	unOutputRegister 	AT %Q*: UINT;
END_VAR

VAR_OUTPUT
	unStatusWord 	AT %I*: UINT;
	DriveState 		AT %I*: eDriveState;
	nAxisType		: eAxisType := AXIS_TYPE_M700_UNIDRIVE; 
	nInverterTemperature	AT %I*: INT;
	nInputRegister			AT %I*: INT;
END_VAR

VAR_IN_OUT
END_VAR

VAR		
	//Motion standard structure
	NcParameter : ST_AxisParameterSet;
	dActualCurrent 	AT %I*: DINT;
	nDcBusVoltage 	AT %I*: INT;
	snBrakeRelease 	AT %I*: SINT;
	usnDriveFaultId AT %I*: USINT;
	dMagnetisingCurrent 	AT %I*: DINT;
	dTorqueProducingCurrent AT %I*: DINT;
	unControlWord 		AT %Q*: UINT;
		
	//Motion function blocks
	fbMcPower				: MC_Power;
	fbReset					: MC_Reset;
	fbMcStop				: MC_Stop;
	fbReadParameterSet		: MC_ReadParameterSet; 
	fbWriteParameter		: MC_WriteParameter;
	fbMcSetPosition			: MC_SetPosition;
	fbMcMoveAbsolute1		: MC_MoveAbsolute;
	fbMcMoveAbsolute2		: MC_MoveAbsolute;
	fbMcMoveJog1			: MC_MoveAbsolute;
	fbMcMoveJog2			: MC_MoveAbsolute;
	fbMcExtSetPointEnable	: MC_ExtSetPointGenEnable;	
	fbMcExtSetPointDisable	: MC_ExtSetPointGenDisable;

	tonTest				: TON;
	tonParamUpdate		: TON;
	tonInitAxis			: TON;
	tonBrakeRelease		: TON;
	tonRampDown			: TON;	
	tonMoveStart		: TON;
	tonDisableExSet		: TON;
	tofMoving			: TOF;
	tonFollowingExtErr	: TON;
	tonJogAlt			: TON;
	tonIddleState		: TON;
	tonCancelAction		: TON;
	tonResetAxis		: TON;
	
	{attribute 'OPC.UA.DA':='1'}
	Hmi 					: stAxisHmi;	
	bOldHmiMoveGo			: BOOL;
	bOldHmiPositionRefSet	: BOOL;
	bOldHmiTestMode			: BOOL;
	bOldHmiApplyParam		: BOOL;
	bOldHmiSaveParam		: BOOL;
	bOldHmiPositionHiSet 	: BOOL;
	bOldHmiPositionLoSet 	: BOOL;
	bOldHmiMoveFwd			: BOOL;
	bOldHmiMoveRev			: BOOL;
	bOldHmiCancel			: BOOL;
	bHmiTestModeToggle		: BOOL;
	tonHmiPositionRefSet	: TON;
	tonHmiPositionHiSet		: TON;
	tonHmiPositionLoSet		: TON;
		
	State					: eMotionState;
	OldSate					: eMotionState;
	bMoveRequest			: BOOL;
	bJogRequest				: BOOL;
	bExtSetEnableRequest	: BOOL;
	bExtSetDisableRequest	: BOOL;
	bInitAxis				: BOOL;
	bBrakeOutput			: BOOL;
	oFlasher				: fbFlasher;
	
	bAltMove				: BOOL;
	bAltJog					: BOOL;
	bOldTestMode			: BOOL;
	rTestPositionHi			: REAL;
	rTestPositionLo			: REAL;
	lrJogTarget				: LREAL;
	rJogAccel				: REAL;
	rJogDecel				: REAL;
	bPositionSetReq			: BOOL;
	rSelectedJogSpeed		: REAL;
	rSelectedTestSpeed		: REAL;
	bOldBypassSoftLimits	: BOOL;
	bSelectBypassSoftLimits	: BOOL;
	bSelectBypassHardLimits	: BOOL;
	
	bWriteParamToNc		: BOOL;
	tonWriteParam		: TON;
	unWriteParamIdx		: UINT;
	unOldWriteParamIdx	: UINT;
	tLastWriteParamDelay: TIME;
	bParamNcInit		: BOOL; 
	unParamInitStep		: UINT;
	bParamInit			: BOOL;
	bFirstScan			: BOOL; //ON only for first FB scan
	bFirstScanSet		: BOOL; //Use to set first scan
	
	//CSV file param read/write
	sNetId			: T_AmsNetId := '';	(* TwinCAT system network address *)
	sFileName		: T_MaxString := '\Axis';(* CSV destination file path and name *)
	sDirectoryName	: T_MaxString := 'C:\Show-Canada'; //CSV file directory
	sCSVLine		: T_MaxString := '';(* Single CSV text line (row, record), we are using string as record buffer (your are able to see created fields) *)
	sCSVField		: T_MaxString := '';(* Single CSV field value (column, record field) *)
	unRow	 		: UINT;(* Row number (record) *)
	unColumn		: UINT;(* Column number (record field) *)
	hFile			: UINT;(* File handle of the source file *)
	b8WriteCsvStep	: BYTE;
	b8ReadCsvStep	: BYTE;
	fbCreateDir		: FB_CreateDir;
	fbFilePuts		: FB_FilePuts;(* Writes one record (line) *)
	fbFileOpen		: FB_FileOpen;(* Opens file *)
	fbFileClose		: FB_FileClose;(* Closes file *)
	fbFileGets		: FB_FileGets;(* Reads one record (line) *)
	fbWriter		: FB_CSVMemBufferWriter;(* Helper function block used to create CSV data bytes *)
	unNumberParam	: UINT;
	pData			: POINTER TO REAL;
END_VAR

VAR CONSTANT
	
	sPARAM_NAME: ARRAY[0..63] OF STRING := [
		'Max Speed',
		'Max Accel',
		'Max Decel',
		'Default Jerk',
		'Jog Max Speed',
		'Jog Accel',
		'Jog Decel',
		'Estop Decel',
		'Stop Decel',
		'Test Position Lo',
		'Test Position Hi',
		'Test Delay',
		'Soft Limit Hi',
		'Soft Limit Lo',
		'Following Error Limit',
		'Reference Position',
		'Encoder Offset',
		'Preset Position 1',
		'Preset Position 2',
		'Preset Position 3',
		'Preset Position 4',
		'Preset Position 5',
		'Brake Release Delay',
		'Cue Move Target',
		'Cue Move Speed',
		'Cue Move Accel',
		'Cue Move Decel',
		'Bypass Speed',
		'Use Absolute Positionning System'];	
END_VAR
]]></Declaration><Implementation><ST>
	<![CDATA[
bFirstScan := NOT bFirstScanSet;
bFirstScanSet := TRUE;

AxisRef.ReadStatus(); //Read axis status from NC
InitParam();	//Initiate parameters
HmiStatus();	//Read HMI status
HmiControl();	//Wrtie HMI controls
WriteParamToNc();	//Write parameters to NC
ReadParamFromCsv();	//Read parameters from CSV file
WriteParamToCsv();	//Write parameters to CSV file
oFlasher();

//Axis Parameter update every 2s
tonParamUpdate(IN := NOT tonParamUpdate.Q, PT := T#2S, Q => fbReadParameterSet.Execute);

IF (AxisRef.Status.Error OR unStatusWord.3) AND NOT tofMoving.Q THEN
	State := MOTION_FAULTED;
ELSIF (NOT bInhibit AND NOT tofMoving.q) THEN
	State := MOTION_INHIBIT;
END_IF

//State management
CASE State OF
	MOTION_FAULTED: 
		IF NOT AxisRef.Status.Error AND NOT  unStatusWord.3 THEN
			State := MOTION_IDLE;
		END_IF

	MOTION_INHIBIT:
		IF bInhibit THEN
			State := MOTION_IDLE;
		END_IF
		
	MOTION_IDLE: 
		IF NOT bInhibit THEN
			State := MOTION_INHIBIT;
		ELSIF (bMoveRequest OR bJogRequest OR bExtSetEnableRequest OR bEnable) AND bParamInit AND bParamNcInit AND NOT bWriteParamToCsv AND NOT bReadParamFromCsv THEN
			State := MOTION_INIT;
		END_IF

	MOTION_RAMP_DOWN: 
		IF tonRampDown.Q OR AxisRef.Status.NotMoving THEN
			IF NOT bInhibit THEN
				State := MOTION_INHIBIT;
			ELSE
				State := MOTION_IDLE;
			END_IF	
		END_IF
	
	MOTION_INIT: 
		IF NOT bInitAxis AND DriveState = DRIVE_RUN THEN
			IF bMoveRequest THEN
				State := MOTION_MOVING;
			ELSIF bJogRequest THEN
				State := MOTION_JOGGING;
			ELSIF bExtSetEnableRequest THEN
				State := MOTION_FOLLOWING_EXT;
			ELSIF bEnable THEN
				State := MOTION_ENABLE;
			ELSE
				State := MOTION_IDLE;
			END_IF
		ELSIF NOT bInitAxis THEN
			State := MOTION_IDLE;
		END_IF
		
	MOTION_ENABLE:
		IF NOT bInhibit THEN
			State := MOTION_INHIBIT;
		ELSIF bJogRequest THEN
			State := MOTION_JOGGING;
		ELSIF bMoveRequest THEN
			State := MOTION_MOVING;
		ELSIF bExtSetEnableRequest THEN
			State := MOTION_FOLLOWING_EXT;
		ELSIF NOT bEnable THEN
			State := MOTION_IDLE;
		END_IF	

	MOTION_MOVING:
		IF NOT bInhibit THEN
			State := MOTION_RAMP_DOWN;
		ELSIF bJogRequest THEN
			State := MOTION_JOGGING;
		ELSIF NOT tofMoving.Q  AND tonMoveStart.Q AND NOT bEnable THEN
			State := MOTION_IDLE;
		ELSIF NOT tofMoving.Q  AND tonMoveStart.Q AND bEnable THEN
			State := MOTION_ENABLE;
		END_IF

	MOTION_JOGGING:
		IF NOT bInhibit THEN
			State := MOTION_RAMP_DOWN;
		ELSIF NOT bJogFwd AND NOT bJogRev AND NOT Hmi.Control.bJogFwd AND NOT Hmi.Control.bJogRev AND NOT tofMoving.Q AND NOT bEnable THEN
			State := MOTION_IDLE;	
		ELSIF NOT bJogFwd AND NOT bJogRev AND NOT Hmi.Control.bJogFwd AND NOT Hmi.Control.bJogRev AND NOT tofMoving.Q AND bEnable THEN
			State := MOTION_ENABLE;
		END_IF
		
	MOTION_FOLLOWING_EXT:
		IF NOT bInhibit THEN
			State := MOTION_RAMP_DOWN;
		ELSIF (tonDisableExSet.Q OR tonFollowingExtErr.Q) AND NOT bEnable THEN
			State := MOTION_IDLE;
		ELSIF (tonDisableExSet.Q OR tonFollowingExtErr.Q) AND bEnable THEN
			State := MOTION_ENABLE;
		END_IF
END_CASE

//Timers execution 
tonRampDown(PT := T#3000MS);
tonIddleState(IN := State = MOTION_IDLE, PT := T#100MS);
tonInitAxis(PT := T#50MS);//50
tonBrakeRelease(PT := REAL_TO_TIME(Parameter.rBrakeReleaseDelay));
tonMoveStart(PT := T#100MS);
tonDisableExSet(PT := T#10MS);
tofMoving(IN := AxisRef.Status.Moving, PT := T#1000MS);
tonFollowingExtErr(IN := (State = MOTION_FOLLOWING_EXT AND NOT fbMcExtSetPointEnable.Enabled), PT := T#100MS); 
tonCancelAction(IN := bMoveRequest OR bExtSetEnableRequest, PT := T#3S);

//State machine management
CASE State OF
	MOTION_INHIBIT:
		unControlWord := 0;
		bBrakeOutput := 0;
		
		bMoveRequest := FALSE;
		bJogRequest := FALSE;
		bExtSetEnableRequest := FALSE;
		bTestMode := FALSE;
		
		IF bPositionSetReq THEN
			bWriteParamToNc := TRUE;
			bWriteParamToCsv := TRUE;
		END_IF
		
	MOTION_FAULTED:
		IF OldSate <> MOTION_FAULTED THEN
			unControlWord := 16#6;	
		END_IF
		bBrakeOutput := 0;
		bMoveRequest := FALSE;
		bJogRequest := FALSE;
		bExtSetEnableRequest := FALSE;
		bTestMode := FALSE;
		
	MOTION_IDLE:
		unControlWord := 16#6;
		bBrakeOutput := snBrakeRelease.0;
		fbMcPower.Enable := FALSE;
		fbMcExtSetPointDisable.Execute := TRUE;
		
		IF OldSate <> MOTION_IDLE THEN
			bJogRequest := FALSE;
			bExtSetEnableRequest := FALSE;
			bWriteParamToNc := TRUE;
		ELSIF bPositionSetReq THEN
			bWriteParamToNc := TRUE;
			bWriteParamToCsv := TRUE;
		END_IF
		
		tonDisableExSet.IN := FALSE;
		tonMoveStart.IN := FALSE;
		tonRampDown.IN := FALSE;
	
	MOTION_RAMP_DOWN:
		unControlWord := 16#F;
		bBrakeOutput := 1; //Keep brake disengaged in case of power loss; 
		
		tonRampDown.IN := TRUE;
		StopAxis(rDecel := Parameter.rEstopDecel, rJerk := 9999);
		bTestMode := FALSE;
	
	MOTION_INIT:
		bBrakeOutput := snBrakeRelease.0;
		
		IF OldSate <> MOTION_INIT THEN
			bInitAxis := TRUE;			
		END_IF
		
		IF NOT tonInitAxis.IN AND NOT tonBrakeRelease.IN THEN
			unControlWord := 16#7;
			tonInitAxis.IN := TRUE;
			tonBrakeRelease.IN := FALSE;
			ResetAxis();
		ELSIF tonInitAxis.Q THEN
			unControlWord := 16#F;
			tonInitAxis.IN := FALSE;
			tonBrakeRelease.IN := TRUE;
			fbMcPower.Enable := TRUE;
		ELSIF tonBrakeRelease.Q THEN
			unControlWord := 16#F;
			bInitAxis := FALSE;	
			tonBrakeRelease.IN := FALSE;		
		END_IF

	MOTION_ENABLE:
		unControlWord := 16#F;
		bBrakeOutput := snBrakeRelease.0;
		tonMoveStart.IN := FALSE;
		tonDisableExSet.IN := FALSE;
		tonRampDown.IN := FALSE;
	
	MOTION_MOVING:			
		unControlWord := 16#F;
		bBrakeOutput := snBrakeRelease.0;
		
		IF bMoveRequest THEN
			IF bAltMove THEN
				fbMcMoveAbsolute1.Execute := TRUE;
			ELSE 
				fbMcMoveAbsolute2.Execute := TRUE;		
			END_IF
			bAltMove := NOT bAltMove;	
			tonMoveStart.IN := TRUE;
			bMoveRequest := FALSE;
		END_IF

	MOTION_JOGGING:
		unControlWord := 16#F;
		bBrakeOutput := snBrakeRelease.0;
		bJogRequest := FALSE;
		bTestMode := FALSE;
		
		tonJogAlt(IN := NOT tonJogAlt.Q, PT := T#10MS);
		IF tonJogAlt.Q THEN
			bAltJog := NOT bAltJog;
		END_IF
		
		IF Hmi.Control.bJogFwd OR Hmi.Control.bJogRev THEN
			rSelectedJogSpeed := INT_TO_REAL(Hmi.Control.nSpeedPercent) * Parameter.rJogMaxSpeed / 100;
		ELSE
			rSelectedJogSpeed := rJogSpeed;
		END_IF
	
		IF rSelectedJogSpeed < (Parameter.rJogMaxSpeed / 4) THEN
			rJogAccel := Parameter.rJogAccel / 2;
			rJogDecel := Parameter.rJogDecel / 2;
		ELSE
			rJogAccel := Parameter.rJogAccel;
			rJogDecel := Parameter.rJogDecel;
		END_IF			
		
		IF bJogFwd OR Hmi.Control.bJogFwd THEN
			IF bSelectBypassSoftLimits THEN
				lrJogTarget := 16#FFFFFFFFFFFF;
			ELSE
				//Prevent axis from going in wrong direction if soft limit is exceeded
				IF AxisRef.NcToPlc.ActPos <= Parameter.rHiSoftLimit THEN
					lrJogTarget := Parameter.rHiSoftLimit;
				ELSE
					lrJogTarget := AxisRef.NcToPlc.ActPos;
				END_IF
			END_IF
			
			IF bAltJog OR OldSate <> MOTION_JOGGING THEN
				fbMcMoveJog1.Execute := TRUE;
			ELSE
				fbMcMoveJog2.Execute := TRUE;
			END_IF
		ELSIF bJogRev OR Hmi.Control.bJogRev THEN
			IF bSelectBypassSoftLimits THEN
				lrJogTarget := -16#FFFFFFFFFFFF;
			ELSE
				//Prevent axis from going in wrong direction if soft limit is exceeded
				IF AxisRef.NcToPlc.ActPos >= Parameter.rLowSoftLimit THEN
					lrJogTarget := Parameter.rLowSoftLimit;
				ELSE
					lrJogTarget := AxisRef.NcToPlc.ActPos;
				END_IF
			END_IF
			
			IF bAltJog OR OldSate <> MOTION_JOGGING THEN
				fbMcMoveJog1.Execute := TRUE;
			ELSE
				fbMcMoveJog2.Execute := TRUE;
			END_IF
		ELSE
			StopAxis(rDecel := Parameter.rJogDecel, rJerk := 0);	
		END_IF		
				
	MOTION_FOLLOWING_EXT:
		
		unControlWord := 16#F;
		bBrakeOutput := snBrakeRelease.0;
		
		IF OldSate <> MOTION_FOLLOWING_EXT THEN
			bExtSetEnableRequest := FALSE;
			bExtSetDisableRequest := FALSE;
			fbMcExtSetPointEnable.Position := 1;
			fbMcExtSetPointEnable.PositionType := POSITIONTYPE_ABSOLUTE;
			fbMcExtSetPointEnable.Execute := TRUE;
			AxisRef.PlcToNc.ExtSetDirection := 1;
			AxisRef.PlcToNc.ExtSetPos := AxisRef.NcToPlc.ActPos;
		END_IF
		
		IF bExtSetDisableRequest THEN
			fbMcExtSetPointDisable.Execute := TRUE;
			tonDisableExSet.IN := TRUE;
			bExtSetDisableRequest := FALSE;
		END_IF	
END_CASE
OldSate := State;


// Test Mode management
tonTest(IN := NOT tonTest.Q AND (bTestMode OR bHmiTestModeToggle), PT := REAL_TO_TIME(Parameter.rTestDelay));

IF bHmiTestModeToggle THEN
	rSelectedTestSpeed := Parameter.rMaxSpeed * Hmi.Control.nSpeedPercent / 100;
ELSE
	rSelectedTestSpeed := rTestSpeed;
END_IF

IF bTestMode OR bHmiTestModeToggle THEN		
	IF (tonTest.Q AND AxisRef.NcToPlc.ActPos >= (Parameter.rTestPositionHi - Parameter.rTestPositionLow) / 2) OR NOT bOldTestMode THEN
		rTestPositionLo := MAX(Parameter.rTestPositionLow, Parameter.rLowSoftLimit); //Use soft if test position is not set
		//Move to Low test position
		MotionMove(
			rPosition := rTestPositionLo, 
			rSpeed := rSelectedTestSpeed, 
			rAccel := Parameter.rMaxAccel, 
			rDecel := Parameter.rMaxDecel, 
			rJerk := 0);
	ELSIF tonTest.Q AND AxisRef.NcToPlc.ActPos < (Parameter.rTestPositionHi - Parameter.rTestPositionLow) / 2 THEN
		//Use hi soft if hi test position not set
		IF Parameter.rTestPositionHi <> 0 THEN 
			rTestPositionHi := Parameter.rTestPositionHi; 
		ELSE 
			rTestPositionHi := Parameter.rHiSoftLimit;
		END_IF
		//Move to high test position
		MotionMove(
			rPosition := rTestPositionHi, //Use soft is test position is not set
			rSpeed := rSelectedTestSpeed, 
			rAccel := Parameter.rMaxAccel, 
			rDecel := Parameter.rMaxDecel, 
			rJerk := 0);
	END_IF
END_IF
bOldTestMode := bTestMode OR bHmiTestModeToggle;

//Jog management
IF (bJogFwd OR bJogRev OR Hmi.Control.bJogFwd OR Hmi.Control.bJogRev) AND Parameter.rJogAccel <= Parameter.rMaxAccel AND Parameter.rJogDecel <= Parameter.rMaxDecel THEN
	bJogRequest := TRUE;
ELSE
	bJogRequest := FALSE;	
END_IF

bSelectBypassSoftLimits := bBypassSoftLimits OR Hmi.Control.bSoftLimitBypass;
bSelectBypassHardLimits := bBypassHardLimits OR Hmi.Control.bHardLimitBypass;

//Soft Limits bypass
IF 	bSelectBypassSoftLimits <> bOldBypassSoftLimits AND State = MOTION_IDLE AND bParamNcInit THEN
	bWriteParamToNc := TRUE;
END_IF	
bOldBypassSoftLimits := bSelectBypassSoftLimits;

IF tonCancelAction.Q THEN
	bMoveRequest := FALSE;
	bExtSetEnableRequest := FALSE;
END_IF

//Override selection
IF bSelectBypassSoftLimits OR bSelectBypassHardLimits THEN
	fbMcPower.Override := Parameter.rBypassSpeed;
ELSE
	fbMcPower.Override := lrOverride;
END_IF

unOutputRegister.0 := bBrakeOutput;

//NC function blocks management
fbMcPower.Enable_Negative := bHardLimitLo OR bSelectBypassHardLimits;
fbMcPower.Enable_Positive := bHardLimitHi OR bSelectBypassHardLimits;	
fbMcPower.Override := lrOverride;
fbMcPower(Axis := AxisRef);

fbReset(Axis := AxisRef);
unControlWord.7 := fbReset.Execute; //Control word bit 7 = Fault reset
tonResetAxis(IN := fbReset.Execute, PT := T#50MS);
IF tonResetAxis.Q THEN
	fbReset.Execute := FALSE; //Unset execute bit after a delay
END_IF

fbMcSetPosition.Options.ClearPositionLag := TRUE;
fbMcSetPosition(Axis := AxisRef);
fbMcSetPosition.Execute := 0;

fbMcStop(Axis := AxisRef);
fbMcStop.Execute := FALSE;

fbMcMoveAbsolute1(Axis := AxisRef);
fbMcMoveAbsolute1.Execute := FALSE;

fbMcMoveAbsolute2(Axis := AxisRef);
fbMcMoveAbsolute2.Execute := FALSE;

fbMcMoveJog1(
	Axis := AxisRef,
	Position := lrJogTarget,
	Velocity := rSelectedJogSpeed,
	Acceleration := rJogAccel, 
	Deceleration := rJogDecel);
fbMcMoveJog1.Execute := FALSE;

fbMcMoveJog2(
	Axis := AxisRef,
	Position := lrJogTarget,
	Velocity := rSelectedJogSpeed,
	Acceleration := rJogAccel,
	Deceleration := rJogDecel);
fbMcMoveJog2.Execute := FALSE;	

fbMcExtSetPointEnable(Axis := AxisRef);
fbMcExtSetPointEnable.Execute := FALSE;

fbMcExtSetPointDisable(Axis := AxisRef);
fbMcExtSetPointDisable.Execute := FALSE;

fbWriteParameter(Axis := AxisRef);
fbWriteParameter.Execute := FALSE;

// Read NC param
fbReadParameterSet(
	Axis := AxisRef, 
	Parameter := NCParameter); 

//Create CSV param file directory
fbCreateDir.bExecute := bFirstScan;
fbCreateDir(sNetId := '', sPathName := sDirectoryName);








]]></ST></Implementation>
	<Method Name="ExtSetPointEnable" Id="{3841dea5-f706-49b6-bf0e-af21098f1d42}">
		<Declaration>
			<![CDATA[METHOD PUBLIC ExtSetPointEnable
VAR_INPUT
bIn: BOOL;
END_VAR
			]]>
		</Declaration>
		<Implementation>
			<ST>
				<![CDATA[// Enable external position
IF bIn THEN
	bExtSetEnableRequest := TRUE;

// Disable external position
ELSE
	bExtSetDisableRequest := TRUE;
END_IF
]]>
			</ST>
		</Implementation>
	</Method>
  <Method Name="HmiControl" Id="{3841dea5-f706-49b6-bf0e-af21298f1d42}">
    <Declaration>
      <![CDATA[METHOD PRIVATE HmiControl : BOOL

VAR_INPUT
END_VAR

VAR
END_VAR
			]]>
    </Declaration>
    <Implementation>
      <ST>
        <![CDATA[//Reset Control variables if communication HMI is down
IF NOT bHmiCommOk THEN
	bHmiTestModeToggle 			:= FALSE; //disable est mode
	MEMSET(ADR(Hmi.Control), 0, SIZEOF(Hmi.Control)); //Set all HMI control to 0
END_IF

//Check if control word is used
IF Hmi.Control.bUseCtrlWord THEN
	//Hmi Control word mapping
	Hmi.Control.bJogFwd := Hmi.Control.dwControlWord.0;	// Bit 0
	Hmi.Control.bJogRev := Hmi.Control.dwControlWord.1;	// Bit 1
	Hmi.Control.bMoveGo := Hmi.Control.dwControlWord.2;	// Bit 2
	Hmi.Control.bStop 	:= Hmi.Control.dwControlWord.3;	// Bit 3
	Hmi.Control.bPositionRefSet := Hmi.Control.dwControlWord.4;	// Bit 4
	Hmi.Control.bPositionHiSet 	:= Hmi.Control.dwControlWord.5;	// Bit 5
	Hmi.Control.bPositionLoSet 	:= Hmi.Control.dwControlWord.6;	// Bit 6
	Hmi.Control.bSoftLimitBypass:= Hmi.Control.dwControlWord.7;// Bit 7
	Hmi.Control.bHardLimitBypass:= Hmi.Control.dwControlWord.8;// Bit 8
	Hmi.Control.bResetError		:= Hmi.Control.dwControlWord.9;	// Bit 9
	Hmi.Control.bMoveFwd	:= Hmi.Control.dwControlWord.16;		// Bit 16
	Hmi.Control.bMoveRev	:= Hmi.Control.dwControlWord.17;		// Bit 17
	Hmi.Control.bApplyParam	:= Hmi.Control.dwControlWord.24;	// Bit 24
	Hmi.Control.bSaveParam	:= Hmi.Control.dwControlWord.25;	// Bit 25
	Hmi.Control.bTestMode	:= Hmi.Control.dwControlWord.26;	// Bit 26
END_IF

tonHmiPositionRefSet(IN := Hmi.Control.bPositionRefSet, PT := T#3S); //Position set timer
tonHmiPositionHiSet(IN := Hmi.Control.bPositionHiSet, PT := T#3S); //High soft limit teach timer
tonHmiPositionLoSet(IN := Hmi.Control.bPositionLoSet, PT := T#3S); //Low soft limit teach timer

//HMI stop
IF Hmi.Control.bStop THEN
	StopAxis(rDecel := Parameter.rStopDecel, rJerk := 0);
END_IF
	
//HMI cue move
IF Hmi.Control.bMoveGo AND NOT bOldHmiMoveGo THEN
	MotionMove(
		rPosition := Hmi.Parameter.rCueTarget,
		rSpeed := Hmi.Parameter.rCueSpeed,
		rAccel := Hmi.Parameter.rCueAccel,
		rDecel := Hmi.Parameter.rCueDecel,
		rJerk := 0); //default jerk
END_IF
	
//HMI fwd move
IF Hmi.Control.bMoveFwd AND NOT bOldHmiMoveFwd THEN
	MotionMove(
		rPosition := Parameter.rHiSoftLimit,
		rSpeed := Parameter.rMaxSpeed * Hmi.Control.nSpeedPercent / 100,
		rAccel := Parameter.rMaxAccel,
		rDecel := Parameter.rMaxDecel,
		rJerk := 0); //default jerk
END_IF
	
//HMI rev move
IF Hmi.Control.bMoveRev AND NOT bOldHmiMoveRev THEN
	MotionMove(
		rPosition := Parameter.rLowSoftLimit,
		rSpeed := Parameter.rMaxSpeed * Hmi.Control.nSpeedPercent / 100,
		rAccel := Parameter.rMaxAccel,
		rDecel := Parameter.rMaxDecel,
		rJerk := 0); //default jerk
END_IF
		
//Hmi test mode toggle
IF Hmi.Control.bTestMode AND NOT bOldHmiTestMode THEN
	bHmiTestModeToggle := NOT bHmiTestModeToggle;
//Deactivate HMI test mode if motion is in incorrect state
ELSIF State <> MOTION_IDLE AND State <> MOTION_INIT AND State <> MOTION_MOVING THEN
	bHmiTestModeToggle := FALSE;
END_IF
	
//HMI position reference set
IF tonHmiPositionRefSet.Q AND NOT bOldHmiPositionRefSet THEN
	PositionSet(rPosition := Hmi.Parameter.rReferencePosition);
END_IF
	
//HMI position Hi set
IF tonHmiPositionHiSet.Q AND NOT bOldHmiPositionHiSet THEN
	Parameter.rHiSoftLimit := AxisRef.NcToPlc.ActPos;
	Hmi.Parameter.rHiSoftLimit := AxisRef.NcToPlc.ActPos;
END_IF
	
//HMI position Low set
IF tonHmiPositionLoSet.Q AND NOT bOldHmiPositionLoSet THEN
	Parameter.rLowSoftLimit := AxisRef.NcToPlc.ActPos;
	Hmi.Parameter.rLowSoftLimit := AxisRef.NcToPlc.ActPos;
END_IF

//HMI apply params
IF Hmi.Control.bApplyParam AND NOT bOldHmiApplyParam THEN
	MEMCPY(destAddr := ADR(Parameter), srcAddr := ADR(Hmi.Parameter), SIZEOF(Parameter));
	bWriteParamToNc := TRUE;
END_IF
	
//HMI save params
IF Hmi.Control.bSaveParam AND NOT bOldHmiSaveParam THEN
	bWriteParamToCsv := TRUE;
ELSIF bOldHmiSaveParam AND NOT bWriteParamToCsv THEN
	Hmi.Control.bSaveParam := FALSE;
	Hmi.Control.dwControlWord.25 := FALSE;
END_IF
	
//HMI cancel param
IF Hmi.Control.bCancelParam AND NOT bOldHmiCancel THEN
	MEMCPY(destAddr := ADR(Hmi.Parameter), srcAddr := ADR(Parameter), SIZEOF(Parameter));
END_IF

//One shot variable set
bOldHmiMoveGo := Hmi.Control.bMoveGo;
bOldHmiMoveFwd := Hmi.Control.bMoveFwd;
bOldHmiMoveRev := Hmi.Control.bMoveRev;
bOldHmiTestMode := Hmi.Control.bTestMode;
bOldHmiPositionRefSet := tonHmiPositionRefSet.Q;
bOldHmiPositionHiSet := tonHmiPositionHiSet.Q;
bOldHmiPositionLoSet := tonHmiPositionLoSet.Q;
bOldHmiApplyParam := Hmi.Control.bApplyParam;
bOldHmiSaveParam := Hmi.Control.bSaveParam;
bOldHmiCancel := Hmi.Control.bCancelParam;
]]>
      </ST>
    </Implementation>
  </Method>
  <Method Name="HmiStatus" Id="{3841dea5-f706-42b6-bf0e-af21098f1d42}">
    <Declaration>
      <![CDATA[METHOD PRIVATE HmiStatus : BOOL

VAR_INPUT
END_VAR

			]]>
    </Declaration>
    <Implementation>
      <ST>
        <![CDATA[Hmi.Status.rActualPosition 		:= AxisRef.NcToPlc.ActPos;
Hmi.Status.rActualSpeed			:= AxisRef.NcToPlc.ActVelo;
Hmi.Status.bSoftLimitHiReached	:= AxisRef.Status.SoftLimitMaxExceeded;
Hmi.Status.bSoftLimitLoReached	:= AxisRef.Status.SoftLimitMinExceeded;
Hmi.Status.bHardLimitHiReached	:= NOT bHardLimitHi;
Hmi.Status.bHardLimitLoReached	:= NOT bHardLimitLo; 
Hmi.Status.bAxisMoving			:= AxisRef.Status.Moving;
Hmi.Status.bAxisAlarmStatus		:= AxisRef.Status.Error; 
Hmi.Status.bDriveAlarmStatus	:= unStatusWord.3; 
Hmi.Status.bPosRefSetStatus		:= (tonHmiPositionRefSet.IN AND oFlasher.b1On1Off) OR tonHmiPositionRefSet.Q;
Hmi.Status.bPosLoSetStatus		:= (tonHmiPositionLoSet.IN AND oFlasher.b1On1Off) OR tonHmiPositionLoSet.Q; 
Hmi.Status.bPosHiSetStatus		:= (tonHmiPositionHiSet.IN AND oFlasher.b1On1Off) OR tonHmiPositionHiSet.Q; 
Hmi.Status.bJoggingFwd			:= AxisRef.Status.PositiveDirection AND State = MOTION_JOGGING; 
Hmi.Status.bJoggingRev			:= AxisRef.Status.NegativeDirection AND State = MOTION_JOGGING;
Hmi.Status.unAxisErrorCode		:= UDINT_TO_UINT(AxisRef.Status.ErrorID);
Hmi.Status.unDriveTripCode		:= usnDriveFaultId;
Hmi.Status.bSoftBypassStatus	:= bSelectBypassSoftLimits;
Hmi.Status.bHardBypassStatus	:= bSelectBypassHardLimits;
Hmi.Status.unDriveState			:= DriveState;
Hmi.Status.unMotionState		:= State;

//Hmi Status Word Mapping
Hmi.Status.dwStatusWord := 0;
Hmi.Status.dwStatusWord.0 := Hmi.Status.bJoggingFwd;
Hmi.Status.dwStatusWord.1 := Hmi.Status.bJoggingRev;
Hmi.Status.dwStatusWord.2 := Hmi.Status.bAxisMoving;	

Hmi.Status.dwStatusWord.4 := Hmi.Status.bPosRefSetStatus;
Hmi.Status.dwStatusWord.5 := Hmi.Status.bPosHiSetStatus;
Hmi.Status.dwStatusWord.6 := Hmi.Status.bPosLoSetStatus;
Hmi.Status.dwStatusWord.7 := Hmi.Status.bSoftBypassStatus;
Hmi.Status.dwStatusWord.8 := Hmi.Status.bHardBypassStatus;
Hmi.Status.dwStatusWord.9 := Hmi.Status.bAxisAlarmStatus;
Hmi.Status.dwStatusWord.10 := Hmi.Status.bDriveAlarmStatus;	

Hmi.Status.dwStatusWord.16 := Hmi.Status.bSoftLimitHiReached;
Hmi.Status.dwStatusWord.17 := Hmi.Status.bSoftLimitLoReached;
Hmi.Status.dwStatusWord.18 := Hmi.Status.bHardLimitHiReached;
Hmi.Status.dwStatusWord.19 := Hmi.Status.bHardLimitLoReached;

//Axis type set
Hmi.nAxisType := nAxisType;

]]>
      </ST>
    </Implementation>
  </Method>
  <Method Name="InitParam" Id="{22B12522-4BB6-484D-AE34-C760569FD2E0}">
    <Declaration>
      <![CDATA[METHOD PRIVATE InitParam : BOOL
VAR_INPUT
END_VAR

			]]>
    </Declaration>
    <Implementation>
      <ST>
        <![CDATA[//When PLC start, read param from csv file, copy to HMI param and write to NC
CASE unParamInitStep OF
	0: //Start reading param from CSV file
		IF NOT bParamInit OR bFirstScan THEN
			bReadParamFromCsv := TRUE;
			unParamInitStep := 1;
		END_IF
	1: //Wait for end of reading from csv file
		IF NOT bReadParamFromCsv THEN
			unParamInitStep := 2;
		END_IF
	2: //Copy parameters to HMI parameters and Write to NC
		MEMCPY(destAddr := ADR(Hmi.Parameter), srcAddr := ADR(Parameter), SIZEOF(stAxisParam));
		bWriteParamToNc := TRUE;
		unParamInitStep := 3;
	3://Wait for write param to NC
		IF NOT bWriteParamToNc THEN
			bParamInit := TRUE;
			unParamInitStep := 0;
		END_IF
END_CASE
]]>
      </ST>
    </Implementation>
  </Method>
  <Method Name="MotionMove" Id="{8F9F64AE-7045-48BF-9EBC-71121397A937}">
    <Declaration>
      <![CDATA[METHOD PUBLIC MotionMove
VAR_INPUT
	rPosition: REAL;
	rSpeed: REAL;
	rAccel: REAL;
	rDecel: REAL;
	rJerk: REAL;
END_VAR
			]]>
    </Declaration>
    <Implementation>
      <ST>
        <![CDATA[// Position Move
IF bAltMove THEN
	fbMcMoveAbsolute1.Position := rPosition;
	fbMcMoveAbsolute1.Velocity := rSpeed;
	fbMcMoveAbsolute1.Acceleration := rAccel;
	fbMcMoveAbsolute1.Deceleration := rDecel;	
	fbMcMoveAbsolute1.Jerk := rJerk;	
ELSE
	fbMcMoveAbsolute2.Position := rPosition;
	fbMcMoveAbsolute2.Velocity := rSpeed;
	fbMcMoveAbsolute2.Acceleration := rAccel;
	fbMcMoveAbsolute2.Deceleration := rDecel;	
	fbMcMoveAbsolute2.Jerk := rJerk;
END_IF

IF rAccel <= Parameter.rMaxAccel AND rDecel <= Parameter.rMaxDecel THEN
	bMoveRequest := TRUE;
END_IF
]]>
      </ST>
    </Implementation>
  </Method>
  <Method Name="PositionSet" Id="{22B12522-4BB6-484D-AE34-C762569FD2E0}">
    <Declaration>
      <![CDATA[METHOD PUBLIC PositionSet
VAR_INPUT
	rPosition: real;
END_VAR

			]]>
    </Declaration>
    <Implementation>
      <ST>
        <![CDATA[IF NOT bPositionSetReq THEN
	Parameter.rEncoderOffset := rPosition - LREAL_TO_REAL(AxisRef.NcToPlc.ActPos) + LREAL_TO_REAL(NcParameter.fEncOffset);
	Hmi.Parameter.rEncoderOffset := rPosition - LREAL_TO_REAL(AxisRef.NcToPlc.ActPos) + LREAL_TO_REAL(NcParameter.fEncOffset);
	bPositionSetReq := TRUE;
END_IF
]]>
      </ST>
    </Implementation>
  </Method>
  <Method Name="ReadParamFromCsv" Id="{1E25208F-0FF0-4452-A334-296D8C84F126}">
    <Declaration>
      <![CDATA[METHOD PRIVATE ReadParamFromCsv : BOOL

VAR_INPUT
END_VAR
			]]>
    </Declaration>
    <Implementation>
      <ST>
        <![CDATA[CASE b8ReadCsvStep OF
	0:	(* Wait for rising edge at bReadParamFromCsv variable *)
		IF bReadParamFromCsv AND NOT bWriteParamToNc AND NOT bWriteParamToNc THEN
			hFile := 0;
			unRow := 0;
			unColumn := 0;
			MEMSET(ADR(Parameter), 0, SIZEOF(stAxisParam));
			b8ReadCsvStep := 1;
			DEFAULT_CSV_FIELD_SEP := 16#3B; //Field separator = semi colon
		END_IF

	1:	(* Open source file *)
		fbFileOpen(bExecute := FALSE);
		fbFileOpen( 
			sNetId := sNetId, 
			sPathName := CONCAT(CONCAT(CONCAT(CONCAT(CONCAT(sDirectoryName, sFileName), UINT_TO_STRING(unAxisId)), '_'), sAxisName), '.csv'),
			nMode := FOPEN_MODEREAD OR FOPEN_MODETEXT, (* Open file in BINARY mode! *)
			ePath := PATH_GENERIC, 
			bExecute := TRUE);
		b8ReadCsvStep := 2;

	2:(* Wait until open not busy *)
		fbFileOpen(bExecute := FALSE, hFile => hFile);
		IF NOT fbFileOpen.bBusy THEN
			IF NOT fbFileOpen.bError THEN
				b8ReadCsvStep := 3;				
			ELSE
				//File not found = create file
			 	bWriteParamToCsv := fbFileOpen.nErrId = 1804;		
				b8ReadCsvStep := 100;
			END_IF
		END_IF

	3:	(* Read single line (record) *)
		fbFileGets(bExecute := FALSE);
		fbFileGets(sNetId := sNetId, hFile := hFile, bExecute := TRUE);
		b8ReadCsvStep := 4;

	4:	(* Wait until read not busy *)
		fbFileGets(bExecute := FALSE, sLine => sCSVLine );
		IF NOT fbFileGets.bBusy THEN
			IF NOT fbFileGets.bError THEN
				IF fbFileGets.bEOF THEN
					b8ReadCsvStep := 10;(* End of file reached => Close source file *)
				ELSE
					b8ReadCsvStep := 5;
				END_IF
			ELSE(* Error *)
				b8ReadCsvStep := 100;
			END_IF
		END_IF

	5:	(* Parse single line (record) *)
		sCSVLine := DELETE(sCSVLine, FIND(sCSVLine,';') + 1, 0);
		sCSVLine := DELETE(sCSVLine, 1, FIND(sCSVLine,'$N'));
		//Axis description is at first row
		IF unRow = 0 THEN
			Hmi.sDescription := sCSVLine; 
		//Rest of rows are parameters
		ELSE
			pData := ADR(Parameter) + 4 * unRow; 
			pData^ := STRING_TO_REAL(sCSVLine); 
		END_IF
		unRow := unRow + 1;
		b8ReadCsvStep := 3;

			
	10:	(* Close source file *)
		fbFileClose(bExecute := FALSE);
		fbFileClose(
			sNetId := sNetId,
			hFile := hFile, 
			bExecute := TRUE );
		b8ReadCsvStep := 11;

	11:(* Wait until close not busy *)
		fbFileClose(bExecute := FALSE);
		IF (NOT fbFileClose.bBusy) THEN
			hFile := 0;
			b8ReadCsvStep := 100;
		END_IF

	100: (* Error or ready b8ReadCsvStep => cleanup *)
		IF ( hFile <> 0 ) THEN
			b8ReadCsvStep := 10; (* Close the source file *)
		ELSE
			b8ReadCsvStep := 0;	(* Ready *)
		END_IF
		bReadParamFromCsv := FALSE;
END_CASE




]]>
      </ST>
    </Implementation>
  </Method>
  <Method Name="ResetAxis" Id="{0C730958-248D-4C08-AACC-44F80478B9D2}">
    <Declaration>
      <![CDATA[METHOD PUBLIC ResetAxis
			]]>
    </Declaration>
    <Implementation>
      <ST>
        <![CDATA[IF AxisRef.Status.Error OR unStatusWord.3 THEN
	fbReset.Execute := TRUE;
END_IF
]]>
      </ST>
    </Implementation>
  </Method>
  <Method Name="StopAxis" Id="{65FF2509-6BE4-40D1-9659-605D6A121F99}">
    <Declaration>
      <![CDATA[METHOD PUBLIC StopAxis
VAR_INPUT
	rDecel: REAL;
	rJerk: REAL;
END_VAR
			]]>
    </Declaration>
    <Implementation>
      <ST>
        <![CDATA[fbMcStop.Deceleration := rDecel;
fbMcStop.Jerk := rJerk;
fbMcStop.Execute := TRUE;
]]>
      </ST>
    </Implementation>
  </Method>
  <Method Name="WriteParamToCsv" Id="{98810BA6-12C0-459E-B42C-A127EF431DD0}">
    <Declaration>
      <![CDATA[METHOD PRIVATE WriteParamToCsv : BOOL

VAR_INPUT
END_VAR
			]]>
    </Declaration>
    <Implementation>
      <ST>
        <![CDATA[CASE b8WriteCsvStep OF
	0:	(* Wait for rising edge at bWrite variable *)
		IF bWriteParamToCsv AND NOT bWriteParamToNc THEN
			hFile	:= 0;
			unRow	:= 0;
			unColumn := 0;
			b8WriteCsvStep 	:= 1;
			unNumberParam := SIZEOF(stAxisParam) / 4;
		END_IF

	1:	(* Open source file *)
		fbFileOpen(bExecute := FALSE);
		fbFileOpen( 
			sNetId := sNetId, 
			sPathName := CONCAT(CONCAT(CONCAT(CONCAT(CONCAT(sDirectoryName, sFileName), UINT_TO_STRING(unAxisId)), '_'), sAxisName), '.csv'),
			nMode := FOPEN_MODEWRITE OR FOPEN_MODETEXT, (* Open file in BINARY mode! *)
			ePath := PATH_GENERIC, 
			bExecute := TRUE);
		b8WriteCsvStep := 2;

	2:(* Wait until open not busy *)
		fbFileOpen( bExecute := FALSE, hFile => hFile);
		IF NOT fbFileOpen.bBusy THEN
			IF NOT fbFileOpen.bError THEN
				b8WriteCsvStep := 3;
			ELSE(* Error: file not found? *)
				b8WriteCsvStep := 100;
			END_IF
		END_IF

	3:(* Convert one PLC record to CSV format *)
		sCSVLine := '';
		fbWriter.eCmd := eEnumCmd_First;(* Write first field value *)
		IF unRow > 0 AND unRow <= unNumberParam - 1 THEN

			FOR unColumn := 0 TO 1 DO

				CASE unColumn OF		
					0: 
						sCSVField := STRING_TO_CSVFIELD(sPARAM_NAME[unRow], FALSE);
					1: 	
						pData := ADR(Parameter) + (unRow * SIZEOF(REAL));
						sCSVField := STRING_TO_CSVFIELD(REAL_TO_STRING(pData^), FALSE);
				END_CASE
					
				(* Add new field to the record buffer *)
				fbWriter(
					pBuffer := ADR(sCSVLine), 
					cbBuffer := SIZEOF(sCSVLine) - 1, 
					putValue := sCSVField, 
					pValue := 0, 
					cbValue := 0,
					bCRLF := unColumn = 1);(* bCRLF == TRUE => Write CRLF after the last field value *)
				
				IF fbWriter.bOk THEN
					fbWriter.eCmd := eEnumCmd_Next;(* Write next field value *)
				ELSE(* Error *)
					b8WriteCsvStep := 100;
					RETURN;
				END_IF
			END_FOR(* FOR nColumn := 0... *)

			(* FB_FilePuts adds allready CR (carriage return) to the written line. We have to replace the $R$L characters with $L character to avoid double CR. *)
			IF RIGHT( sCSVLine, 2 ) = '$R$L' THEN
				sCSVLine := REPLACE(sCSVLine, '$L', 2, LEN(sCSVLine) - 1);
			END_IF
			
			unRow := unRow + 1;(* Increment number of created records (rows) *)
			b8WriteCsvStep := 4;(* Write record to the file *)
		
		//Axis description is written on first line
		ELSIF unRow = 0 THEN
			
			(* Add new field to the record buffer *)
			fbWriter(
				pBuffer := ADR(sCSVLine), 
				cbBuffer := SIZEOF(sCSVLine) - 1, 
				putValue := Hmi.sDescription, 
				pValue := 0, 
				cbValue := 0,
				bCRLF := TRUE);(* bCRLF == TRUE => Write CRLF after the last field value *)
			
			IF NOT fbWriter.bOk THEN
				b8WriteCsvStep := 100;
			END_IF
			
			(* FB_FilePuts adds allready CR (carriage return) to the written line. We have to replace the $R$L characters with $L character to avoid double CR. *)
			IF RIGHT( sCSVLine, 2 ) = '$R$L' THEN
				sCSVLine := REPLACE(sCSVLine, '$L', 2, LEN(sCSVLine) - 1);
			END_IF
			
			unRow := unRow + 1;(* Increment number of created records (rows) *)
			b8WriteCsvStep := 4;(* Write record to the file *)			
			
		//All rows written => Close file
		ELSE
			b8WriteCsvStep := 10;
		END_IF

	4:	(* Write single text line *)
		fbFilePuts( bExecute := FALSE );
		fbFilePuts( sNetId := sNetId, hFile := hFile, sLine := sCSVLine, bExecute := TRUE );
		b8WriteCsvStep := 5;

	5:(* Wait until write not busy *)
		fbFilePuts( bExecute := FALSE);
		IF NOT fbFilePuts.bBusy THEN
			IF NOT fbFilePuts.bError THEN
				b8WriteCsvStep := 3;(* Write next record *)
			ELSE(* Error *)
				b8WriteCsvStep := 100;
			END_IF
		END_IF

	10:	(* Close source file *)
		fbFileClose(bExecute := FALSE);
		fbFileClose(
			sNetId := sNetId, 
			hFile := hFile, 
			bExecute := TRUE);
		b8WriteCsvStep := 11;

	11:(* Wait until close not busy *)
		fbFileClose(bExecute := FALSE);
		IF (NOT fbFileClose.bBusy) THEN
			hFile := 0;
			b8WriteCsvStep := 100;
		END_IF

	100: (* Error or ready b8WriteCsvStep => cleanup *)
		IF (hFile <> 0) THEN
			b8WriteCsvStep := 10; (* Close the source file *)
		ELSE
			b8WriteCsvStep := 0;(* Ready *)
		END_IF
		bWriteParamToCsv := FALSE;
END_CASE




]]>
      </ST>
    </Implementation>
  </Method>
  <Method Name="WriteParamToNc" Id="{70B15506-EC19-4424-A632-E0425B79409F}">
    <Declaration>
      <![CDATA[METHOD PRIVATE WriteParamToNc : BOOL

VAR_INPUT
END_VAR
			]]>
    </Declaration>
    <Implementation>
      <ST>
        <![CDATA[IF bWriteParamToNc AND (fbWriteParameter.Done OR unWriteParamIdx = 0) THEN
	unWriteParamIdx := unWriteParamIdx + 1;
	bParamNcInit := FALSE;
END_IF

//Write param done
IF unWriteParamIdx > 8 THEN
	bWriteParamToNc := FALSE;
	unWriteParamIdx := 0;
	tLastWriteParamDelay := tonWriteParam.ET;
	bParamNcInit := TRUE;
	bPositionSetReq := FALSE;
//Write param failed
ELSIF tonWriteParam.Q THEN
	bWriteParamToNc := FALSE;
	unWriteParamIdx := 0;
	bPositionSetReq := FALSE;
END_IF

tonWriteParam(IN := bWriteParamToNc, PT := T#10S);

CASE unWriteParamIdx OF
	
	//Feedback offset
	1: 	
		IF 	unWriteParamIdx <> unOldWriteParamIdx THEN
			//NC does not accept encoder offset = 0 so we jump if it's the case
			fbWriteParameter.ParameterNumber := MC_AxisParameter.AxisEncoderOffset; 
			fbWriteParameter.Value := Parameter.rEncoderOffset;
			fbWriteParameter.Execute := TRUE;
		END_IF	
		
	//Hi soft limit
	2: 	
		IF 	unWriteParamIdx <> unOldWriteParamIdx THEN
			fbWriteParameter.ParameterNumber := MC_AxisParameter.AxisMaxSoftPosLimit; 
			fbWriteParameter.Value := Parameter.rHiSoftLimit;
			fbWriteParameter.Execute := TRUE;
		END_IF
		
	//Low soft limit
	3: 	
		IF 	unWriteParamIdx <> unOldWriteParamIdx THEN
			fbWriteParameter.ParameterNumber := MC_AxisParameter.AxisMinSoftPosLimit; 
			fbWriteParameter.Value := Parameter.rLowSoftLimit;
			fbWriteParameter.Execute := TRUE;
		END_IF

	//Following error
	4: 	
		IF 	unWriteParamIdx <> unOldWriteParamIdx THEN
			fbWriteParameter.ParameterNumber := MC_AxisParameter.AxisMaxPosLagValue; 
			fbWriteParameter.Value := Parameter.rFollowingErrorLimit;
			fbWriteParameter.Execute := TRUE;
		END_IF
			
	//Max Speed
	5:
		IF 	unWriteParamIdx <> unOldWriteParamIdx THEN
			fbWriteParameter.ParameterNumber := MC_AxisParameter.AxisMaxVelocity; 
			fbWriteParameter.Value := Parameter.rMaxSpeed;
			fbWriteParameter.Execute := TRUE;
		END_IF	
				
	//Default Jerk
	6:
		IF 	unWriteParamIdx <> unOldWriteParamIdx THEN
			fbWriteParameter.ParameterNumber := MC_AxisParameter.MaxJerkAppl; 
			fbWriteParameter.Value := Parameter.rDefaultJerk;
			fbWriteParameter.Execute := TRUE;
		END_IF	
		
	//Enable Hi soft limit
	7:
		IF 	unWriteParamIdx <> unOldWriteParamIdx THEN
			fbWriteParameter.ParameterNumber := MC_AxisParameter.AxisEnMaxSoftPosLimit; 
			fbWriteParameter.Value := BOOL_TO_REAL(NOT bSelectBypassSoftLimits);
			fbWriteParameter.Execute := TRUE;
		END_IF
		
	//Enable Lo soft limit
	8:
		IF 	unWriteParamIdx <> unOldWriteParamIdx THEN
			fbWriteParameter.ParameterNumber := MC_AxisParameter.AxisEnMinSoftPosLimit; 
			fbWriteParameter.Value := BOOL_TO_REAL(NOT bSelectBypassSoftLimits);
			fbWriteParameter.Execute := TRUE;
		END_IF
END_CASE

unOldWriteParamIdx := unWriteParamIdx;

]]>
      </ST>
    </Implementation>
  </Method><ObjectProperties /></POU></TcPlcObject>