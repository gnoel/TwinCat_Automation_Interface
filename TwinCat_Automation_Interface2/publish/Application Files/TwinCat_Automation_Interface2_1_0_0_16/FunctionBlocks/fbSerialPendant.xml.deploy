﻿<?xml version="1.0" encoding="utf-8"?><TcPlcObject Version="1.0.0.0"><POU Name="fbSerialPendant" Id="{2DB5A0C0-A36D-4CC8-865C-2870D3EAAA19}"><Declaration>
	<![CDATA[FUNCTION_BLOCK fbSerialPendant

VAR_INPUT
	bLtAlarm		: BOOL;
	bLtFnc			: BOOL;
	bLtRun			: BOOL;
	bLtReset 		AT %Q*: BOOL;
END_VAR

VAR_OUTPUT
	bPbReset 		AT %I*: BOOL;
	bPbAlarm		: BOOL;
	bPbFnc			: BOOL;
	bToggleUp		: BOOL;
	bToggleDown		: BOOL;
	b8AddressSel	: BYTE;
	b8SpeedSel		: BYTE;
	b8ModeSel		: BYTE;
	bLostSerialComm	: BOOL;	
END_VAR

VAR_IN_OUT
	aoMotionUnidrive	: ARRAY[*] OF fbMotionUnidrive;
	aoDoorKnife			: ARRAY[*] OF fbDcActuator;
	aoDoubleKnives		: ARRAY[*] OF fbDoubleDcActuator;
	aeTypes				: ARRAY[*] OF eAxisType;
END_VAR

VAR PERSISTENT
	aAxisSelector	: ARRAY[0..99] OF stPendantAxisSelector;
END_VAR

VAR
	//Beckhoff EL6021 Inputs
	unStatus AT %I* : UINT;
	aDataIn AT %I* : ARRAY[0..21] OF BYTE;
	bTransmitAccepted : BOOL;
	bReceiveRequest : BOOL;
	bInitAccepted : BOOL;
	bBufferFull : BOOL;
	bParityError : BOOL;
	bFramingError : BOOL;
	bOverrunError : BOOL;
	unInputLength : UINT;
	
	//Beckhoff EL6021 Outputs
	unControl AT %Q* : UINT;
	aDataOut AT %Q* : ARRAY[0..21] OF BYTE;
	bTransmitRequest : BOOL;
	bReceiveAccepted : BOOL;
	bInitRequest : BOOL;
	bSendContinuous : BOOL;
	unOutputLenght : UINT;
	
	//Internal Variables
	oFlasher : fbFlasher;			// Flashing light for pendant
	nRxMachineState: INT := nRX_INIT;
	nRxMachineStateOld: INT := nRX_INIT;
	nTxMachineState: INT := nTX_INIT;
	aRxBuffer: ARRAY[0..100] OF BYTE;
	aTxBuffer: ARRAY[0..100] OF BYTE;
	unWriteIndex: UINT;
	unReadIndex: UINT;
	unBytesReceivedCount: UINT;
	b8BccMessage: BYTE;
	b8BccCalculated: BYTE;
	bReceiveRequestOld: BOOL;
	bTransmitAcceptedOld: BOOL;
	bRxInactiv: BOOL;
	bSendData: BOOL;
	bSendBusy: BOOL;
	bResetLostCommTimer: BOOL;
	bDataReceived: BOOL;
	
	//Timers
	tonRxTimeOutInactiv : TON;
	tonRxTimeOutInactivPT : TIME := T#500MS;
	tonSendTimeOut: TON;
	tonTramDelay: TON;
	tTramDelay: TIME;
	
	ab8RxBuffer: ARRAY[0..9] OF BYTE;
	ab8TxBuffer:  ARRAY[0..9] OF BYTE;
	
	b8Bcc: BYTE;
	bClock: BOOL;
	tonLostComm: ton;
	i: INT;
	
	unAddressIdx	: UINT;	
	unAxisIdx		: UINT;		
	tonFncButton	: TON;
	bOldTonFncButton: BOOL;
	bOldFncButton	: BOOL;
	bOldToggleUp	: BOOL;
	bOldToggleDown	: BOOL;
	dVerifyInit		: DINT;

	unAxisStartIdx	: UINT;
	unAxisEndIdx	: UINT;
END_VAR

VAR CONSTANT
	//Message constants
	b8READ_MESSAGE_START_CODE : BYTE := 2;
	b8WRITE_MESSAGE_START_CODE : BYTE := 29;
	b8MESSAGE_SIZE : BYTE := 10;
	
	//Write State Machine 
	nTX_INIT: INT := 0;
	nTX_IDDLE: INT := 1;
	nTX_SEND_MESSAGE: INT := 2;
	nTX_WAIT_APPROVAL: INT := 3;

	//Read State Machine 
	nRX_INIT : INT := 0;
	nRX_IDLE : INT := 1; 
	nRX_COPY_DATA : INT := 2;
	nRX_SEARCH_MESSAGE : INT := 3;
 	nRX_VERIFY_CHECKSUM : INT := 4;  
	nRX_DELIVER_MESSAGE : INT := 5;
END_VAR

]]></Declaration><Implementation><ST>
	<![CDATA[
MainSerialPendant();

]]></ST></Implementation>
	<Method Name="AxisControl" Id="{3841dea5-f706-49b6-bf0e-af21098f1d42}">
		<Declaration>
			<![CDATA[METHOD AxisControl : BOOL
VAR_INPUT
END_VAR
			]]>
		</Declaration>
		<Implementation>
			<ST>
				<![CDATA[//Check if Axis Selector array is initiated by looking at 10 first addresses
dVerifyInit := 0;
FOR unAddressIdx := 1 TO 10 DO
	FOR unAxisIdx := 0 TO unAxisEndIdx DO
		IF aAxisSelector[unAddressIdx].abAxis[unAxisIdx] THEN
			dVerifyInit := dVerifyInit + 1;
		END_IF
	END_FOR
END_FOR

//Set Axis Selector array if not initiated
IF dVerifyInit = 0 THEN
	SetAxisSelector();
END_IF

//Reset Pendant output
bLtFnc := FALSE;
bLtAlarm := FALSE;
bLtRun := FALSE;

FOR unAxisIdx := unAxisStartIdx TO unAxisEndIdx DO

	//Reset Axis pendant cmd
	aoMotionUnidrive[unAxisIdx].bJogFwd := FALSE;
	aoMotionUnidrive[unAxisIdx].bJogRev := FALSE;
	aoMotionUnidrive[unAxisIdx].bBypassSoftLimits := FALSE;
	aoMotionUnidrive[unAxisIdx].bBypassHardLimits := FALSE;
	
	//Reset aoDoorKnife pendant cmd
	aoDoorKnife[unAxisIdx].bJogFwd := FALSE;
	aoDoorKnife[unAxisIdx].bJogRev := FALSE;
	
	//Reset Double Knives pendant cmd
	aoDoubleKnives[unAxisIdx].bJogFwd := FALSE;
	aoDoubleKnives[unAxisIdx].bJogRev := FALSE;
	
	//Comm is OK with the pendant
	IF NOT bLostSerialComm THEN
		aoMotionUnidrive[unAxisIdx].bEnable := FALSE;
	END_IF
	
	//Axis selected		
	IF aAxisSelector[b8AddressSel].abAxis[unAxisIdx] THEN
					
		//****************************In case of M700 drive axis***********************************\\	
		IF aeTypes[unAxisIdx] = AXIS_TYPE_M700_UNIDRIVE THEN	
			
			IF bPbAlarm THEN 
				aoMotionUnidrive[unAxisIdx].ResetAxis(); 
			END_IF
			
			bLtAlarm := bLtAlarm OR aoMotionUnidrive[unAxisIdx].AxisRef.Status.Error OR aoMotionUnidrive[unAxisIdx].unStatusWord.3; 			
			bLtRun := bLtRun OR aoMotionUnidrive[unAxisIdx].unStatusWord.2;
			
			//Mode selector
			CASE b8ModeSel OF
				
				//Mode A: Jog
				0: 		
					aoMotionUnidrive[unAxisIdx].bJogFwd := bToggleUp;
					aoMotionUnidrive[unAxisIdx].bJogRev := bToggleDown;
					aoMotionUnidrive[unAxisIdx].rJogSpeed := aoMotionUnidrive[unAxisIdx].Parameter.rJogMaxSpeed * b8SpeedSel / 10;
									
					bLtFnc :=	bLtFnc OR aoMotionUnidrive[unAxisIdx].bTestMode
								OR ((aoMotionUnidrive[unAxisIdx].AxisRef.Status.SoftLimitMaxExceeded
								OR aoMotionUnidrive[unAxisIdx].AxisRef.Status.SoftLimitMinExceeded) AND oFlasher.b1On1Off);
					
					//Axis test mode activation on falling edge of function button
					IF bPbFnc AND NOT bOldFncButton THEN
						aoMotionUnidrive[unAxisIdx].bTestMode := NOT aoMotionUnidrive[unAxisIdx].bTestMode;
					END_IF
										
					aoMotionUnidrive[unAxisIdx].rTestSpeed := aoMotionUnidrive[unAxisIdx].Parameter.rMaxSpeed * b8SpeedSel / 10;
					
					//Special for MGM: Axis 38 and 39 need to be synchronized in mode A
					IF b8AddressSel = 38 THEN
						aAxisSelector[b8AddressSel].abAxis[39] := TRUE; 
					ELSIF b8AddressSel = 39 THEN
						aAxisSelector[b8AddressSel].abAxis[38] := TRUE; 
					END_IF
								
				//Mode B: Position reference
				1:					
					//Axis Position set				
					tonFncButton.IN := bPbFnc;
					
					//Hold Function alone to set Position Reference (Zero offset)				
					IF tonFncButton.Q AND NOT bOldTonFncButton AND NOT bToggleUp AND NOT bToggleDown THEN					
						aoMotionUnidrive[unAxisIdx].PositionSet(aoMotionUnidrive[unAxisIdx].Parameter.rReferencePosition);
								
					//Hold Function and Toggle Up to set High Soft Limit					
					ELSIF tonFncButton.Q AND NOT bOldTonFncButton AND bToggleUp THEN					
						aoMotionUnidrive[unAxisIdx].Parameter.rHiSoftLimit := LREAL_TO_REAL(aoMotionUnidrive[unAxisIdx].AxisRef.NcToPlc.ActPos);								
					//Hold Function and Toggle Down to set Low Soft Limit					
					ELSIF tonFncButton.Q AND NOT bOldTonFncButton AND bToggleDown THEN					
						aoMotionUnidrive[unAxisIdx].Parameter.rLowSoftLimit := LREAL_TO_REAL(aoMotionUnidrive[unAxisIdx].AxisRef.NcToPlc.ActPos);								
					END_IF
					
					bLtFnc := (oFlasher.b1On1Off AND tonFncButton.IN) OR tonFncButton.Q;			
					bOldTonFncButton := tonFncButton.Q;				
					aoMotionUnidrive[unAxisIdx].bTestMode := FALSE;
									
				//Mode C: Effect Sequence mode
				2:
					//To Do
					
				//Mode D: Brake disengage
				3:
					aoMotionUnidrive[unAxisIdx].bEnable := bPbFnc;
					aoMotionUnidrive[unAxisIdx].bJogFwd := bToggleUp AND bPbFnc;
					aoMotionUnidrive[unAxisIdx].bJogRev := bToggleDown AND bPbFnc;
					aoMotionUnidrive[unAxisIdx].rJogSpeed := aoMotionUnidrive[unAxisIdx].Parameter.rJogMaxSpeed * b8SpeedSel / 10;	
					aoMotionUnidrive[unAxisIdx].bTestMode := FALSE;			
				
				//Mode E: Synchro mode off
				4:
					IF bPbFnc THEN				//Bypass button pressed (Function button on the pendant)
						//bSynchroBypass := TRUE;			//Bypasses the synchronisation on the axis 38 and 39
						aoMotionUnidrive[unAxisIdx].bJogFwd := bToggleUp;
						aoMotionUnidrive[unAxisIdx].bJogRev := bToggleDown;
						aoMotionUnidrive[unAxisIdx].rJogSpeed := aoMotionUnidrive[unAxisIdx].Parameter.rJogMaxSpeed * b8SpeedSel / 10;								
					END_IF
					
					//Special for MGM: Axis 38 and 39 need to be desynchronized in mode E
					IF b8AddressSel = 38 THEN
						aAxisSelector[b8AddressSel].abAxis[39] := FALSE; 
					ELSIF b8AddressSel = 39 THEN
						aAxisSelector[b8AddressSel].abAxis[38] := FALSE; 
					END_IF
					
					aoMotionUnidrive[unAxisIdx].bTestMode := FALSE;
				
				//Mode F: Bypass hard limits
				5:
					IF bPbFnc THEN				//Bypass button pressed (Alarm button on the pendant)
						aoMotionUnidrive[unAxisIdx].bBypassHardLimits := TRUE;
						aoMotionUnidrive[unAxisIdx].bJogFwd := bToggleUp;
						aoMotionUnidrive[unAxisIdx].bJogRev := bToggleDown;
						aoMotionUnidrive[unAxisIdx].rJogSpeed := aoMotionUnidrive[unAxisIdx].Parameter.rJogMaxSpeed * b8SpeedSel / 10;						
					END_IF
					
					aoMotionUnidrive[unAxisIdx].bTestMode := FALSE;
					
				//Mode G: Bypass soft limits
				6:
					IF bPbFnc THEN				//Bypass button pressed (Alarm button on the pendant)
						aoMotionUnidrive[unAxisIdx].bBypassSoftLimits := TRUE;
						aoMotionUnidrive[unAxisIdx].bJogFwd := bToggleUp;
						aoMotionUnidrive[unAxisIdx].bJogRev := bToggleDown;
						aoMotionUnidrive[unAxisIdx].rJogSpeed := aoMotionUnidrive[unAxisIdx].Parameter.rJogMaxSpeed * b8SpeedSel / 10;
					END_IF				
					aoMotionUnidrive[unAxisIdx].bTestMode := FALSE;
				
				//Mode H: Bypass all limits
				7:
					IF bPbFnc THEN				//Bypass button pressed (Alarm button on the pendant)
						aoMotionUnidrive[unAxisIdx].bBypassSoftLimits := TRUE;
						aoMotionUnidrive[unAxisIdx].bBypassHardLimits := TRUE;
						aoMotionUnidrive[unAxisIdx].bJogFwd := bToggleUp;
						aoMotionUnidrive[unAxisIdx].bJogRev := bToggleDown;
						aoMotionUnidrive[unAxisIdx].rJogSpeed := aoMotionUnidrive[unAxisIdx].Parameter.rJogMaxSpeed * b8SpeedSel / 10;
					END_IF					
					aoMotionUnidrive[unAxisIdx].bTestMode := FALSE;
			
				//Not in a pendant supported mode		
				ELSE
					aoMotionUnidrive[unAxisIdx].bTestMode := FALSE;
			END_CASE			
			
		//****************************In case of locking knives axis***********************************\\
		ELSIF aeTypes[unAxisIdx] = AXIS_TYPE_DC_ACTUATOR THEN
												
			//Mode selector
			CASE b8ModeSel OF
					
				//Mode A: Jog
				0: 		
					aoDoorKnife[unAxisIdx].bJogRev := bToggleDown;
					aoDoorKnife[unAxisIdx].bJogFwd := bToggleUp;		
								
				//Mode B: Position reference
				//1:
			
				//Mode C: not used
				//2:
								
				//Mode D: not used
				//3:	
					
				//Mode E: not used
				//4:
					
				//Mode F: not used
				//5:
					
				//Mode G: not used
				//6:
		
				//Mode H: not used
				//7:
			END_CASE				
	
		//****************************In case of double locking knives axis******************************\\
		ELSIF aeTypes[unAxisIdx] = AXIS_TYPE_DOUBLE_DC_ACTUATOR THEN
							
			//Mode selector
			CASE b8ModeSel OF
					
				//Mode A: Jog
				0: 		
					aoDoubleKnives[unAxisIdx].bJogRev := bToggleDown;
					aoDoubleKnives[unAxisIdx].bJogFwd := bToggleUp;		
								
				//Mode B: Position reference
				//1:
			
				//Mode C: not used
				//2:
								
				//Mode D: not used
				//3:	
					
				//Mode E: not used
				//4:
					
				//Mode F: not used
				//5:
					
				//Mode G: not used
				//6:
		
				//Mode H: not used
				//7:
				
			END_CASE
		END_IF
	END_IF
END_FOR

tonFncButton(PT := T#3S);
bOldFncButton := bPbFnc;
bOldToggleUp := bToggleUp;
bOldToggleDown := bToggleDown;

]]>
			</ST>
		</Implementation>
	</Method>
  <Method Name="FlushBuffer" Id="{3841dea5-f706-49b6-bf0e-af21298f1d42}">
    <Declaration>
      <![CDATA[METHOD PRIVATE FlushBuffer
VAR_INPUT
	pBuffer : POINTER TO BYTE;
	unBufferLenght : UINT;
END_VAR
VAR
	i: UINT := 0;
END_VAR
			]]>
    </Declaration>
    <Implementation>
      <ST>
        <![CDATA[FOR i :=0 TO (unBufferLenght - 1) DO
    pBuffer^ :=0;
	pBuffer := pBuffer + 1;
END_FOR 
unReadIndex :=0;
unWriteIndex :=0;
]]>
      </ST>
    </Implementation>
  </Method>
  <Method Name="MainSerialPendant" Id="{3841dea5-f706-42b6-bf0e-af21098f1d42}">
    <Declaration>
      <![CDATA[METHOD MainSerialPendant
VAR_INPUT
END_VAR


			]]>
    </Declaration>
    <Implementation>
      <ST>
        <![CDATA[ReceiveData();

//Time out timer
tonLostComm(IN := NOT bResetLostCommTimer, PT := T#1S);
bResetLostCommTimer := FALSE;

//Data was received
IF bDataReceived THEN
		
	//Reset lost comm timer
	bResetLostCommTimer := TRUE;
	
	// RX Data mapping
	b8AddressSel := ab8RxBuffer[1];
	bPbAlarm  	 := ab8RxBuffer[4].3;
	b8ModeSel.0  := ab8RxBuffer[4].4;
	b8ModeSel.1  := ab8RxBuffer[4].5;
	b8ModeSel.2  := ab8RxBuffer[4].6;
	bPbFnc 	 	 := ab8RxBuffer[4].7;
	bToggleUp 	 := ab8RxBuffer[3].0;
	bToggleDown  := ab8RxBuffer[3].1;
	b8SpeedSel.0 := ab8RxBuffer[3].2;
	b8SpeedSel.1 := ab8RxBuffer[3].3;
	b8SpeedSel.2 := ab8RxBuffer[3].4;
	b8SpeedSel.3 := ab8RxBuffer[3].5;
	b8SpeedSel 	 := b8SpeedSel + 1;
	bClock 		 := ab8RxBuffer[3].6;
	
	IF NOT bSendBusy THEN
		// Tx Data mapping
		ab8TxBuffer[0] := b8WRITE_MESSAGE_START_CODE;
		ab8TxBuffer[1] := b8AddressSel;
		ab8TxBuffer[5].6 := bLtRun;
		ab8TxBuffer[6].5 := bLtFnc;
		ab8TxBuffer[6].7 := bLtAlarm;
		
		//BCC calculation
		b8Bcc := 0;
		FOR i := 0 TO 8 DO
			b8Bcc := b8Bcc XOR ab8TxBuffer[i];
		END_FOR				
		ab8TxBuffer[9] := b8Bcc;
		
		bSendData := TRUE;
	END_IF
END_IF	

// Communication is lost. Disable all inputs
IF tonLostComm.Q THEN
	b8AddressSel := 0;
	bPbAlarm  	 := 0;
	b8ModeSel    := 0;
	bPbFnc 	 	 := 0;
	bToggleUp 	 := 0;
	bToggleDown  := 0;
	b8SpeedSel   := 0;
END_IF

bLostSerialComm := tonLostComm.Q;

oFlasher();
AxisControl();
SendData();

]]>
      </ST>
    </Implementation>
  </Method>
  <Method Name="ReceiveData" Id="{22B12522-4BB6-484D-AE34-C760569FD2E0}">
    <Declaration>
      <![CDATA[METHOD private ReceiveData

VAR_INPUT
	
END_VAR

VAR
	i : UINT :=0;
END_VAR

VAR_OUTPUT
	
END_VAR

			]]>
    </Declaration>
    <Implementation>
      <ST>
        <![CDATA[// Input Mapping
//bTransmitAccepted:= unStatus.0;
bReceiveRequest:= unStatus.1;
bInitAccepted := unStatus.2;
bBufferFull := unStatus.3;
bParityError := unStatus.4;
bFramingError := unStatus.5;
bOverrunError := unStatus.6;
unInputLength := unStatus / 256;

// Stay TRUE for only one scan
bDataReceived := FALSE;

(*****************************Important Note*************************************
aRxBuffer is an array of 20 bytes and we are looping continuously through the array*)

CASE nRxMachineState OF
	//nRX_INIT:
	0:
		FlushBuffer(ADR(aRxBuffer), SIZEOF(aRxBuffer));
		unWriteIndex := 0;
		unReadIndex := 0;
		unBytesReceivedCount := 0;
		bInitRequest := TRUE;
		bReceiveAccepted := FALSE;
		//nTxMachineState := nTX_INIT;
		//bTransmitAcceptedOld :=0;
		
		IF bInitAccepted THEN
			bInitRequest := FALSE;
			nRxMachineState := nRX_IDLE;
		END_IF
					
	//nRX_IDLE
	1:
		IF bReceiveRequest <> bReceiveRequestOld THEN
			nRxMachineState := nRX_COPY_DATA;
		END_IF
		bReceiveRequestOld := bReceiveRequest;

	//nRX_COPY_DATA
	2:
		IF unInputLength > 0 THEN
			FOR i := 0 TO (unInputLength - 1) DO
				aRxBuffer[(unWriteIndex + i) MOD SIZEOF(aRxBuffer)] := aDataIn[i];
			END_FOR
		
			unWriteIndex := (unWriteIndex + unInputLength) MOD (SIZEOF(aRxBuffer) - 1) ;
		
			unBytesReceivedCount := unBytesReceivedCount + unInputLength;
			IF unBytesReceivedCount >= b8MESSAGE_SIZE THEN 
				nRxMachineState := nRX_SEARCH_MESSAGE;
				unBytesReceivedCount := 0;
			ELSE
				nRxMachineState := nRX_IDLE;
			END_IF
		END_IF
		bReceiveAccepted := NOT bReceiveAccepted ;
		
	//nRX_SEARCH_MESSAGE
	3:
		FOR i := 0 TO SIZEOF(aRxBuffer) - 1 DO
			IF aRxBuffer[i] = b8READ_MESSAGE_START_CODE THEN
				unReadIndex := i;
				b8BccMessage := aRxBuffer[(i + (b8MESSAGE_SIZE - 1)) MOD (SIZEOF(aRxBuffer) - 1)];
				nRxMachineState := nRX_VERIFY_CHECKSUM;
				EXIT;
			END_IF
		END_FOR		
		
	//nRX_VERIFY_CHECKSUM
	4:
		b8BccCalculated := 0;
		FOR i := 0 TO b8MESSAGE_SIZE - 2 DO // -2 to exclude the BccMessage for the BccCalculated loop
			b8BccCalculated := b8BccCalculated XOR aRxBuffer[(unReadIndex + i) MOD (SIZEOF(aRxBuffer) - 1)];
		END_FOR
		
		IF b8BccMessage = b8BccCalculated THEN
			nRxMachineState := nRX_DELIVER_MESSAGE;
		ELSE
			FlushBuffer(ADR(aRxBuffer), SIZEOF(aRxBuffer));
			nRxMachineState := nRX_IDLE;
		END_IF 

	//nRX_DELIVER_MESSAGE
	5:
		bDataReceived := TRUE;
		FOR i :=0 TO b8MESSAGE_SIZE - 1 DO
			IF unReadIndex + i <= SIZEOF(aRxBuffer) - 1 THEN
				ab8RxBuffer[i]:= aRxBuffer[unReadIndex + i];
			ELSE
				ab8RxBuffer[i]:= aRxBuffer[unReadIndex + i - SIZEOF(aRxBuffer)];
			END_IF
		END_FOR
		nRxMachineState := nRX_IDLE;
		FlushBuffer(ADR(aRxBuffer), SIZEOF(aRxBuffer));
		
		tTramDelay := tonTramDelay.ET;	
ELSE
	nRxMachineState := nRX_INIT;
END_CASE

//Initialize if the BeckHoff EL6021 card indicates an error
IF bParityError OR bFramingError OR bOverrunError THEN
	nRxMachineState := nRX_INIT;
END_IF

//Timer to initialize after inactivity time out
IF nRxMachineState = nRxMachineStateOld THEN
	bRxInactiv := TRUE;
ELSE
	bRxInactiv := FALSE;
END_IF
nRxMachineStateOld := nRxMachineState;

tonRxTimeOutInactiv(IN := bRxInactiv, PT := tonRxTimeOutInactivPT);

IF tonRxTimeOutInactiv.Q THEN
	nRxMachineState := nRX_INIT;
END_IF

tonTramDelay(IN := NOT bDataReceived, PT := T#10S);

// Output Mapping
//unControl:= unOutputLenght * 256;
//unControl.0:= bTransmitRequest;
unControl.1:= bReceiveAccepted;
unControl.2:= bInitRequest;
//unControl.3:= bSendContinues;

]]>
      </ST>
    </Implementation>
  </Method>
  <Method Name="SendData" Id="{8F9F64AE-7045-48BF-9EBC-71121397A937}">
    <Declaration>
      <![CDATA[METHOD PRIVATE SendData
VAR_INPUT
END_VAR
VAR
	i : UINT :=0;
END_VAR

			]]>
    </Declaration>
    <Implementation>
      <ST>
        <![CDATA[// Input Mapping
bTransmitAccepted:= unStatus.0;
//bReceiveRequest:= unStatus.1;
//bInitAccepted:= unStatus.2;
//bBufferFull:= unStatus.3;
//bParityError:= unStatus.4;
//bFramingError:= unStatus.5;
//bOverrunError:= unStatus.6;
//unInputLength:= unStatus/256;

CASE nTxMachineState OF

	//nTX_INIT
	0:
		unOutputLenght := 0;
		bTransmitAcceptedOld := 0;
		bSendBusy := FALSE;
		IF nRxMachineState <> nRX_INIT THEN
			nTxMachineState := nTX_IDDLE;
		END_IF	
	
	//nTX_IDDLE
	1:
		IF bSendData THEN
			bSendBusy := TRUE;
			bSendData := FALSE;
			nTxMachineState := nTX_SEND_MESSAGE;
		END_IF
	
	//nTX_SEND_MESSAGE
	2:
		FOR i := 0 TO SIZEOF(ab8TxBuffer) DO
			aDataOut[i] := ab8TxBuffer[i];
		END_FOR
		unOutputLenght := SIZEOF(ab8TxBuffer);
		nTxMachineState := nTX_WAIT_APPROVAL;
		bTransmitRequest := NOT bTransmitRequest;
	
	//nTX_WAIT_APPROVAL
	3:
		IF bTransmitAccepted <> bTransmitAcceptedOld THEN
			bTransmitAcceptedOld := bTransmitAccepted;
			bSendBusy := FALSE;
			nTxMachineState := nTX_IDDLE;
		END_IF	
ELSE		
	nTxMachineState := nTX_INIT;
END_CASE

tonSendTimeOut(IN := bSendBusy, PT := T#1S);
IF tonSendTimeOut.Q THEN
	nTxMachineState := nTX_IDDLE;
	bSendBusy := FALSE;
END_IF

// Output Mapping
unControl:= unControl OR (unOutputLenght * 256);
unControl.0:= bTransmitRequest;
//unControl.1:= bReceiveAccepted;
//unControl.2:= bInitRequest;
unControl.3:= bSendContinuous;

]]>
      </ST>
    </Implementation>
  </Method>
  <Method Name="SetAxisSelector" Id="{0C143115-973C-434A-B680-F5B32E4FEAC4}">
    <Declaration>
      <![CDATA[METHOD SetAxisSelector : BOOL

VAR_INPUT
END_VAR

			]]>
    </Declaration>
    <Implementation>
      <ST>
        <![CDATA[//Reset array
MEMSET(ADR(aAxisSelector), 0, sizeof(aAxisSelector));

//Set default axis for all adresses
FOR unAddressIdx := 1 TO 99 DO
	FOR unAxisIdx := unAxisStartIdx TO unAxisEndIdx DO
		//axis 99 select all axis
		IF unAddressIdx = 99 THEN
			aAxisSelector[unAddressIdx].abAxis[unAxisIdx] := TRUE;
		ELSE	
			aAxisSelector[unAddressIdx].abAxis[unAxisIdx] := unAddressIdx = unAxisIdx;
		END_IF
	END_FOR
END_FOR 





]]>
      </ST>
    </Implementation>
  </Method><ObjectProperties /></POU></TcPlcObject>