﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TwinCat_Automation_Interface2.ViewModels;
using System.ComponentModel;
using TCatSysManagerLib;
using EnvDTE;
using Moq;
using Moq.Matchers;


namespace TwinCat_Automation_UnitTest
{
    [TestClass]
    public class MainWindowViewModelTests
    {
        #region Constructor_Test
        /// <summary>
        /// 
        /// </summary>
        [TestMethod]
        public void MainWindowViewModel_CTor_Test()
        {
            //Arrange
            List<string> expectedList = new List<string>();

            expectedList.Add("Visual Studio 2013");
            expectedList.Add("Visual Studio 2015");



            //Act
            MainWindowViewModel testVM  = new MainWindowViewModel();

            //Assert
            Assert.IsTrue(testVM.CreateDrivesCommand.CanExecute(null), "The CreateDriveCommand canExecute should be true.");


            Assert.IsTrue(testVM.CreateNewProjectCommand.CanExecute(null), "The CreateNewProjectCommand canExecute should be true.");


            Assert.IsTrue(testVM.HelpFileCommand.CanExecute(null), "The HelpFileCommand canExecute should be true.");

            Assert.IsTrue(testVM.ExitCommand.CanExecute(null), "The ExitCommand canExecute should be true.");


            Assert.IsTrue(testVM.CreateDriveListCommand.CanExecute(null), "The OpenDriveListCommand canExecute should be true.");


            Assert.IsTrue(testVM.OpenSolutionCommand.CanExecute(null), "The OpenSolutionCommand canExecute should be true.");

            CollectionAssert.AreEqual(expectedList, testVM.VSVersions, "The VS Version should be equal.");



            Assert.AreEqual(testVM.SelectedPLCName, string.Empty, "The SelectedPLCName should be string empty.");

            Assert.AreEqual(testVM.SelectedVSVersion, "None Selected", "The SelectedVSVersion should be string empty.");

            Assert.AreEqual(testVM.VSVersionNum, string.Empty, "The VSVersionNum should be string empty.");

            Assert.AreEqual(testVM.FileStatus, string.Empty, "The FileStatus should be string empty.");

            Assert.AreEqual(testVM.FileName, string.Empty, "The FileName should be string empty.");

            Assert.AreEqual(testVM.EtherCatMasName, string.Empty, "The EtherCatMasName should be string empty.");

            Assert.AreEqual(testVM.NCTaskName, string.Empty, "The NCTaskName should be string empty.");

            Assert.IsFalse(testVM.HasDrives, "The HasDrives should be equal to false");



            Assert.IsFalse(testVM.HasValidEtherCat, "The HasValidEtherCat should be equal to false");




            Assert.IsFalse(testVM.HasValidMotion, "The HasValidMotion should be equal to false");

            Assert.IsFalse(testVM.HasValidPLC, "The HasValidPLC should be equal to false");

            Assert.IsFalse(testVM.HasValidReference, "The HasValidReference should be equal to false");

            Assert.IsFalse(testVM.HasValidTMC, "The HasValidTMC should be equal to false");

            Assert.IsFalse(testVM.DrivesConfigured, "The DrivesConfigured should be equal to false");


            Assert.IsFalse(testVM.DrivesCreated, "The DrivesCreated should be equal to false");

            Assert.AreEqual(testVM.NumberOfDrive, 1, "The NumberOfDrive should be equal to 1. ");

            Assert.AreEqual(testVM.MinIndexAxisValue, 0, "The MinIndexAxisValue should be equal to 1. ");

            Assert.AreEqual(testVM.MaxIndexAxisValue, 0, "The MaxIndexAxisValue should be equal to 1. ");


            Assert.IsNull(testVM.DTE,  "The MaxIndexAxisValue should be null.");


            Assert.IsNull(testVM.SysManager, "The SysManager should be null.");

            Assert.IsNull(testVM.SysLibrary, "The SysManager should be null.");


        }

        #endregion

        #region Properties_Test

        #region SelectedPLCName Tests

        /// <summary>
        /// The following test the SelectedPLCName is the value is change to the same value.
        /// </summary>
        [TestMethod]
        public void MainWindowViewModel_SelectedPLCName_SameValue() {

            //Arrange
            List<string> receivedEvents = new List<string>();
            MainWindowViewModel testVM = new MainWindowViewModel();
           
            //Set initial value
            testVM.SelectedPLCName = "value";


            testVM.PropertyChanged += delegate (object sender, PropertyChangedEventArgs e) {

                receivedEvents.Add(e.PropertyName);

            };
            
            //Act
            testVM.SelectedPLCName = "value";


            //Assert

            // Verify if the NotifyProperty has not been trigger since it the same value
            Assert.AreEqual(0, receivedEvents.Count,
                "The count should be 0 since its the same value the PropertyChanged should not be triggered.");

            
        }


        /// <summary>
        /// The following test the SelectedPLCName is the value is change to the new value.
        /// </summary>
        [TestMethod]
        public void MainWindowViewModel_SelectedPLCName_NewValue()
        {

            //Arrange
            List<string> receivedEvents = new List<string>();
            MainWindowViewModel testVM = new MainWindowViewModel();

            //Set initial value
            testVM.SelectedPLCName = "value";


            testVM.PropertyChanged += delegate (object sender, PropertyChangedEventArgs e) {

                receivedEvents.Add(e.PropertyName);

            };



            //Act
            testVM.SelectedPLCName = "NewValue";


            //Assert

            // Verify if the NotifyProperty has  been trigger since it the new value
            Assert.AreEqual(1, receivedEvents.Count,
                "The count should be 1 since its the same value the PropertyChanged should not be triggered.");

            Assert.AreEqual("SelectedPLCName", receivedEvents[0], "The name should be equal.");



        }


        #endregion

        #region SelectedVSVersion Tests

        /// <summary>
        /// The following test the SelectedVSVersion is the value is change to the same value.
        /// </summary>
        [TestMethod]
        public void MainWindowViewModel_SelectedVSVersion_SameValue()
        {

            //Arrange
            List<string> receivedEvents = new List<string>();
            MainWindowViewModel testVM = new MainWindowViewModel();

            //Set initial value
            testVM.SelectedVSVersion = "value";


            testVM.PropertyChanged += delegate (object sender, PropertyChangedEventArgs e) {

                receivedEvents.Add(e.PropertyName);

            };

            //Act
            testVM.SelectedVSVersion = "value";


            //Assert

            // Verify if the NotifyProperty has not been trigger since it the same value
            Assert.AreEqual(0, receivedEvents.Count,
                "The count should be 0 since its the same value the PropertyChanged should not be triggered.");


        }


        /// <summary>
        /// The following test the SelectedVSVersion is the value is change to the new value.
        /// </summary>
        [TestMethod]
        public void MainWindowViewModel_SelectedVSVersion_NewValue()
        {

            //Arrange
            List<string> receivedEvents = new List<string>();
            MainWindowViewModel testVM = new MainWindowViewModel();

            //Set initial value
            testVM.SelectedVSVersion = "value";


            testVM.PropertyChanged += delegate (object sender, PropertyChangedEventArgs e) {

                receivedEvents.Add(e.PropertyName);

            };



            //Act
            testVM.SelectedVSVersion = "NewValue";


            //Assert

            // Verify if the NotifyProperty has  been trigger since it the new value
            Assert.AreEqual(1, receivedEvents.Count,
                "The count should be 1 since its the same value the PropertyChanged should not be triggered.");

            Assert.AreEqual("SelectedVSVersion", receivedEvents[0], "The name should be equal.");


        }


        #endregion

        #region VSVersionNum Tests

        /// <summary>
        /// The following test the VSVersionNum is the value is change to the same value.
        /// </summary>
        [TestMethod]
        public void MainWindowViewModel_VSVersionNum_SameValue()
        {

            //Arrange
            List<string> receivedEvents = new List<string>();
            MainWindowViewModel testVM = new MainWindowViewModel();

            //Set initial value
            testVM.VSVersionNum = "value";


            testVM.PropertyChanged += delegate (object sender, PropertyChangedEventArgs e) {

                receivedEvents.Add(e.PropertyName);

            };

            //Act
            testVM.VSVersionNum = "value";


            //Assert

            // Verify if the NotifyProperty has not been trigger since it the same value
            Assert.AreEqual(0, receivedEvents.Count,
                "The count should be 0 since its the same value the PropertyChanged should not be triggered.");


        }


        /// <summary>
        /// The following test the VSVersionNum is the value is change to the new value.
        /// </summary>
        [TestMethod]
        public void MainWindowViewModel_VSVersionNum_NewValue()
        {

            //Arrange
            List<string> receivedEvents = new List<string>();
            MainWindowViewModel testVM = new MainWindowViewModel();

            //Set initial value
            testVM.VSVersionNum = "value";


            testVM.PropertyChanged += delegate (object sender, PropertyChangedEventArgs e) {

                receivedEvents.Add(e.PropertyName);

            };



            //Act
            testVM.VSVersionNum = "NewValue";


            //Assert

            // Verify if the NotifyProperty has  been trigger since it the new value
            Assert.AreEqual(1, receivedEvents.Count,
                "The count should be 1 since its the same value the PropertyChanged should not be triggered.");

            Assert.AreEqual("VSVersionNum", receivedEvents[0], "The name should be equal.");


        }


        #endregion

        #region FileStatus Tests

        /// <summary>
        /// The following test the FileStatus is the value is change to the same value.
        /// </summary>
        [TestMethod]
        public void MainWindowViewModel_FileStatus_SameValue()
        {

            //Arrange
            List<string> receivedEvents = new List<string>();
            MainWindowViewModel testVM = new MainWindowViewModel();

            //Set initial value
            testVM.FileStatus = "value";


            testVM.PropertyChanged += delegate (object sender, PropertyChangedEventArgs e) {

                receivedEvents.Add(e.PropertyName);

            };

            //Act
            testVM.FileStatus = "value";


            //Assert

            // Verify if the NotifyProperty has not been trigger since it the same value
            Assert.AreEqual(0, receivedEvents.Count,
                "The count should be 0 since its the same value the PropertyChanged should not be triggered.");


        }


        /// <summary>
        /// The following test the FileStatus is the value is change to the new value.
        /// </summary>
        [TestMethod]
        public void MainWindowViewModel_FileStatus_NewValue()
        {

            //Arrange
            List<string> receivedEvents = new List<string>();
            MainWindowViewModel testVM = new MainWindowViewModel();

            //Set initial value
            testVM.FileStatus = "value";


            testVM.PropertyChanged += delegate (object sender, PropertyChangedEventArgs e) {

                receivedEvents.Add(e.PropertyName);

            };



            //Act
            testVM.FileStatus = "NewValue";


            //Assert

            // Verify if the NotifyProperty has  been trigger since it the new value
            Assert.AreEqual(1, receivedEvents.Count,
                "The count should be 1 since its the same value the PropertyChanged should not be triggered.");

            Assert.AreEqual("FileStatus", receivedEvents[0], "The name should be equal.");


        }


        #endregion

        #region FileName Tests

        /// <summary>
        /// The following test the FileName is the value is change to the same value.
        /// </summary>
        [TestMethod]
        public void MainWindowViewModel_FileName_SameValue()
        {

            //Arrange
            List<string> receivedEvents = new List<string>();
            MainWindowViewModel testVM = new MainWindowViewModel();

            //Set initial value
            testVM.FileName = "value";


            testVM.PropertyChanged += delegate (object sender, PropertyChangedEventArgs e) {

                receivedEvents.Add(e.PropertyName);

            };

            //Act
            testVM.FileName = "value";


            //Assert

            // Verify if the NotifyProperty has not been trigger since it the same value
            Assert.AreEqual(0, receivedEvents.Count,
                "The count should be 0 since its the same value the PropertyChanged should not be triggered.");


        }


        /// <summary>
        /// The following test the FileName is the value is change to the new value.
        /// </summary>
        [TestMethod]
        public void MainWindowViewModel_FileName_NewValue()
        {

            //Arrange
            List<string> receivedEvents = new List<string>();
            MainWindowViewModel testVM = new MainWindowViewModel();

            //Set initial value
            testVM.FileName = "value";


            testVM.PropertyChanged += delegate (object sender, PropertyChangedEventArgs e) {

                receivedEvents.Add(e.PropertyName);

            };



            //Act
            testVM.FileName = "NewValue";


            //Assert

            // Verify if the NotifyProperty has  been trigger since it the new value
            Assert.AreEqual(1, receivedEvents.Count,
                "The count should be 1 since its the same value the PropertyChanged should not be triggered.");

            Assert.AreEqual("FileName", receivedEvents[0], "The name should be equal.");

        }


        #endregion

        #region EtherCatMasName Tests

        /// <summary>
        /// The following test the EtherCatMasName is the value is change to the same value.
        /// </summary>
        [TestMethod]
        public void MainWindowViewModel_EtherCatMasName_SameValue()
        {

            //Arrange
            List<string> receivedEvents = new List<string>();
            MainWindowViewModel testVM = new MainWindowViewModel();

            //Set initial value
            testVM.EtherCatMasName = "value";


            testVM.PropertyChanged += delegate (object sender, PropertyChangedEventArgs e) {

                receivedEvents.Add(e.PropertyName);

            };

            //Act
            testVM.EtherCatMasName = "value";


            //Assert

            // Verify if the NotifyProperty has not been trigger since it the same value
            Assert.AreEqual(0, receivedEvents.Count,
                "The count should be 0 since its the same value the PropertyChanged should not be triggered.");


        }


        /// <summary>
        /// The following test the EtherCatMasName is the value is change to the new value.
        /// </summary>
        [TestMethod]
        public void MainWindowViewModel_EtherCatMasName_NewValue()
        {

            //Arrange
            List<string> receivedEvents = new List<string>();
            MainWindowViewModel testVM = new MainWindowViewModel();

            //Set initial value
            testVM.EtherCatMasName = "value";


            testVM.PropertyChanged += delegate (object sender, PropertyChangedEventArgs e) {

                receivedEvents.Add(e.PropertyName);

            };



            //Act
            testVM.EtherCatMasName = "NewValue";


            //Assert

            // Verify if the NotifyProperty has  been trigger since it the new value
            Assert.AreEqual(1, receivedEvents.Count,
                "The count should be 1 since its the same value the PropertyChanged should not be triggered.");

            Assert.AreEqual("EtherCatMasName", receivedEvents[0], "The name should be equal.");

        }


        #endregion

        #region NCTaskName Tests

        /// <summary>
        /// The following test the NCTaskName is the value is change to the same value.
        /// </summary>
        [TestMethod]
        public void MainWindowViewModel_NCTaskName_SameValue()
        {

            //Arrange
            List<string> receivedEvents = new List<string>();
            MainWindowViewModel testVM = new MainWindowViewModel();

            //Set initial value
            testVM.NCTaskName = "value";


            testVM.PropertyChanged += delegate (object sender, PropertyChangedEventArgs e) {

                receivedEvents.Add(e.PropertyName);

            };

            //Act
            testVM.NCTaskName = "value";


            //Assert

            // Verify if the NotifyProperty has not been trigger since it the same value
            Assert.AreEqual(0, receivedEvents.Count,
                "The count should be 0 since its the same value the PropertyChanged should not be triggered.");


        }


        /// <summary>
        /// The following test the NCTaskName is the value is change to the new value.
        /// </summary>
        [TestMethod]
        public void MainWindowViewModel_NCTaskName_NewValue()
        {

            //Arrange
            List<string> receivedEvents = new List<string>();
            MainWindowViewModel testVM = new MainWindowViewModel();

            //Set initial value
            testVM.NCTaskName = "value";


            testVM.PropertyChanged += delegate (object sender, PropertyChangedEventArgs e) {

                receivedEvents.Add(e.PropertyName);

            };



            //Act
            testVM.NCTaskName = "NewValue";


            //Assert

            // Verify if the NotifyProperty has  been trigger since it the new value
            Assert.AreEqual(1, receivedEvents.Count,
                "The count should be 1 since its the same value the PropertyChanged should not be triggered.");

            Assert.AreEqual("NCTaskName", receivedEvents[0], "The name should be equal.");

        }


        #endregion

        #region HasDrives Tests

        /// <summary>
        // The following test the HasDrives is the value is change to the same value.
        /// </summary>
        [TestMethod]
        public void MainWindowViewModel_HasDrives_SameValue() {
            //Arrange
            List<string> receivedEvents = new List<string>();
            MainWindowViewModel testVM = new MainWindowViewModel();

            


            testVM.PropertyChanged += delegate (object sender, PropertyChangedEventArgs e) {

                receivedEvents.Add(e.PropertyName);

            };

            //Act
            testVM.HasDrives = false;


            //Assert

            // Verify if the NotifyProperty has not been trigger since it the same value
            Assert.AreEqual(0, receivedEvents.Count,
                "The count should be 0 since its the same value the PropertyChanged should not be triggered.");



        }


        /// <summary>
        /// The following test the HasDrives is the value is change to the new value.
        /// </summary>
        [TestMethod]
        public void MainWindowViewModel_HasDrives_NewValue()
        {

            //Arrange
            List<string> receivedEvents = new List<string>();
            MainWindowViewModel testVM = new MainWindowViewModel();

            


            testVM.PropertyChanged += delegate (object sender, PropertyChangedEventArgs e) {

                receivedEvents.Add(e.PropertyName);

            };



            //Act
            testVM.HasDrives = true;


            //Assert

            // Verify if the NotifyProperty has  been trigger since it the new value
            Assert.AreEqual(1, receivedEvents.Count,
                "The count should be 1 since its the same value the PropertyChanged should not be triggered.");

            Assert.AreEqual("HasDrives", receivedEvents[0], "The name should be equal.");

        }

        #endregion
        
        

        #region HasValidEtherCat Tests

        /// <summary>
        // The following test the HasValidEtherCat is the value is change to the same value.
        /// </summary>
        [TestMethod]
        public void MainWindowViewModel_HasValidEtherCat_SameValue()
        {
            //Arrange
            List<string> receivedEvents = new List<string>();
            MainWindowViewModel testVM = new MainWindowViewModel();




            testVM.PropertyChanged += delegate (object sender, PropertyChangedEventArgs e) {

                receivedEvents.Add(e.PropertyName);

            };

            //Act
            testVM.HasValidEtherCat = false;


            //Assert

            // Verify if the NotifyProperty has not been trigger since it the same value
            Assert.AreEqual(0, receivedEvents.Count,
                "The count should be 0 since its the same value the PropertyChanged should not be triggered.");



        }


        /// <summary>
        /// The following test the HasValidEtherCat is the value is change to the new value.
        /// </summary>
        [TestMethod]
        public void MainWindowViewModel_HasValidEtherCat_NewValue()
        {

            //Arrange
            List<string> receivedEvents = new List<string>();
            MainWindowViewModel testVM = new MainWindowViewModel();




            testVM.PropertyChanged += delegate (object sender, PropertyChangedEventArgs e) {

                receivedEvents.Add(e.PropertyName);

            };



            //Act
            testVM.HasValidEtherCat = true;


            //Assert

            // Verify if the NotifyProperty has  been trigger since it the new value
            Assert.AreEqual(1, receivedEvents.Count,
                "The count should be 1 since its the same value the PropertyChanged should not be triggered.");

            Assert.AreEqual("HasValidEtherCat", receivedEvents[0], "The name should be equal.");

        }

        #endregion

        
        
        #region HasValidMotion Tests

        /// <summary>
        // The following test the HasValidMotion is the value is change to the same value.
        /// </summary>
        [TestMethod]
        public void MainWindowViewModel_HasValidMotion_SameValue()
        {
            //Arrange
            List<string> receivedEvents = new List<string>();
            MainWindowViewModel testVM = new MainWindowViewModel();




            testVM.PropertyChanged += delegate (object sender, PropertyChangedEventArgs e) {

                receivedEvents.Add(e.PropertyName);

            };

            //Act
            testVM.HasValidMotion = false;


            //Assert

            // Verify if the NotifyProperty has not been trigger since it the same value
            Assert.AreEqual(0, receivedEvents.Count,
                "The count should be 0 since its the same value the PropertyChanged should not be triggered.");



        }


        /// <summary>
        /// The following test the HasValidMotion is the value is change to the new value.
        /// </summary>
        [TestMethod]
        public void MainWindowViewModel_HasValidMotion_NewValue()
        {

            //Arrange
            List<string> receivedEvents = new List<string>();
            MainWindowViewModel testVM = new MainWindowViewModel();




            testVM.PropertyChanged += delegate (object sender, PropertyChangedEventArgs e) {

                receivedEvents.Add(e.PropertyName);

            };



            //Act
            testVM.HasValidMotion = true;


            //Assert

            // Verify if the NotifyProperty has  been trigger since it the new value
            Assert.AreEqual(1, receivedEvents.Count,
                "The count should be 1 since its the same value the PropertyChanged should not be triggered.");

            Assert.AreEqual("HasValidMotion", receivedEvents[0], "The name should be equal.");

        }

        #endregion

        #region HasValidPLC Tests

        /// <summary>
        // The following test the HasValidPLC is the value is change to the same value.
        /// </summary>
        [TestMethod]
        public void MainWindowViewModel_HasValidPLC_SameValue()
        {
            //Arrange
            List<string> receivedEvents = new List<string>();
            MainWindowViewModel testVM = new MainWindowViewModel();




            testVM.PropertyChanged += delegate (object sender, PropertyChangedEventArgs e) {

                receivedEvents.Add(e.PropertyName);

            };

            //Act
            testVM.HasValidPLC = false;


            //Assert

            // Verify if the NotifyProperty has not been trigger since it the same value
            Assert.AreEqual(0, receivedEvents.Count,
                "The count should be 0 since its the same value the PropertyChanged should not be triggered.");



        }


        /// <summary>
        /// The following test the HasValidPLC is the value is change to the new value.
        /// </summary>
        [TestMethod]
        public void MainWindowViewModel_HasValidPLC_NewValue()
        {

            //Arrange
            List<string> receivedEvents = new List<string>();
            MainWindowViewModel testVM = new MainWindowViewModel();




            testVM.PropertyChanged += delegate (object sender, PropertyChangedEventArgs e) {

                receivedEvents.Add(e.PropertyName);

            };



            //Act
            testVM.HasValidPLC = true;


            //Assert

            // Verify if the NotifyProperty has  been trigger since it the new value
            Assert.AreEqual(1, receivedEvents.Count,
                "The count should be 1 since its the same value the PropertyChanged should not be triggered.");

            Assert.AreEqual("HasValidPLC", receivedEvents[0], "The name should be equal.");

        }

        #endregion
                
        #region HasValidReference Tests

        /// <summary>
        // The following test the HasValidReference is the value is change to the same value.
        /// </summary>
        [TestMethod]
        public void MainWindowViewModel_HasValidReference_SameValue()
        {
            //Arrange
            List<string> receivedEvents = new List<string>();
            MainWindowViewModel testVM = new MainWindowViewModel();




            testVM.PropertyChanged += delegate (object sender, PropertyChangedEventArgs e) {

                receivedEvents.Add(e.PropertyName);

            };

            //Act
            testVM.HasValidReference = false;


            //Assert

            // Verify if the NotifyProperty has not been trigger since it the same value
            Assert.AreEqual(0, receivedEvents.Count,
                "The count should be 0 since its the same value the PropertyChanged should not be triggered.");



        }


        /// <summary>
        /// The following test the HasValidReference is the value is change to the new value.
        /// </summary>
        [TestMethod]
        public void MainWindowViewModel_HasValidReference_NewValue()
        {

            //Arrange
            List<string> receivedEvents = new List<string>();
            MainWindowViewModel testVM = new MainWindowViewModel();




            testVM.PropertyChanged += delegate (object sender, PropertyChangedEventArgs e) {

                receivedEvents.Add(e.PropertyName);

            };



            //Act
            testVM.HasValidReference = true;


            //Assert

            // Verify if the NotifyProperty has  been trigger since it the new value
            Assert.AreEqual(1, receivedEvents.Count,
                "The count should be 1 since its the same value the PropertyChanged should not be triggered.");

            Assert.AreEqual("HasValidReference", receivedEvents[0], "The name should be equal.");

        }

        #endregion
        
        #region HasValidTMC Tests

        /// <summary>
        // The following test the HasValidTMC is the value is change to the same value.
        /// </summary>
        [TestMethod]
        public void MainWindowViewModel_HasValidTMC_SameValue()
        {
            //Arrange
            List<string> receivedEvents = new List<string>();
            MainWindowViewModel testVM = new MainWindowViewModel();




            testVM.PropertyChanged += delegate (object sender, PropertyChangedEventArgs e) {

                receivedEvents.Add(e.PropertyName);

            };

            //Act
            testVM.HasValidTMC = false;


            //Assert

            // Verify if the NotifyProperty has not been trigger since it the same value
            Assert.AreEqual(0, receivedEvents.Count,
                "The count should be 0 since its the same value the PropertyChanged should not be triggered.");



        }


        /// <summary>
        /// The following test the HasValidTMC is the value is change to the new value.
        /// </summary>
        [TestMethod]
        public void MainWindowViewModel_HasValidTMC_NewValue()
        {

            //Arrange
            List<string> receivedEvents = new List<string>();
            MainWindowViewModel testVM = new MainWindowViewModel();




            testVM.PropertyChanged += delegate (object sender, PropertyChangedEventArgs e) {

                receivedEvents.Add(e.PropertyName);

            };



            //Act
            testVM.HasValidTMC = true;


            //Assert

            // Verify if the NotifyProperty has  been trigger since it the new value
            Assert.AreEqual(1, receivedEvents.Count,
                "The count should be 1 since its the same value the PropertyChanged should not be triggered.");

            Assert.AreEqual("HasValidTMC", receivedEvents[0], "The name should be equal.");

        }

        #endregion

        #region DrivesConfigured Tests

        /// <summary>
        // The following test the DrivesConfigured is the value is change to the same value.
        /// </summary>
        [TestMethod]
        public void MainWindowViewModel_DrivesConfigured_SameValue()
        {
            //Arrange
            List<string> receivedEvents = new List<string>();
            MainWindowViewModel testVM = new MainWindowViewModel();




            testVM.PropertyChanged += delegate (object sender, PropertyChangedEventArgs e) {

                receivedEvents.Add(e.PropertyName);

            };

            //Act
            testVM.DrivesConfigured = false;


            //Assert

            // Verify if the NotifyProperty has not been trigger since it the same value
            Assert.AreEqual(0, receivedEvents.Count,
                "The count should be 0 since its the same value the PropertyChanged should not be triggered.");



        }


        /// <summary>
        /// The following test the DrivesConfigured is the value is change to the new value.
        /// </summary>
        [TestMethod]
        public void MainWindowViewModel_DrivesConfigured_NewValue()
        {

            //Arrange
            List<string> receivedEvents = new List<string>();
            MainWindowViewModel testVM = new MainWindowViewModel();




            testVM.PropertyChanged += delegate (object sender, PropertyChangedEventArgs e) {

                receivedEvents.Add(e.PropertyName);

            };



            //Act
            testVM.DrivesConfigured = true;


            //Assert

            // Verify if the NotifyProperty has  been trigger since it the new value
            Assert.AreEqual(1, receivedEvents.Count,
                "The count should be 1 since its the same value the PropertyChanged should not be triggered.");

            Assert.AreEqual("DrivesConfigured", receivedEvents[0], "The name should be equal.");

        }

        #endregion

        #region DrivesCreated Tests

        /// <summary>
        // The following test the DrivesCreated is the value is change to the same value.
        /// </summary>
        [TestMethod]
        public void MainWindowViewModel_DrivesCreated_SameValue()
        {
            //Arrange
            List<string> receivedEvents = new List<string>();
            MainWindowViewModel testVM = new MainWindowViewModel();




            testVM.PropertyChanged += delegate (object sender, PropertyChangedEventArgs e) {

                receivedEvents.Add(e.PropertyName);

            };

            //Act
            testVM.DrivesCreated = false;


            //Assert

            // Verify if the NotifyProperty has not been trigger since it the same value
            Assert.AreEqual(0, receivedEvents.Count,
                "The count should be 0 since its the same value the PropertyChanged should not be triggered.");



        }


        /// <summary>
        /// The following test the DrivesCreated is the value is change to the new value.
        /// </summary>
        [TestMethod]
        public void MainWindowViewModel_DrivesCreated_NewValue()
        {

            //Arrange
            List<string> receivedEvents = new List<string>();
            MainWindowViewModel testVM = new MainWindowViewModel();




            testVM.PropertyChanged += delegate (object sender, PropertyChangedEventArgs e) {

                receivedEvents.Add(e.PropertyName);

            };



            //Act
            testVM.DrivesCreated = true;


            //Assert

            // Verify if the NotifyProperty has  been trigger since it the new value
            Assert.AreEqual(1, receivedEvents.Count,
                "The count should be 1 since its the same value the PropertyChanged should not be triggered.");

            Assert.AreEqual("DrivesCreated", receivedEvents[0], "The name should be equal.");

        }

        #endregion

        #region NumberOfDrive Tests

        /// <summary>
        // The following test the NumberOfDrive is the value is change to the same value.
        /// </summary>
        [TestMethod]
        public void MainWindowViewModel_NumberOfDrive_SameValue()
        {
            //Arrange
            List<string> receivedEvents = new List<string>();
            MainWindowViewModel testVM = new MainWindowViewModel();




            testVM.PropertyChanged += delegate (object sender, PropertyChangedEventArgs e) {

                receivedEvents.Add(e.PropertyName);

            };

            //Act
            testVM.NumberOfDrive = 1;


            //Assert

            // Verify if the NotifyProperty has not been trigger since it the same value
            Assert.AreEqual(0, receivedEvents.Count,
                "The count should be 0 since its the same value the PropertyChanged should not be triggered.");



        }


        /// <summary>
        /// The following test the NumberOfDrive is the value is change to the new value.
        /// </summary>
        [TestMethod]
        public void MainWindowViewModel_NumberOfDrive_NewValue()
        {

            //Arrange
            List<string> receivedEvents = new List<string>();
            MainWindowViewModel testVM = new MainWindowViewModel();




            testVM.PropertyChanged += delegate (object sender, PropertyChangedEventArgs e) {

                receivedEvents.Add(e.PropertyName);

            };



            //Act
            testVM.NumberOfDrive = 2;


            //Assert

            // Verify if the NotifyProperty has  been trigger since it the new value
            Assert.AreEqual(1, receivedEvents.Count,
                "The count should be 1 since its the same value the PropertyChanged should not be triggered.");

            Assert.AreEqual("NumberOfDrive", receivedEvents[0], "The name should be equal.");

        }

        #endregion

        #region MinIndexAxisValue Tests

        /// <summary>
        // The following test the MinIndexAxisValue is the value is change to the same value.
        /// </summary>
        [TestMethod]
        public void MainWindowViewModel_MinIndexAxisValue_SameValue()
        {
            //Arrange
            List<string> receivedEvents = new List<string>();
            MainWindowViewModel testVM = new MainWindowViewModel();




            testVM.PropertyChanged += delegate (object sender, PropertyChangedEventArgs e) {

                receivedEvents.Add(e.PropertyName);

            };

            //Act
            testVM.MinIndexAxisValue = 0;


            //Assert

            // Verify if the NotifyProperty has not been trigger since it the same value
            Assert.AreEqual(0, receivedEvents.Count,
                "The count should be 0 since its the same value the PropertyChanged should not be triggered.");



        }


        /// <summary>
        /// The following test the MinIndexAxisValue is the value is change to the new value.
        /// </summary>
        [TestMethod]
        public void MainWindowViewModel_MinIndexAxisValue_NewValue()
        {

            //Arrange
            List<string> receivedEvents = new List<string>();
            MainWindowViewModel testVM = new MainWindowViewModel();




            testVM.PropertyChanged += delegate (object sender, PropertyChangedEventArgs e) {

                receivedEvents.Add(e.PropertyName);

            };



            //Act
            testVM.MinIndexAxisValue = 2;


            //Assert

            // Verify if the NotifyProperty has  been trigger since it the new value
            Assert.AreEqual(1, receivedEvents.Count,
                "The count should be 1 since its the same value the PropertyChanged should not be triggered.");

            Assert.AreEqual("MinIndexAxisValue", receivedEvents[0], "The name should be equal.");

        }

        #endregion

        #region MaxIndexAxisValue Tests

        /// <summary>
        // The following test the MaxIndexAxisValue is the value is change to the same value.
        /// </summary>
        [TestMethod]
        public void MainWindowViewModel_MaxIndexAxisValue_SameValue()
        {
            //Arrange
            List<string> receivedEvents = new List<string>();
            MainWindowViewModel testVM = new MainWindowViewModel();




            testVM.PropertyChanged += delegate (object sender, PropertyChangedEventArgs e) {

                receivedEvents.Add(e.PropertyName);

            };

            //Act
            testVM.MaxIndexAxisValue = 0;


            //Assert

            // Verify if the NotifyProperty has not been trigger since it the same value
            Assert.AreEqual(0, receivedEvents.Count,
                "The count should be 0 since its the same value the PropertyChanged should not be triggered.");



        }


        /// <summary>
        /// The following test the MaxIndexAxisValue is the value is change to the new value.
        /// </summary>
        [TestMethod]
        public void MainWindowViewModel_MaxIndexAxisValue_NewValue()
        {

            //Arrange
            List<string> receivedEvents = new List<string>();
            MainWindowViewModel testVM = new MainWindowViewModel();




            testVM.PropertyChanged += delegate (object sender, PropertyChangedEventArgs e) {

                receivedEvents.Add(e.PropertyName);

            };



            //Act
            testVM.MaxIndexAxisValue = 2;


            //Assert

            // Verify if the NotifyProperty has  been trigger since it the new value
            Assert.AreEqual(1, receivedEvents.Count,
                "The count should be 1 since its the same value the PropertyChanged should not be triggered.");

            Assert.AreEqual("MaxIndexAxisValue", receivedEvents[0], "The name should be equal.");

        }

        #endregion
        
        #region DTE Tests

        /// <summary>
        // The following test the DTE is the value is change to the same value.
        /// </summary>
        [TestMethod]
        public void MainWindowViewModel_DTE_SameValue()
        {
            //Arrange
            List<string> receivedEvents = new List<string>();
            MainWindowViewModel testVM = new MainWindowViewModel();

            Type t = System.Type.GetTypeFromProgID("VisualStudio.DTE.12.0");

            var initialDTE = (EnvDTE.DTE)Activator.CreateInstance(t);

            testVM.DTE = initialDTE;
            

            testVM.PropertyChanged += delegate (object sender, PropertyChangedEventArgs e) {

                receivedEvents.Add(e.PropertyName);

            };



            //Act
            testVM.DTE = initialDTE;


            //Assert

            // Verify if the NotifyProperty has not been trigger since it the same value
            Assert.AreEqual(0, receivedEvents.Count,
                "The count should be 0 since its the same value the PropertyChanged should not be triggered.");



        }


        /// <summary>
        /// The following test the DTE is the value is change to the new value.
        /// </summary>
        [TestMethod]
        public void MainWindowViewModel_DTE_NewValue()
        {

            //Arrange
            List<string> receivedEvents = new List<string>();
            MainWindowViewModel testVM = new MainWindowViewModel();

            Type t = System.Type.GetTypeFromProgID("VisualStudio.DTE.12.0");

            var initialDTE = (EnvDTE.DTE)Activator.CreateInstance(t);


            testVM.DTE = initialDTE;



            testVM.PropertyChanged += delegate (object sender, PropertyChangedEventArgs e) {

                receivedEvents.Add(e.PropertyName);

            };

            var newDTE =  (EnvDTE.DTE)Activator.CreateInstance(t); ;


            //Act
            testVM.DTE = newDTE;


            //Assert

            // Verify if the NotifyProperty has  been trigger since it the new value
            Assert.AreEqual(1, receivedEvents.Count,
                "The count should be 1 since its the same value the PropertyChanged should not be triggered.");

            Assert.AreEqual("DTE", receivedEvents[0], "The name should be equal.");

        }

        #endregion

        #region SysManager Tests

        /// <summary>
        // The following test the SysManager is the value is change to the same value.
        /// </summary>
        [TestMethod]
        public void MainWindowViewModel_SysManager_SameValue()
        {
            //Arrange
            List<string> receivedEvents = new List<string>();
            MainWindowViewModel testVM = new MainWindowViewModel();

            Type t = System.Type.GetTypeFromProgID("VisualStudio.DTE.12.0");

            var initialDTE = (EnvDTE.DTE)Activator.CreateInstance(t);

            //testVM.SysManager = initialDTE.DTE;


            testVM.PropertyChanged += delegate (object sender, PropertyChangedEventArgs e) {

                receivedEvents.Add(e.PropertyName);

            };



            //Act
            //testVM.SysManager = initialDTE;


            //Assert

            // Verify if the NotifyProperty has not been trigger since it the same value
            Assert.AreEqual(1, receivedEvents.Count,
                "The count should be 0 since its the same value the PropertyChanged should not be triggered.");



        }


        /// <summary>
        /// The following test the SysManager is the value is change to the new value.
        /// </summary>
        [TestMethod]
        public void MainWindowViewModel_SysManager_NewValue()
        {

            //Arrange
            List<string> receivedEvents = new List<string>();
            MainWindowViewModel testVM = new MainWindowViewModel();


            //ITcSysManager initialSysManager = new ITcSysManager;


            //testVM.SysManager = initialSysManager;



            testVM.PropertyChanged += delegate (object sender, PropertyChangedEventArgs e) {

                receivedEvents.Add(e.PropertyName);

            };



            //Act
            //testVM.SysManager = newDTE;


            //Assert

            // Verify if the NotifyProperty has  been trigger since it the new value
            Assert.AreEqual(1, receivedEvents.Count,
                "The count should be 1 since its the same value the PropertyChanged should not be triggered.");

            Assert.AreEqual("SysManager", receivedEvents[0], "The name should be equal.");

        }

        #endregion

        #region SysLibrary Tests

        /// <summary>
        // The following test the SysLibrary is the value is change to the same value.
        /// </summary>
        [TestMethod]
        public void MainWindowViewModel_SysLibrary_SameValue()
        {
            //Arrange
            List<string> receivedEvents = new List<string>();
            MainWindowViewModel testVM = new MainWindowViewModel();

            Type t = System.Type.GetTypeFromProgID("VisualStudio.DTE.12.0");

            var initialDTE = (EnvDTE.DTE)Activator.CreateInstance(t);

            //testVM.SysLibrary = initialDTE.DTE;


            testVM.PropertyChanged += delegate (object sender, PropertyChangedEventArgs e) {

                receivedEvents.Add(e.PropertyName);

            };



            //Act
            //testVM.SysLibrary = initialDTE;


            //Assert

            // Verify if the NotifyProperty has not been trigger since it the same value
            Assert.AreEqual(1, receivedEvents.Count,
                "The count should be 0 since its the same value the PropertyChanged should not be triggered.");



        }


        /// <summary>
        /// The following test the SysLibrary is the value is change to the new value.
        /// </summary>
        [TestMethod]
        public void MainWindowViewModel_SysLibrary_NewValue()
        {

            //Arrange
            List<string> receivedEvents = new List<string>();
            MainWindowViewModel testVM = new MainWindowViewModel();


            //ITcSysManager initialSysManager = new ITcSysManager;


            //testVM.SysLibrary = initialSysManager;



            testVM.PropertyChanged += delegate (object sender, PropertyChangedEventArgs e) {

                receivedEvents.Add(e.PropertyName);

            };



            //Act
            //testVM.SysLibrary = newDTE;


            //Assert

            // Verify if the NotifyProperty has  been trigger since it the new value
            Assert.AreEqual(1, receivedEvents.Count,
                "The count should be 1 since its the same value the PropertyChanged should not be triggered.");

            Assert.AreEqual("SysLibrary", receivedEvents[0], "The name should be equal.");

        }

        #endregion

        #endregion

        #region Methods_Tests

                 
        #endregion


    }
}
